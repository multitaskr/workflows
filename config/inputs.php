<?php

return [
    'type' => [
        'welcome-screen' => 'Welcome Screen',
        'thank-you-screen' => 'Thank You Screen',
        'checkbox' => 'CheckBox',
        'radio' => 'Radio',
        'text' => 'Text',
        'textarea' => 'Textarea',
        'statement' => 'Statement',
        'phone' => 'Phone',
        'credit-card' => 'Credit Card',
        'captcha' => 'Captcha',
        'file' => 'File',
        'email' => 'Email',
        'opinion-scale' => 'Opinion Scale',
        'rating' => 'Rating',
        'date' => 'Date',
        'number' => 'Number',
        'hidden' => 'Hidden',
        'maps' => 'Maps',
        'calendar' => 'Calendar',
        'panda-doc' => 'Panda Doc'
    ],

    'structures' => [
        'welcome-screen' => [
            'type' => 'welcome-screen',
            'required' => false,
            'hint' => null,
            'label' => null,
            'description' => null,
            'image' => null,
            'button' => [
                'color' => null,
                'text' => null
            ]
        ],
        'thank-you-screen' => [
            'type' => 'thank-you-screen' ,
            'required' => false,
            'hint' => null,
            'label' => null,
            'image' => null,
        ],
        'checkbox' => [
            'type' => 'checkbox',
            'required' => false,
            'hint' => null,
            'placeholder' => null,
            'label' => null,
            'description' => null,
            'image' => null,
            'with_images' => null,
            'hide_label' => null,
            'show' => true,
            'options' => [
                [
                    'image' => null,
                    'value' => null,
                    'text' => null
                ]
            ],
        ],
        'radio' => [
            'type' => 'radio',
            'required' => false,
            'hint' => null,
            'label' => null,
            'description' => null,
            'placeholder' => null,
            'image' => null,
            'with_images' => null,
            'hide_label' => null,
            'show' => true,
            'options' => [
                [
                    'image' => null,
                    'value' => null,
                    'text' => null
                ]
            ],
        ],
        'text' => [
            'type' => 'text',
            'required' => false,
            'hint' => null,
            'label' => null,
            'description' => null,
            'placeholder' => null,
            'image' => null,
            'hide_label' => null,
            'show' => true,
        ],
        'textarea' => [
            'type' => 'textarea',
            'required' => false,
            'hint' => null,
            'label' => null,
            'description' => null,
            'placeholder' => null,
            'image' => null,
            'hide_label' => null,
            'show' => true,
        ],
        'statement' => [
            'type' => 'statement',
            'required' => false,
            'hint' => null,
            'label' => null,
            'description' => null,
            'placeholder' => null,
            'image' => null,
            'hide_label' => null,
            'show' => true,
            'button' => [
                'text' => null,
                'color' => null
            ]
        ],
        'phone' => [
            'type' => 'phone',
            'required' => false,
            'hint' => null,
            'label' => null,
            'description' => null,
            'placeholder' => null,
            'image' => null,
            'hide_label' => null,
            'show' => true,
        ],
        'credit-card' => [
            'type' => 'credit-card',
            'required' => false,
            'hint' => null,
            'label' => null,
            'description' => null,
            'placeholder' => null,
            'image' => null,
            'hide_label' => null,
            'show' => true,
            'button' => [
                'text' => null,
                'color' => null
            ]
        ],
        'captcha' => [
            'type' => 'captcha',
            'required' => false,
            'hint' => null,
            'label' => null,
            'description' => null,
            'placeholder' => null,
            'image' => null,
            'hide_label' => null,
            'show' => true,
        ],
        'file' => [
            'type' => 'file',
            'required' => false,
            'hint' => null,
            'label' => null,
            'description' => null,
            'placeholder' => null,
            'image' => null,
            'hide_label' => null,
            'show' => true,
        ],
        'email' => [
            'type' => 'email',
            'required' => false,
            'hint' => null,
            'label' => null,
            'description' => null,
            'placeholder' => null,
            'image' => null,
            'hide_label' => null,
            'show' => true,
        ],
        'opinion-scale' => [
            'type' => 'opinion-scale',
            'required' => false,
            'hint' => null,
            'label' => null,
            'description' => null,
            'placeholder' => null,
            'image' => null,
            'start_scale_at_one' => false,
            'steps' => 10,
            'hide_label' => null,
            'show' => true,
        ],
        'rating' => [
            'type' => 'rating',
            'required' => false,
            'hint' => null,
            'label' => null,
            'description' => null,
            'placeholder' => null,
            'image' => null,
            'steps' => 5,
            'hide_label' => null,
            'show' => true,
        ],
        'date' => [
            'type' => 'date',
            'required' => false,
            'hint' => null,
            'label' => null,
            'description' => null,
            'placeholder' => null,
            'image' => null,
            'hide_label' => null,
            'show' => true,
        ],
        'number' => [
            'type' => 'number',
            'required' => false,
            'hint' => null,
            'label' => null,
            'description' => null,
            'placeholder' => null,
            'image' => null,
            'min_number' => 1,
            'max_number' => 1000,
            'hide_label' => null,
            'show' => true,
        ],
        'hidden' => [
            'type' => 'hidden',
            'required' => false,
            'hint' => null,
            'hide_label' => null
        ],
        'maps' => [
            'type' => 'maps',
            'required' => false,
            'hint' => null,
            'label' => null,
            'description' => null,
            'placeholder' => null,
            'image' => null,
            'hide_label' => null,
            'show' => true,
        ],
        'calendar' => [
            'type' => 'calendar',
            'required' => false,
            'hint' => null,
            'label' => null,
            'description' => null,
            'placeholder' => null,
            'image' => null,
            'hide_label' => null,
            'calendar_uid' => null,
            'show' => true,
        ],
        'panda-doc' => [
            'type' => 'calendar',
            'required' => false,
            'hint' => null,
            'label' => null,
            'description' => null,
            'placeholder' => null,
            'image' => null,
            'hide_label' => null,
            'show' => true,
            'doc_id' => null
        ]
    ]
];