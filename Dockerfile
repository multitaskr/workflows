FROM php:7.4-apache
ARG APP_ENV

# Install packages
RUN apt-get update && apt-get install -y \
    git \
    zip \
    curl \
    sudo \
    unzip \
    libicu-dev \
    libbz2-dev \
    libpng-dev \
    libjpeg-dev \
    libmcrypt-dev \
    libreadline-dev \
    libfreetype6-dev \
    sqlite3 \
    libsqlite3-dev \
    g++ \
    wget \
    systemctl \
    supervisor \
    librabbitmq-dev \
    && pecl install amqp \
    && docker-php-ext-enable amqp \
    && docker-php-ext-install sockets


RUN wget -O phpunit https://phar.phpunit.de/phpunit-9.phar

RUN chmod +x phpunit

# Apache configuration
ENV APACHE_DOCUMENT_ROOT=/var/www/html/public
RUN sed -ri -e 's!/var/www/html!${APACHE_DOCUMENT_ROOT}!g' /etc/apache2/sites-available/*.conf
RUN sed -ri -e 's!/var/www/!${APACHE_DOCUMENT_ROOT}!g' /etc/apache2/apache2.conf /etc/apache2/conf-available/*.conf
RUN a2enmod rewrite headers

RUN docker-php-ext-configure gd \
    --with-jpeg \
    --with-freetype

# Common PHP Extensions
RUN docker-php-ext-install \
    bz2 \
    intl \
    iconv \
    bcmath \
    opcache \
    calendar \
    pdo_mysql \
    gd 

# Ensure PHP logs are captured by the container
#ENV LOG_CHANNEL=stderr

# Copy code and run composer
COPY --from=composer:latest /usr/bin/composer /usr/bin/composer

COPY composer.json /var/www/html
COPY composer.lock /var/www/html
RUN composer install --no-scripts --no-autoloader
COPY --chown=www-data:www-data . /var/www/html
RUN composer dump-autoload --optimize

COPY supervisord.conf /etc/supervisord.conf

COPY ".env.$APP_ENV" .env

RUN php artisan key:generate
RUN php artisan config:cache
RUN php artisan config:clear
RUN php artisan cache:clear

COPY --chown=www-data:www-data oauth-public.key /var/www/html/storage
RUN chmod 755 /var/www/html/storage/oauth-public.key

RUN touch /usr/local/etc/php/conf.d/uploads.ini \
    && echo "upload_max_filesize = 512M;\nmax_execution_time = 30000;\npost_max_size = 512M;\nmax_input_time = 3000000;" >> /usr/local/etc/php/conf.d/uploads.ini
    
WORKDIR /var/www/html


# The default apache run command
CMD supervisord -c /etc/supervisord.conf



