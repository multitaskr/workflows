<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Foundation\Testing\RefreshDatabase;

class WorkflowTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function uid_is_set_when_creating()
    {
        $workflow = factory('App\Models\Workflow')->create();

        $this->assertNotNull($workflow->uid);
    }

    /** @test */
    public function a_workflow_has_steps()
    {
        $workflow = factory('App\Models\Workflow')->create();

        $this->assertInstanceOf(Collection::class,$workflow->steps);
    }
}
