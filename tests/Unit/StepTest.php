<?php

namespace Tests\Unit;

use Tests\TestCase;
use App\Models\Workflow;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Foundation\Testing\RefreshDatabase;

class StepTest extends TestCase
{
    use RefreshDatabase;
    
    /** @test */
    public function uid_is_set_when_creating()
    {
        $step = factory('App\Models\Step')->create();

        $this->assertNotNull($step->uid);
    }

    /** @test */
    public function a_step_has_a_workflow()
    {
        $step = factory('App\Models\Step')->create();

        $this->assertInstanceOf(Workflow::class,$step->workflow);
    }

    /** @test */
    public function a_step_has_sections()
    {
        $step = factory('App\Models\Step')->create();

        $this->assertInstanceOf(Collection::class,$step->sections);
    }
}
