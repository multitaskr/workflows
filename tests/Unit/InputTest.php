<?php

namespace Tests\Unit;

use Tests\TestCase;
use App\Models\Section;
use Illuminate\Foundation\Testing\RefreshDatabase;

class InputTest extends TestCase
{
    use RefreshDatabase;
    
    /** @test */
    public function uid_is_set_when_creating()
    {
        $input = factory('App\Models\Input')->create();

        $this->assertNotNull($input->uid);
    }


    /** @test */
    public function a_section_has_a_section()
    {
        $input = factory('App\Models\Input')->create();

        $this->assertInstanceOf(Section::class,$input->section);
    }
}
