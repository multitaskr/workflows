<?php

namespace Tests\Unit;

use Tests\TestCase;
use App\Models\Step;
use Illuminate\Foundation\Testing\RefreshDatabase;


class StepLogicTest extends TestCase
{
    use RefreshDatabase;
    
    /** @test */
    public function uid_is_set_when_creating()
    {
        $stepLogic = factory('App\Models\StepLogic')->create();

        $this->assertNotNull($stepLogic->uid);
    }


    /** @test */
    public function a_section_has_a_step()
    {
        $stepLogic = factory('App\Models\StepLogic')->create();

        $this->assertInstanceOf(Step::class, $stepLogic->step);
    }
}
    