<?php

namespace Tests\Unit;

use Tests\TestCase;
use App\Models\Step;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Database\Eloquent\Collection;

class SectionTest extends TestCase
{
    use RefreshDatabase;
    
    /** @test */
    public function uid_is_set_when_creating()
    {
        $section = factory('App\Models\Section')->create();

        $this->assertNotNull($section->uid);
    }


    /** @test */
    public function a_section_has_a_step()
    {
        $section = factory('App\Models\Section')->create();

        $this->assertInstanceOf(Step::class,$section->step);
    }

    /** @test */
    public function a_section_has_inputs()
    {
        $section = factory('App\Models\Section')->create();

        $this->assertInstanceOf(Collection::class,$section->inputs);
    }
}
