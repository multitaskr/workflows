<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Support\Facades\Event;
use App\Events\SubjectStepActionHandled;
use Illuminate\Foundation\Testing\RefreshDatabase;

class SubjectStepManagementTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function a_user_can_clone_steps()
    {        
        Event::fake([
            SubjectStepActionHandled::class,
        ]);
    
       
        $workflow = factory('App\Models\Workflow')->create([
            'clone_steps' => true
        ]);

        factory('App\Models\Step')->create([
            'order' => 1,
            'workflow_id' => $workflow->id
        ]);

        factory('App\Models\Step')->create([
            'order' => 2,
            'workflow_id' => $workflow->id
        ]);


        $this->post(route('workflows.clone-steps', $workflow), [
            'subject_api' => 'test',
            'subject_type' => 'Test',
            'subject_uid' => '123-abc'
        ])
        ->assertStatus(200)
        ->assertJsonStructure(['data']);

        Event::assertDispatched(SubjectStepActionHandled::class);

        $this->assertDatabaseCount('subject_steps', 2);
    }

    /** @test */
    public function a_user_can_get_list_of_subject_steps()
    {        
        factory('App\Models\SubjectStep')->create();

        factory('App\Models\SubjectStep')->create();

        $this->get(route('subject-step.index'))
        ->assertStatus(200)
        ->assertJsonStructure(['data', 'meta']);
    }

    /** @test */
    public function a_user_can_get_a_subject_step()
    {        
        $subjectStep = factory('App\Models\SubjectStep')->create();

        $this->get(route('subject-step.show', $subjectStep))
        ->assertStatus(200)
        ->assertJsonFragment([
            'uid' => $subjectStep->uid
        ]);
    }

    /** @test */
    public function a_user_can_complete_a_subject_step()
    {        
        Event::fake([
            SubjectStepActionHandled::class,
        ]);
    
        $subjectStep = factory('App\Models\SubjectStep')->create([
            'status' => 'in progress',
            'order' => 1,
        ]);

        $nextSubjectStep = factory('App\Models\SubjectStep')->create([
            'status' => 'pending',
            'order' => 2,
        ]);

        $this->post(route('subject-step.complete', $subjectStep), [
            'subject_step_uid' => $nextSubjectStep->uid
        ])
        ->assertStatus(200)
        ->assertJsonFragment([
            'uid' => $subjectStep->uid,
            'status' => 'completed',
        ]);

        Event::assertDispatched(SubjectStepActionHandled::class);

    }


    /** @test */
    public function a_user_can_rollback_a_subject_step()
    {        
        Event::fake([
            SubjectStepActionHandled::class,
        ]);

        factory('App\Models\SubjectStep')->create([
            'status' => 'completed',
            'order' => 1
        ]);
    
        $subjectStep = factory('App\Models\SubjectStep')->create([
            'status' => 'in progress',
            'order' => 2
        ]);

        $this->post(route('subject-step.rollback', $subjectStep))
        ->assertStatus(200)
        ->assertJsonFragment([
            'uid' => $subjectStep->uid,
            'status' => 'pending'
        ]);

        Event::assertDispatched(SubjectStepActionHandled::class);

    }
}
