<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class PrivateInputValueTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function an_api_can_update_a_input_value_by_input_uid()
    {
        $input = factory('App\Models\Input')->create();
        $inputValue = factory('App\Models\InputValue')->create([
            'subject_uid' => 'testing-testing',
            'subject_type' => 'Deal',
            'subject_api' => 'deals',
            'value' => 1,
            'input_id' => $input->id
        ]);

        $this->post(route('private.inputs.value', $input), [
            'subject_uid' => 'testing-testing',
            'subject_type' => 'Deal',
            'subject_api' => 'deals',
            'value' => 2
        ])->assertStatus(201)
        ->assertJsonFragment([
            'value' => 2
        ]);
    }

    /** @test */
    public function an_api_can_add_empty_value_by_input_uid()
    {
        $input = factory('App\Models\Input')->create();

        $this->post(route('private.inputs.value', $input), [
            'subject_uid' => 'testing-testing',
            'subject_type' => 'Deal',
            'subject_api' => 'deals'
        ])->assertStatus(200);
    }
}
