<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Support\Str;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class SectionManagementTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function a_user_can_get_the_list_of_sections()
    {
        factory('App\Models\Section')->create();

        $this->get(route('sections.index'))
        ->assertStatus(200)
        ->assertJsonStructure(['data', 'meta']);
    }

    /** @test */
    public function a_user_can_get_only_the_fields_of_sections_that_he_send()
    {
        factory('App\Models\Section')->create();

        $this->call('GET', route('sections.index'), ['fields' => 'uid,title'])
        ->assertStatus(200)
        ->assertJsonStructure([
            'data' => [
            '*' => [
                'uid',
                'title',
                ]
            ]   
        ]);
    }

    /** @test */
    public function a_user_can_filter_the_list_of_sections()
    {
        factory('App\Models\Section')->create([
            'title' => 'Section One'
        ]);

        factory('App\Models\Section')->create([
            'title' => 'Section Two'
        ]);

        $this->call('GET', route('sections.index'),['search' => 'Section One'])
        ->assertStatus(200)
        ->assertJsonFragment(['total' => 1]);
    }

    /** @test */
    public function a_user_can_create_a_section()
    {
        $this->withoutExceptionHandling();
        $attributes = factory("App\Models\Section")
        ->raw([
            'step' => factory("App\Models\Step")->create()->uid
        ]);
        
        $this->post(route('sections.store'), $attributes)
        ->assertStatus(201)
        ->assertJsonStructure([
            'data' => ['uid']
        ]);

        $this->assertDatabaseCount('sections', 1);
    }

    /** @test */
    public function a_user_can_view_a_section()
    {
        $section = factory('App\Models\Section')->create();

        $this->get(route('sections.show', $section))
        ->assertStatus(200)
        ->assertJsonFragment([
            'uid' => $section->uid 
        ]);
    }

    /** @test */
    public function a_user_cant_view_a_section_that_doesnt_exists()
    {
        $this->get(route('sections.show', 1))
        ->assertStatus(404);
    }

    /** @test */
    public function a_user_can_update_a_section()
    {
        $section = factory('App\Models\Section')->create();
        
        $step = factory("App\Models\Step")->create();

        $this->put(route('sections.update', $section), [
            'title' => 'changed',
            'order' => 1,
            'step' => $step->uid
        ])->assertStatus(200)
        ->assertJsonFragment([
            'title' => 'changed',
            'order' => 1,
            'uid' => $step->uid
        ]);
    }

    /** @test */
    public function a_user_cant_update_a_section_that_doesnt_exists()
    {
        $this->put(route('sections.update', 1), [
            'title' => 'changed'
        ])->assertStatus(404);
    }

    /** @test */
    public function a_user_can_delete_a_section()
    {
        $section = factory('App\Models\Section')->create();
        
        $this->delete(route('sections.destroy', $section))
        ->assertStatus(200);

        $this->assertSoftDeleted('sections', [
            'id' => $section->id
        ]);
    }

    /** @test */
    public function a_user_cant_delete_a_section_that_doesnt_exist()
    {
        $this->delete(route('sections.destroy', 1))
        ->assertStatus(404);
    }

    /** @test */
    public function a_user_can_save_input_values_of_the_step()
    {        
        $section = factory('App\Models\Section')->create();

        $inputs = factory('App\Models\Input', 2)->create([
            'section_id' => $section->id
        ]);

        $attributes = [];

        foreach ($inputs as $input) {
            $attributes['values'][$input->uid] = 'TEST';
        }

        $attributes['subject_uid'] = (string) Str::uuid();
        $attributes['subject_type'] = 'Test';
        $attributes['subject_api'] = 'test';

        $this->post(route('sections.values.store', $section), $attributes)
        ->assertStatus(200)
        ->assertJsonStructure([
            'data'
        ]);
    }

    /** @test */
    public function to_create_and_update_a_section_title_is_required()
    {
        $attributes = factory("App\Models\Section")
        ->raw([
            'title' => ''
        ]);

        $section = factory('App\Models\Section')->create();

        $this->post(route('sections.store'), $attributes)
        ->assertSessionHasErrors('title');

        $this->put(route('sections.update', $section), [
            'title' => ''
        ])->assertSessionHasErrors('title');
    }

    /** @test */
    public function to_update_a_section_order_is_required()
    {

        $section = factory('App\Models\Section')->create();

        $this->put(route('sections.update', $section), [
            'order' => ''
        ])->assertSessionHasErrors('order');
    }
}
