<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class StepLogicsManagementTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function a_user_can_get_the_list_of_step_logics()
    {
        factory('App\Models\StepLogic')->create();

        $this->get(route('step-logics.index'))
        ->assertStatus(200)
        ->assertJsonStructure(['data', 'meta']);
    }

    /** @test */
    public function a_user_can_get_only_the_fields_of_step_logics_that_he_send()
    {
        $this->withoutExceptionHandling();

        factory('App\Models\StepLogic')->create();

        $this->call('GET', route('step-logics.index'), ['fields' => 'uid'])
        ->assertStatus(200)
        ->assertJsonStructure([
            'data' => [
            '*' => [
                'uid',
                ]
            ]   
        ]);
    }

    /** @test */
    public function a_user_can_create_a_step_logic()
    {
        $this->withoutExceptionHandling();
        $attributes = factory("App\Models\StepLogic")
        ->raw([
            'step' => factory("App\Models\Step")->create()->uid
        ]);
        
        $this->post( route('step-logics.store'), $attributes)
        ->assertStatus(201)
        ->assertJsonStructure([
            'data' => ['uid']
        ]);

        $this->assertDatabaseCount('step_logics', 1);
    }

    /** @test */
    public function a_user_can_view_a_step_logic()
    {
        $step_logic = factory('App\Models\StepLogic')->create();

        $this->get( route('step-logics.show', $step_logic))
        ->assertStatus(200)
        ->assertJsonFragment([
            'uid' => $step_logic->uid 
        ]);
    }

    /** @test */
    public function a_user_cant_view_a_step_logic_that_doesnt_exists()
    {
        $this->get(route('step-logics.show', 1))
        ->assertStatus(404);
    }

    /** @test */
    public function a_user_can_update_a_step_logic()
    {
        $this->withoutExceptionHandling();
        $step_logic = factory('App\Models\StepLogic')->create();
        
        $step = factory("App\Models\Step")->create();

        $this->put(route('step-logics.update', $step_logic), [
            'logic_data' => '{"changed":"changed"}',
            'action' => '{"changed":"changed"}',
            'step' => $step->uid
        ])->assertStatus(200)
        ->assertJsonFragment([
            'uid' => $step->uid
        ]);
    }

    /** @test */
    public function a_user_cant_update_a_step_logic_that_doesnt_exists()
    {
        $this->put(route('step-logics.show', 1), [
            'title' => 'changed'
        ])->assertStatus(404);
    }

    /** @test */
    public function a_user_can_delete_a_step_logic()
    {
        $step_logic = factory('App\Models\StepLogic')->create();
        
        $this->delete(route('step-logics.destroy', $step_logic))
        ->assertStatus(200);

        $this->assertSoftDeleted('step_logics', ['id' => $step_logic->id]);
    }

    /** @test */
    public function a_user_cant_delete_a_step_logic_that_doesnt_exist()
    {
        $this->delete(route('step-logics.destroy', 1))
        ->assertStatus(404);
    }

    /** @test */
    public function to_create_and_update_a_step_logic_logic_data_is_required()
    {
        $attributes = factory("App\Models\StepLogic")
        ->raw([
            'logic_data' => ''
        ]);

        $step_logic = factory('App\Models\StepLogic')->create();

        $this->post( route('step-logics.store'), $attributes)
        ->assertSessionHasErrors('logic_data');

        $this->put( route('step-logics.update', $step_logic), [
            'logic_data' => ''
        ])->assertSessionHasErrors('logic_data');
    }

    /** @test */
    public function to_create_and_update_a_step_logic_action_is_required()
    {
        $attributes = factory("App\Models\StepLogic")
        ->raw([
            'action' => ''
        ]);

        $step_logic = factory('App\Models\StepLogic')->create();

        $this->post( route('step-logics.store'), $attributes)
        ->assertSessionHasErrors('action');

        $this->put( route('step-logics.update', $step_logic), [
            'action' => ''
        ])->assertSessionHasErrors('action');
    }
    
}
