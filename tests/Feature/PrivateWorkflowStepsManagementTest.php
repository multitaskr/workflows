<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class PrivateWorkflowStepsManagementTest extends TestCase
{
    use RefreshDatabase;
    
    /** @test */
    public function an_api_can_get_the_steps_of_a_workflow()
    {
        $workflow = factory('App\Models\Workflow')->create();

        factory('App\Models\Step', 2)->create([
            'workflow_id' => $workflow->id
        ]);

        $this->get(route('private.workflow.step.index', $workflow))
        ->assertStatus(200)
        ->assertJsonStructure(['data']);
    }

    /** @test */
    public function an_api_can_get_the_public_step_of_a_workflow()
    {
        $workflow = factory('App\Models\Workflow')->create();

        factory('App\Models\Step', 2)->create([
            'workflow_id' => $workflow->id,
            'public' => true
        ]);

        $this->get(route('private.public.workflow.step.show', $workflow))
        ->assertStatus(200)
        ->assertJsonStructure(['data']);
    }

    /** @test */
    public function an_api_can_get_a_workflow()
    {
        $workflow = factory('App\Models\Workflow')->create();

        $this->get(route('private.workflows.show', $workflow))
        ->assertStatus(200)
        ->assertJsonFragment([
            'uid' => $workflow->uid 
        ]);
    }
}
