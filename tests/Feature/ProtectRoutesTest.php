<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ProtectRoutesTest extends TestCase
{
    use RefreshDatabase;
    
    public function setUp(): void
    {
        parent::setUp();

        app()->detectEnvironment(function() { return 'local'; });
    }

    /** @test  */
    public function guest_cannot_manage_workflows()
    {
        $workflow = factory('App\Models\Workflow')->create();

        $this->get(route('workflows.index'))->assertStatus(403);
        $this->post(route('workflows.store'))->assertStatus(403);
        $this->get(route('workflows.show', $workflow))->assertStatus(403);
        $this->put(route('workflows.update', $workflow))->assertStatus(403);
        $this->delete(route('workflows.destroy', $workflow))->assertStatus(403);
    }

    /** @test  */
    public function guest_cannot_manage_steps()
    {
        $step = factory('App\Models\Step')->create();

        $this->get(route('steps.index'))->assertStatus(403);
        $this->post(route('steps.store'))->assertStatus(403);
        $this->get(route('steps.show', $step))->assertStatus(403);
        $this->put(route('steps.update', $step))->assertStatus(403);
        $this->delete(route('steps.destroy', $step))->assertStatus(403);
    }

    /** @test  */
    public function guest_cannot_manage_step_logics()
    {
        $stepLogic = factory('App\Models\StepLogic')->create();

        $this->get(route('step-logics.index'))->assertStatus(403);
        $this->post(route('step-logics.store'))->assertStatus(403);
        $this->get(route('step-logics.show', $stepLogic))->assertStatus(403);
        $this->put(route('step-logics.update', $stepLogic))->assertStatus(403);
        $this->delete(route('step-logics.destroy', $stepLogic))->assertStatus(403);
    }

    /** @test  */
    public function guest_cannot_manage_section()
    {
        $section = factory('App\Models\Section')->create();

        $this->get(route('sections.index'))->assertStatus(403);
        $this->post(route('sections.store'))->assertStatus(403);
        $this->get(route('sections.show', $section))->assertStatus(403);
        $this->put(route('sections.update', $section))->assertStatus(403);
        $this->delete(route('sections.destroy', $section))->assertStatus(403);
    }

    /** @test  */
    public function guest_cannot_manage_inputs()
    {
        $input = factory('App\Models\Input')->create();

        $this->get(route('inputs.index'))->assertStatus(403);
        $this->post(route('inputs.store'))->assertStatus(403);
        $this->get(route('inputs.show', $input))->assertStatus(403);
        $this->put(route('inputs.update', $input))->assertStatus(403);
        $this->delete(route('inputs.destroy', $input))->assertStatus(403);
    }

    public function tearDown() : void
    {
        app()->detectEnvironment(function() { return 'testing'; });
    }
}
