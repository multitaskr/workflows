<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Support\Str;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class StepManagementTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function a_user_can_get_the_list_of_steps()
    {
        factory('App\Models\Step')->create();

        $this->get(route('steps.index'))
        ->assertStatus(200)
        ->assertJsonStructure(['data', 'meta']);
    }

    /** @test */
    public function a_user_can_get_only_the_fields_of_steps_that_he_send()
    {
        factory('App\Models\Step')->create();

        $this->call('GET', route('steps.index'), ['fields' => 'uid,name'])
        ->assertStatus(200)
        ->assertJsonStructure([
            'data' => [
            '*' => [
                'uid',
                'name',
                ]
            ]   
        ]);
    }

    /** @test */
    public function a_user_can_filter_the_list_of_steps()
    {
        factory('App\Models\Step')->create([
            'name' => 'Step One'
        ]);

        factory('App\Models\Step')->create([
            'name' => 'Step Two'
        ]);

        $this->call('GET', route('steps.index'),['search' => 'Step One'])
        ->assertStatus(200)
        ->assertJsonFragment(['total' => 1]);
    }

    /** @test */
    public function a_user_can_filter_the_list_of_steps_by_workflow()
    {
        $workflow = factory('App\Models\Workflow')->create();
        factory('App\Models\Step')->create([
            'name' => 'Step One',
            'workflow_id' => $workflow->id
        ]);

        $workflow = factory('App\Models\Workflow')->create();
        factory('App\Models\Step')->create([
            'name' => 'Step Two',
            'workflow_id' => $workflow->id
        ]);

        $this->call('GET', route('steps.index'),['workflow' => $workflow->uid])
        ->assertStatus(200)
        ->assertJsonFragment(['total' => 1]);
    }

    /** @test */
    public function a_user_can_create_a_step()
    {
        $attributes = factory("App\Models\Step")
        ->raw([
            'workflow' => factory("App\Models\Workflow")->create()->uid
        ]);
        
        $this->post( route('steps.store'), $attributes)
        ->assertStatus(201)
        ->assertJsonStructure([
            'data' => ['uid']
        ]);

        $this->assertDatabaseCount('steps', 1);
    }

    /** @test */
    public function a_user_can_view_a_step()
    {
        $step = factory('App\Models\Step')->create();

        $this->get(route('steps.show', $step))
        ->assertStatus(200)
        ->assertJsonFragment([
            'uid' => $step->uid 
        ]);
    }

    /** @test */
    public function a_user_cant_view_a_step_that_doesnt_exists()
    {
        $this->get(route('steps.show', 1))
        ->assertStatus(404);
    }

    /** @test */
    public function a_user_can_update_a_step()
    {
        $step = factory('App\Models\Step')->create();
        
        $workflow = factory("App\Models\Workflow")->create();

        $this->put(route('steps.update', $step), [
            'name' => 'changed',
            'description' => 'changed',
            'order' => 1,
            'public' => 1,
            'workflow' => $workflow->uid
        ])->assertStatus(200)
        ->assertJsonFragment([
            'name' => 'changed',
            'description' => 'changed',
            'order' => 1,
            'public' => 1,
            'uid' => $workflow->uid
        ]);
    }

    /** @test */
    public function a_user_cant_update_a_step_that_doesnt_exists()
    {
        $this->put(route('steps.update', 1), [
            'name' => 'changed'
        ])->assertStatus(404);
    }

    /** @test */
    public function a_user_can_delete_a_step()
    {
        $step = factory('App\Models\Step')->create();
        
        $this->delete(route('steps.destroy', $step))
        ->assertStatus(200);

        $this->assertSoftDeleted('steps', ['id' => $step->id]);
    }

    /** @test */
    public function a_user_cant_delete_a_step_that_doesnt_exist()
    {
        $this->delete(route('steps.destroy', 1))
        ->assertStatus(404);
    }

    /** @test */
    public function a_user_can_save_input_values_of_the_step()
    {        
        $step = factory('App\Models\Step')->create();

        $section = factory('App\Models\Section')->create([
            'step_id' => $step->id
        ]);

        $inputs = factory('App\Models\Input', 2)->create([
            'section_id' => $section->id
        ]);

        $attributes = [];

        foreach ($inputs as $input) {
            $attributes['values'][$input->uid] = 'TEST';
        }

        $attributes['subject_uid'] = (string) Str::uuid();
        $attributes['subject_type'] = 'Test';
        $attributes['subject_api'] = 'test';

        $this->post(route('steps.values.store', $step), $attributes)
        ->assertStatus(200)
        ->assertJsonStructure([
            'data'
        ]);
    }

    /** @test */
    public function to_create_and_update_a_step_name_is_required()
    {
        $attributes = factory("App\Models\Step")
        ->raw([
            'name' => ''
        ]);

        $step = factory('App\Models\Step')->create();

        $this->post(route('steps.store'), $attributes)
        ->assertSessionHasErrors('name');

        $this->put(route('steps.update', $step), [
            'name' => ''
        ])->assertSessionHasErrors('name');
    }

    /** @test */
    public function to_create_and_update_a_step_public_property_is_required()
    {
        $attributes = factory("App\Models\Step")
        ->raw([
            'public' => ''
        ]);

        $step = factory('App\Models\Step')->create();

        $this->post(route('steps.store'), $attributes)
        ->assertSessionHasErrors('public');

        $this->put(route('steps.update', $step), [
            'public' => ''
        ])->assertSessionHasErrors('public');
    }

    /** @test */
    public function to_update_a_step_order_is_required()
    {

        $step = factory('App\Models\Step')->create();

        $this->put( route('steps.update', $step), [
            'order' => ''
        ])->assertSessionHasErrors('order');
    }
}
