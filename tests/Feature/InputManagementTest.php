<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class InputManagementTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function a_user_can_get_the_list_of_inputs()
    {
        factory('App\Models\Input')->create();
       
        $this->get(route('inputs.index'))
        ->assertStatus(200)
        ->assertJsonStructure(['data', 'meta']);
    }

    /** @test */
    public function a_user_can_get_only_the_fields_of_inputs_that_he_send()
    {
        factory('App\Models\Input')->create();

        $this->call('GET',route('inputs.index'), ['fields' => 'uid,title'])
        ->assertStatus(200)
        ->assertJsonStructure([
            'data' => [
            '*' => [
                'uid',
                'title',
                ]
            ]   
        ]);
    }

    /** @test */
    public function a_user_can_filter_the_list_of_inputs()
    {
        factory('App\Models\Input')->create([
            'title' => 'Input One'
        ]);

        factory('App\Models\Input')->create([
            'title' => 'Input Two'
        ]);

        $this->call('GET', route('inputs.index'),['search' => 'Input One'])
        ->assertStatus(200)
        ->assertJsonFragment(['total' => 1]);
    }

    /** @test */
    public function a_user_can_create_a_input()
    {
        $attributes = factory("App\Models\Input")
        ->raw([
            'data' => '{"type": "text", "required":true}',
            'section' => factory("App\Models\Section")->create()->uid
        ]);

        $this->post(route('inputs.store'), $attributes)
        ->assertStatus(201);

        $this->assertDatabaseCount('inputs', 1);
    }

    /** @test */
    public function a_user_can_view_a_input()
    {
        $input = factory('App\Models\Input')->create();

        $this->get(route('inputs.show', $input))
        ->assertStatus(200)
        ->assertJsonFragment([
            'uid' => $input->uid 
        ]);
    }

    /** @test */
    public function a_user_cant_view_a_input_that_doesnt_exists()
    {
        $this->get(route('inputs.update', 1))
        ->assertStatus(404);
    }

    /** @test */
    public function a_user_can_update_a_input()
    {
        $input = factory('App\Models\Input')->create();
        
        $section = factory("App\Models\Section")->create();
        $this->withoutExceptionHandling();
        
        $this->put(route('inputs.update', $input), [
            'title' => 'changed',
            'order' => 1,
            'data' => '{"type": "changed", "required":true}',
            'section' => $section->uid
        ])->assertStatus(200)
        ->assertJsonFragment([
            'title' => 'changed',
            'order' => 1,
            'uid' => $section->uid
        ]);
    }

    /** @test */
    public function a_user_cant_update_a_input_that_doesnt_exists()
    {
        $this->put(route('inputs.update', 1), [
            'title' => 'changed'
        ])->assertStatus(404);
    }

    /** @test */
    public function a_user_can_delete_a_input()
    {
        $input = factory('App\Models\Input')->create();
        
        $this->delete(route('inputs.destroy', $input))
        ->assertStatus(200);

        $this->assertSoftDeleted('inputs', ['id' => $input->id]);
    }

    /** @test */
    public function a_user_cant_delete_a_input_that_doesnt_exist()
    {
        $this->delete(route('inputs.destroy', 1))
        ->assertStatus(404);
    }

    /** @test */
    public function to_create_and_update_a_input_title_is_required()
    {
        $attributes = factory("App\Models\Input")
        ->raw([
            'title' => ''
        ]);

        $input = factory('App\Models\Input')->create();

        $this->post(route('inputs.store'), $attributes)
        ->assertSessionHasErrors('title');

        $this->put(route('inputs.update', $input), [
            'title' => ''
        ])->assertSessionHasErrors('title');
    }

    /** @test */
    public function to_create_and_update_a_input_data_is_required()
    {
        $attributes = factory("App\Models\Input")
        ->raw([
            'data' => ''
        ]);

        $input = factory('App\Models\Input')->create();

        $this->post(route('inputs.store'), $attributes)
        ->assertSessionHasErrors('data');

        $this->put(route('inputs.update', $input), [
            'data' => ''
        ])->assertSessionHasErrors('data');
    }

     /** @test */
    public function to_create_and_update_a_input_section_is_required()
    {
        $attributes = factory("App\Models\Input")
        ->raw([
            'section' => ''
        ]);

        $input = factory('App\Models\Input')->create();

        $this->post(route('inputs.store'), $attributes)
        ->assertSessionHasErrors('section');

        $this->put(route('inputs.update', $input), [
            'section' => ''
        ])->assertSessionHasErrors('section');
    }

    /** @test */
    public function to_update_a_input_order_is_required()
    {

        $input = factory('App\Models\Input')->create();

        $this->put(route('inputs.update', $input), [
            'order' => ''
        ])->assertSessionHasErrors('order');
    }
}
