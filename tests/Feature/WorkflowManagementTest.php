<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class WorkflowManagementTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function a_user_can_get_the_list_of_workflows()
    {
        factory('App\Models\Workflow')->create();

        $this->get(route('workflows.index'))
            ->assertStatus(200)
            ->assertJsonStructure(['data', 'meta']);
    }

    /** @test */
    public function a_user_can_get_only_the_fields_of_workflows_that_he_send()
    {
        factory('App\Models\Workflow')->create();

        $this->call('GET', route('workflows.index'), ['fields' => 'uid,name'])
            ->assertStatus(200)
            ->assertJsonStructure([
                'data' => [
                    '*' => [
                        'uid',
                        'name',
                    ]
                ]
            ]);
    }

    /** @test */
    public function a_user_can_filter_the_list_of_workflows()
    {
        factory('App\Models\Workflow')->create([
            'name' => 'ADU'
        ]);

        factory('App\Models\Workflow')->create([
            'name' => 'Financial'
        ]);

        $this->call('GET', route('workflows.index'), ['search' => 'ADU'])
            ->assertStatus(200)
            ->assertJsonFragment(['total' => 1]);
    }

    /** @test */
    public function a_user_can_filter_the_list_of_workflows_by_vertical()
    {
        factory('App\Models\Workflow')->create([
            'name' => 'ADU',
            'vertical' => 'Construction'
        ]);

        factory('App\Models\Workflow')->create([
            'name' => 'Financial',
            'vertical' => 'Financial'
        ]);

        $this->call('GET', route('workflows.index'), ['vertical' => 'Construction'])
            ->assertStatus(200)
            ->assertJsonFragment(['total' => 1]);
    }

    /** @test */
    public function a_user_can_create_a_workflow()
    {
        Storage::fake();

        $attributes = factory("App\Models\Workflow")
            ->raw();

        $attributes['image'] = UploadedFile::fake()->image('image.jpg');
        $attributes['success_image'] = UploadedFile::fake()->image('success_image.jpg');
        $attributes['mobile_icon'] = UploadedFile::fake()->image('mobile_icon.jpg');

        $this->post(route('workflows.store'), $attributes)
            ->assertStatus(201)
            ->assertJsonStructure([
                'data' => ['uid']
            ]);

        $this->assertDatabaseCount('workflows', 1);
    }

    /** @test */
    public function a_user_can_view_a_workflow()
    {
        $workflow = factory('App\Models\Workflow')->create();

        $this->get(route('workflows.show', $workflow))
            ->assertStatus(200)
            ->assertJsonFragment([
                'uid' => $workflow->uid
            ]);
    }

    /** @test */
    public function a_user_cannot_view_a_public_workflow_that_is_protected()
    {
        $workflow = factory('App\Models\Workflow')->create([
            'protected' => true
        ]);

        $this->get(route('public.workflows.steps.show', [
            'slug' => $workflow->slug
        ]))
            ->assertStatus(403);
    }

    /** @test */
    public function a_user_can_view_a_public_workflow_that_is_not_protected()
    {
        $workflow = factory('App\Models\Workflow')->create([
            'protected' => false
        ]);

        $this->get(route('public.workflows.steps.show', [
            'slug' => $workflow->slug
        ]))
            ->assertStatus(200);
    }

    /** @test */
    public function a_user_cant_view_a_workflow_that_doesnt_exists()
    {
        $this->get(route('workflows.show', 1))
            ->assertStatus(404);
    }

    /** @test */
    public function a_user_can_update_a_workflow()
    {
        $workflow = factory('App\Models\Workflow')->create();

        $this->put(route('workflows.update', $workflow), [
            'name' => 'changed',
            'description' => 'changed',
            'public' => false,
            'protected' => false,
            'requires_financing' => false,
            'clone_steps' => false
        ])->assertStatus(200)
            ->assertJsonFragment([
                'name' => 'changed',
                'description' => 'changed',
                'public' => false,
                'protected' => false,
                'requires_financing' => false,
                'clone_steps' => false
            ]);
    }

    /** @test */
    public function a_user_cant_update_a_workflow_that_doesnt_exists()
    {
        $this->put(route('workflows.update', 1), [
            'name' => 'changed'
        ])->assertStatus(404);
    }

    /** @test */
    public function a_user_can_delete_a_workflow()
    {
        $workflow = factory('App\Models\Workflow')->create();

        $this->delete(route('workflows.destroy', $workflow))
            ->assertStatus(200);

        $this->assertSoftDeleted('workflows', [
            'id' =>  $workflow->id
        ]);
    }

    /** @test */
    public function a_user_cant_delete_a_workflow_that_doesnt_exist()
    {
        $this->delete(route('workflows.destroy', 1))
            ->assertStatus(404);
    }

    /** @test */
    public function a_user_can_get_the_workflows_public_steps()
    {
        $workflow = factory('App\Models\Workflow')->create([
            'protected' => false
        ]);

        factory('App\Models\Step')->create([
            'public' => true,
            'workflow_id' => $workflow->id
        ]);

        $this->get(route('public.workflows.steps.show', $workflow->slug))
            ->assertStatus(200)
            ->assertJsonStructure([
                'data' => ['uid']
            ]);
    }


    /** @test */
    public function to_create_and_update_a_workflow_name_is_required()
    {
        $attributes = factory("App\Models\Workflow")
            ->raw([
                'name' => ''
            ]);

        $workflow = factory('App\Models\Workflow')->create();

        $this->post(route('workflows.store'), $attributes)
            ->assertSessionHasErrors('name');

        $this->put(route('workflows.update', $workflow), [
            'name' => ''
        ])->assertSessionHasErrors('name');
    }
}
