<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class PrivateStepsManagementTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function an_api_can_get_a_step()
    {
        $step = factory('App\Models\Step')->create();

        $this->get(route('private.steps.show', $step))
        ->assertStatus(200)
        ->assertJsonFragment([
            'uid' => $step->uid 
        ]);
    }

    /** @test */
    public function an_api_can_get_a_step_values()
    {
        $step = factory('App\Models\Step')->create();
        $section = factory('App\Models\Section')->create([
            'step_id' => $step->id
        ]);
        $input = factory('App\Models\Input')->create([
            'section_id' => $section->id
        ]);

        factory('App\Models\InputValue')->create([
            'input_id' => $input->id,
            'subject_uid' => 'testing',
            'subject_type' => 'testing',
            'subject_api' => 'testing'
        ]);

        $this->get(route('private.steps.values', [
            'step' => $step->uid,
            'subject_uid' => 'testing',
            'subject_type' => 'testing',
            'subject_api' => 'testing'
        ]))->assertStatus(200)
        ->assertJsonFragment([
            'value' => 1
        ]);
    }
}
