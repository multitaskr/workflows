<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStepLogicsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('step_logics', function (Blueprint $table) {
            $table->id();
            $table->uuid('uid')->unique();
            $table->text('logic_data');
            $table->timestamps();
            $table->softDeletes();
            $table->unsignedBigInteger('step_id');

            $table->foreign('step_id')
            ->references('id')
            ->on('steps')
            ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('step_logics');
    }
}
