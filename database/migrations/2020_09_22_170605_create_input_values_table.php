<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInputValuesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('input_values', function (Blueprint $table) {
            $table->id();
            $table->uuid('uid')->unique();
            $table->text('value');
            $table->string('subject_uid');
            $table->string('subject_type');
            $table->string('subject_api');
            $table->timestamps();
            $table->softDeletes();
            $table->unsignedBigInteger('input_id');

            $table->foreign('input_id')
            ->references('id')
            ->on('inputs')
            ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('input_values');
    }
}
