<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSubjectStepsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subject_steps', function (Blueprint $table) {
            $table->id();
            $table->string('uid')->unique();
            $table->string('name');
            $table->integer('order');
            $table->integer('estimated_time')->nullable();
            $table->string('status');
            $table->string('sub_status')->nullable();
            $table->tinyInteger('public')->default(false);
            $table->tinyInteger('customer_edit')->default(false);
            $table->tinyInteger('hidden_from_customer')->default(false);
            $table->timestamp('started_at')->nullable();
            $table->unsignedDouble('time_worked')->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->string('subject_api');
            $table->string('subject_type');
            $table->string('subject_uid');
            $table->bigInteger('step_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('subject_steps');
    }
}
