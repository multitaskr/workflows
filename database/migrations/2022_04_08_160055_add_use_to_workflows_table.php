<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUseToWorkflowsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('workflows', function (Blueprint $table) {
            $table->string('use')->nullable();
        });

        DB::statement("UPDATE workflows SET `use` = ? WHERE vertical IN('Financial','Solar','Construction','Landscape')", ["projects"]);
        DB::statement("UPDATE workflows SET `use` = ? WHERE vertical IN(?)", ["call center", 'Call Center']);
        DB::statement("UPDATE workflows SET `use` = ? WHERE vertical IN(?)", ["reports", 'Reports']);
        DB::statement("UPDATE workflows SET `use` = ? WHERE vertical IN(?)", ["real estate", 'Real Estate']);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('workflows', function (Blueprint $table) {
            $table->dropColumn('use');
        });
    }
}
