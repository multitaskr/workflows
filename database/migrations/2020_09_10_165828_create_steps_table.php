<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStepsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('steps', function (Blueprint $table) {
            $table->id();
            $table->uuid('uid')->uniqe();
            $table->string('name');
            $table->text('description')->nullable();
            $table->integer('order');
            $table->timestamps();
            $table->softDeletes();
            $table->unsignedBigInteger('workflow_id');

            $table->foreign('workflow_id')
            ->references('id')
            ->on('workflows')
            ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('steps');
    }
}
