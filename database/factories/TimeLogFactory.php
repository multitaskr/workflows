<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Carbon\Carbon;
use App\Models\TimeLog;
use Illuminate\Support\Str;
use Faker\Generator as Faker;

$factory->define(TimeLog::class, function (Faker $faker) {
    return [
        'started_at' => Carbon::now(),
        'user_uid' => Str::uuid(),
        'subject_step_id' => function() {
            return factory('App\Models\SubjectStep')->create()->id;
        }
    ];
});
