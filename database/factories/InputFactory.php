<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Input;
use Faker\Generator as Faker;

$factory->define(Input::class, function (Faker $faker) {
    return [
        'title' => $faker->title,
        'data' => json_decode('{"type": "text", "required":true}', true),
        'validations' => 'required|string|max:30',
        'order' => $faker->randomDigit,
        'section_id' => function(){
            return factory('App\Models\Section')->create()->id;
        }
    ];
});
