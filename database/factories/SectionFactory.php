<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Section;
use Faker\Generator as Faker;

$factory->define(Section::class, function (Faker $faker) {
    return [
        'title' => $faker->name,
        'order' => $faker->randomDigit,
        'step_id' =>  function(){
            return factory('App\Models\Step')->create()->id;
        }
    ];
});
