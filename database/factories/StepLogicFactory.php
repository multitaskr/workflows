<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\StepLogic;
use Faker\Generator as Faker;

$factory->define(StepLogic::class, function (Faker $faker) {
    return [
        'logic_data' => '{"if": "812u18js82u", "conditional": "=", "value": "test"}',
        'action' => '{"type": "input", "uid": "92ujww9w9jw", "action": "show"}',
        'step_id' => function(){
            return factory('App\Models\Step')->create()->id;
        }
    ];
});
