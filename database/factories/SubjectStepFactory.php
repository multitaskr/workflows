<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\SubjectStep;
use Faker\Generator as Faker;

$factory->define(SubjectStep::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'order' => $faker->randomDigit,
        'estimated_time' => null,
        'status' => 'pending',
        'sub_status' =>  null,
        'public' => $faker->boolean(),
        'customer_edit' => $faker->boolean(),
        'hidden_from_customer' => $faker->boolean(),
        'subject_api' => 'test',
        'subject_type' => 'Test',
        'subject_uid' => 'abc-123',
        'step_id' => function(){
            return factory('App\Models\Step')->create()->id;
        }
    ];
});
