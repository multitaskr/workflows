<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\InputValue;
use Faker\Generator as Faker;

$factory->define(InputValue::class, function (Faker $faker) {
    return [
        'subject_uid' => $faker->uuid,
        'subject_type' => 'Deal',
        'subject_api' => 'deals',
        'value' => 1,
        'input_id' => function() {
            return factory('App\Models\Input')->create()->id;
        }
    ];
});
