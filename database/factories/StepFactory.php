<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Step;
use Faker\Generator as Faker;

$factory->define(Step::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'description' => $faker->paragraph,
        'order' => $faker->randomDigit,
        'public' => $faker->boolean,
        'required' => false,
        'workflow_id' =>  function(){
            return factory('App\Models\Workflow')->create()->id;
        }
    ];
});
