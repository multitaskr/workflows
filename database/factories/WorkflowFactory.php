<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Workflow;
use Illuminate\Support\Str;
use Faker\Generator as Faker;

$factory->define(Workflow::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'description' => $faker->paragraph,
        'vertical' => $faker->word,
        'public' => $faker->boolean,
        'protected' => $faker->boolean,
        'use' => $faker->word(),
        'requires_financing' => $faker->boolean,
        'clone_steps' => $faker->boolean
    ];
});
