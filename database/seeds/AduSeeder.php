<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AduSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $workflow_id = DB::table('workflows')->insertGetId([
            'uid' => 'e2ad6d7d-b5f8-4c48-82d4-d8994e376956',
            'name' => 'ADU',
            'slug' => 'adu',
            'description' => 'Generate extra income from a second living space',
            'image' => 'https://cdn.develop.gomultitaskr.com/testing/workflows/wizard%20adu%20services.jpg',
            'success_image' => 'https://cdn.develop.gomultitaskr.com/testing/workflows/success/adu.svg',
            'public' => 1,
            'order' => 1,
            'vertical' => 'Construction',
            'created_at' => now(),
            'updated_at' => now(),
            'use' => 'projects'
        ]);

        $step_id = DB::table('steps')->insertGetId([
            'uid' => '90907f0a-410c-49f7-bcff-060befe48082',
            'name' => 'Customer Info Questionnaire',
            'order' => 1,
            'public' => true,
            'workflow_id' => $workflow_id
        ]);

        $section = factory(\App\Models\Section::class)->create([
            'title' => 'Types of ADUs',
            'description' => "What kind of ADU would you like to build?",
            'order' => 1,
            'step_id' => $step_id
        ]);

        factory(\App\Models\Input::class)->create([
            'title' => 'Types of ADUs',
            'data' =>  json_decode('{
                "type": "radio", 
                "required":true,
                "label": "Types of ADUs",
                "description": null,
                "image": false,
                "place_holder": null,
                "with_images": true,
                "hide_label": true,
                "show": true,
                "options": [
                    {
                        "image": "https://gomultitaskr.sfo2.digitaloceanspaces.com/workflows/types-of-adus-detached-unit.svg",
                        "value": "Detached Unit",
                        "text": "Detached Unit",
                        "sub_text": "A modern private livable space"
                    },
                    {
                        "image": "https://gomultitaskr.sfo2.digitaloceanspaces.com/workflows/types-of-adus-garage-conversion.svg",
                        "value": "Garage Conversion",
                        "text": "Garage Conversion",
                        "sub_text": "Convert to add more space"
                    },
                    {
                        "image": "https://gomultitaskr.sfo2.digitaloceanspaces.com/workflows/types-of-adus-over-the-garage.svg",
                        "value": "Over The Garage",
                        "text": "Over The Garage",
                        "sub_text": "Build above your existing garage"
                    },
                    {
                        "image": "https://gomultitaskr.sfo2.digitaloceanspaces.com/workflows/types-of-adus-basement-or-attic.svg",
                        "value": "Basement or Attic",
                        "text": "Basement or Attic",
                        "sub_text": "Make abandoned space livable"
                    },
                    {
                        "image": "https://gomultitaskr.sfo2.digitaloceanspaces.com/workflows/adu-office-unit.svg",
                        "value": "Office Unit",
                        "text": "Office Unit",
                        "sub_text": "Work remote from a backyard office"
                    },
                    {
                        "image": "https://cdn.develop.gomultitaskr.com/testing/workflows/ADU/ADU_multifamiliar.svg",
                        "value": "Multifamily Units",
                        "text": "Multifamily Units",
                        "sub_text": "Make your outdoor space dought-tolerant"
                    }
                ]
            }', true),
            'validations' => 'required',
            'section_id' => $section->id,
            'order' => 1,
            'tag' => 'adu-type'
        ]);

        $section = factory(\App\Models\Section::class)->create([
            'title' => 'Project Details',
            'description' => "Let’s review your plan & some details to start your ADU consultation.",
            'order' => 2,
            'step_id' => $step_id
        ]);

        factory(\App\Models\Input::class)->create([
            'title' => 'Purpose',
            'data' =>  json_decode('{
                "type": "radio", 
                "required":true,
                "label": "What is the purpose of this construction or remodel?",
                "description": null,
                "image": false,
                "place_holder": null,
                "with_images": false,
                "hide_label": false,
                "show": true,
                "options": [
                    {
                        "image": null,
                        "value": "Rental",
                        "text": "Rental"
                    },
                    {
                        "image": null,
                        "value": "More Space",
                        "text": "More Space"
                    },
                    {
                        "image": null,
                        "value": "Needed Remodel or Repair",
                        "text": "Needed Remodel or Repair"
                    },
                    {
                        "image": null,
                        "value": "Add Value",
                        "text": "Add Value"
                    },
                    {
                        "image": null,
                        "value": "Owner Choice",
                        "text": "Owner Choice"
                    }
                ]
            }', true),
            'validations' => 'required',
            'tag' => 'adu-purpouse',
            'section_id' => $section->id,
            'order' => 1
        ]);


        factory(\App\Models\Input::class)->create([
            'title' => 'Owner authorized',
            'data' =>  json_decode('{
                "type": "radio", 
                "required":true,
                "label": "Are you the owner or authorized to make the property changes?",
                "description": null,
                "image": false,
                "place_holder": null,
                "with_images": false,
                "hide_label": false,
                "show": true,
                "options": [
                    {
                        "image": null,
                        "value": "Yes",
                        "text": "Yes"
                    },
                    {
                        "image": null,
                        "value": "No",
                        "text": "No"
                    }
                ]
            }', true),
            'validations' => 'required',
            'section_id' => $section->id,
            'order' => 2
        ]);

        factory(\App\Models\Input::class)->create([
            'title' => 'Stage',
            'data' =>  json_decode('{
                "type": "radio", 
                "required":true,
                "label": "What stage are you currently at for your home project?",
                "description": null,
                "image": false,
                "place_holder": null,
                "with_images": false,
                "hide_label": false,
                "show": true,
                "options": [
                    {
                        "image": null,
                        "value": "Design and Budget",
                        "text": "Design and Budget"
                    },
                    {
                        "image": null,
                        "value": "Ready to Hire",
                        "text": "Ready to Hire"
                    },
                    {
                        "image": null,
                        "value": "Comparing Bids",
                        "text": "Comparing Bids"
                    }
                ]
            }', true),
            'validations' => 'required',
            'section_id' => $section->id,
            'order' => 3
        ]);

        factory(\App\Models\Input::class)->create([
            'title' => 'Bedrooms',
            'data' =>  json_decode('{
                "type": "radio", 
                "required":true,
                "label": "How many bedrooms would you like?",
                "description": null,
                "image": false,
                "place_holder": null,
                "with_images": false,
                "hide_label": false,
                "show": true,
                "options": [
                    {
                        "image": null,
                        "value": "Studio",
                        "text": "Studio"
                    },
                    {
                        "image": null,
                        "value": "1 Bedroom",
                        "text": "1 Bedroom"
                    },
                    {
                        "image": null,
                        "value": "2 Bedrooms",
                        "text": "2 Bedrooms"
                    },
                    {
                        "image": null,
                        "value": "3 Bedrooms",
                        "text": "3 Bedrooms"
                    }
                ]
            }', true),
            'validations' => 'required',
            'tag' => 'adu-bedrooms',
            'section_id' => $section->id,
            'order' => 4
        ]);

        factory(\App\Models\Input::class)->create([
            'title' => 'Bathrooms',
            'data' =>  json_decode('{
                "type": "radio", 
                "required":true,
                "label": "How many bathrooms would you like?",
                "description": null,
                "image": false,
                "place_holder": null,
                "with_images": false,
                "hide_label": false,
                "show": true,
                "options": [
                    {
                        "image": null,
                        "value": "No bathroom",
                        "text": "No bathroom"
                    },
                    {
                        "image": null,
                        "value": "Half bathroom",
                        "text": "Half bathroom"
                    },
                    {
                        "image": null,
                        "value": "1 Bathroom",
                        "text": "1 Bathroom"
                    },
                    {
                        "image": null,
                        "value": "2 Bathrooms",
                        "text": "2 Bathrooms"
                    }
                ]
            }', true),
            'validations' => 'required',
            'tag' => 'adu-baths',
            'section_id' => $section->id,
            'order' => 5
        ]);

        factory(\App\Models\Input::class)->create([
            'title' => 'Tier',
            'data' =>  json_decode('{
                "type": "radio", 
                "required":true,
                "label": "Which finish do you prefer?",
                "description": null,
                "image": false,
                "place_holder": null,
                "with_images": true,
                "hide_label": false,
                "show": true,
                "options": [
                    {
                        "image": "https://gomultitaskr.sfo2.digitaloceanspaces.com/workflows/adus-exterior-finish-standard.svg",
                        "value": "STANDARD",
                        "text": "Standard",
                        "sub_text": "Entry-level appliances & fixtures"
                    },
                    {
                        "image": "https://gomultitaskr.sfo2.digitaloceanspaces.com/workflows/adus-exterior-finish-medium.svg",
                        "value": "MEDIUM",
                        "text": "Medium",
                        "sub_text": "Mid-level appliances & fixtures"
                    },
                    {
                        "image": "https://gomultitaskr.sfo2.digitaloceanspaces.com/workflows/adus-exterior-finish-premium.svg",
                        "value": "PREMIUM",
                        "text": "Premium",
                        "sub_text": "High-level appliances & fixtures"
                    }
                ]
            }', true),
            'validations' => 'required',
            'tag' => 'adu-tier',
            'section_id' => $section->id,
            'order' => 6
        ]);

        factory(\App\Models\Input::class)->create([
            'title' => 'Start',
            'data' =>  json_decode('{
                "type": "radio", 
                "required":true,
                "label": "When would you like to start this project?",
                "description": null,
                "image": false,
                "place_holder": null,
                "with_images": false,
                "hide_label": false,
                "show": true,
                "options": [
                    {
                        "image": null,
                        "value": "0 - 3 Months",
                        "text": "0 - 3 Months"
                    },
                    {
                        "image": null,
                        "value": "3 - 6 Months",
                        "text": "3 - 6 Months"
                    },
                    {
                        "image": null,
                        "value": "6 - 12 Months",
                        "text": "6 - 12 Months"
                    }
                ]
            }', true),
            'validations' => 'required',
            'section_id' => $section->id,
            'order' => 7
        ]);

        factory(\App\Models\Input::class)->create([
            'title' => 'Budget',
            'data' =>  json_decode('{
                "type": "textarea", 
                "required":true,
                "label": "Do you have a Budget in mind for this project?",
                "description": null,
                "image": false,
                "place_holder": null,
                "with_images": false,
                "hide_label": false,
                "show": true
            }', true),
            'validations' => 'required',
            'section_id' => $section->id,
            'order' => 8
        ]);

        factory(\App\Models\Input::class)->create([
            'title' => 'Extra details that you would like to share',
            'data' =>  json_decode('{
                "type": "textarea", 
                "required":false,
                "label": "Extra details that you would like to share",
                "description": null,
                "image": false,
                "place_holder": "",
                "with_images": false,
                "hide_label": false,
                "show": true
            }', true),
            'validations' => 'nullable',
            'section_id' => $section->id,
            'order' => 9
        ]);

        $step_id = DB::table('steps')->insertGetId([
            'uid' => '814da102-81d0-4c31-bd57-dc039cfee173',
            'name' => 'Project Analysis Cost',
            'order' => 2,
            'public' => false,
            'workflow_id' => $workflow_id
        ]);

        $section = factory(\App\Models\Section::class)->create([
            'title' => 'Project Analysis Cost',
            'description' => null,
            'order' => 1,
            'step_id' => $step_id
        ]);

        factory(\App\Models\Input::class)->create([
            'title' => 'Project Analysis Cost',
            'data' =>  json_decode('{
                "type": "preexp", 
                "required":false,
                "label": "",
                "description": null,
                "image": false,
                "place_holder": null,
                "with_images": false,
                "hide_label": false,
                "show": true
            }', true),
            'order' => 1,
            'validations' => 'nullable',
            'section_id' => $section->id,
        ]);

        $step_id = DB::table('steps')->insertGetId([
            'uid' => 'e24297d4-5279-45bb-a3ee-adb578bcfd27',
            'name' => 'Post-Exploration Questionnaire',
            'order' => 3,
            'public' => false,
            'workflow_id' => $workflow_id
        ]);

        $section = factory(\App\Models\Section::class)->create([
            'title' => 'Post-exp',
            'description' => null,
            'order' => 1,
            'step_id' => $step_id
        ]);

        factory(\App\Models\Input::class)->create([
            'title' => 'Please describe all the details of the Custom ADU below',
            'data' =>  json_decode('{
                "type": "textarea", 
                "required":true,
                "label": "Please describe all the details of the Custom ADU below",
                "description": null,
                "image": false,
                "place_holder": "Explain in detail your main objectives.",
                "with_images": false,
                "hide_label": false,
                "show": true
            }', true),
            'validations' => 'required',
            'section_id' => $section->id,
            'order' => 1
        ]);


        factory(\App\Models\Input::class)->create([
            'title' => "Please upload 4 pictures around the desired work area",
            'data' =>  json_decode('{
                "type": "imagepicker", 
                "required":true,
                "label": "Please upload 4 pictures around the desired work area",
                "description": null,
                "image": false,
                "place_holder": null,
                "with_images": false,
                "hide_label": false,
                "show": true
            }', true),
            'order' => 1,
            'validations' => 'required',
            'section_id' => $section->id,
        ]);

        $step_id = DB::table('steps')->insertGetId([
            'uid' => '1e282f03-8114-4e99-a34f-0609f0bd17fb',
            'name' => 'Project Development Plan (Contract)',
            'order' => 4,
            'public' => false,
            'workflow_id' => $workflow_id
        ]);

        $section = factory(\App\Models\Section::class)->create([
            'title' => 'Project Development Plan (Contract)',
            'description' => null,
            'order' => 1,
            'step_id' => $step_id
        ]);

        factory(\App\Models\Input::class)->create([
            'title' => 'Project Development Plan',
            'data' =>  json_decode('{
                "type": "quick-books", 
                "required":true,
                "label": "",
                "description": null,
                "image": false,
                "place_holder": null,
                "with_images": false,
                "hide_label": false,
                "show": true
            }', true),
            'order' => 1,
            'validations' => 'required',
            'section_id' => $section->id,
        ]);

        $step_id = DB::table('steps')->insertGetId([
            'uid' => '0e25087e-4f10-4f9f-90d2-8d5c29ae0816',
            'name' => 'Home Diagnostic Report',
            'order' => 5,
            'public' => false,
            'workflow_id' => $workflow_id,
            'preserve_input_values' => true
        ]);

        $section = factory(\App\Models\Section::class)->create([
            'title' => 'Home Diagnostic Report',
            'description' => null,
            'order' => 1,
            'step_id' => $step_id
        ]);

        factory(\App\Models\Input::class)->create([
            'title' => 'Please add link to Matterport.',
            'data' =>  json_decode('{
                "type": "text", 
                "required":true,
                "label": "Please add link to Matterport.",
                "description": null,
                "image": false,
                "place_holder": null,
                "with_images": false,
                "hide_label": false,
                "show": true
            }', true),
            'validations' => 'required',
            'section_id' => $section->id,
            'order' => 1
        ]);

        factory(\App\Models\Input::class)->create([
            'title' => 'Please add link to Drone Deploy.',
            'data' =>  json_decode('{
                "type": "text", 
                "required":true,
                "label": "Please add link to Drone Deploy.",
                "description": null,
                "image": false,
                "place_holder": null,
                "with_images": false,
                "hide_label": false,
                "show": true
            }', true),
            'validations' => 'required',
            'section_id' => $section->id,
            'order' => 2
        ]);

        factory(\App\Models\Input::class)->create([
            'title' => 'Please add link to Home Inspection Report.',
            'data' =>  json_decode('{
                "type": "text", 
                "required":true,
                "label": "Please add link to Home Inspection Report.",
                "description": null,
                "image": false,
                "place_holder": null,
                "with_images": false,
                "hide_label": false,
                "show": true
            }', true),
            'validations' => 'required',
            'section_id' => $section->id,
            'order' => 3
        ]);

        $step_id = DB::table('steps')->insertGetId([
            'uid' => '85992c8f-c4b2-4d99-96cb-416e7f33e54c',
            'name' => '2D Design',
            'order' => 6,
            'public' => false,
            'workflow_id' => $workflow_id,
            'preserve_input_values' => true
        ]);

        $section = factory(\App\Models\Section::class)->create([
            'title' => '2D Design',
            'description' => null,
            'order' => 1,
            'step_id' => $step_id
        ]);

        factory(\App\Models\Input::class)->create([
            'title' => 'Please add file',
            'data' =>  json_decode('{
                "type": "file", 
                "required":true,
                "label": "Please add file",
                "description": null,
                "image": false,
                "place_holder": null,
                "with_images": false,
                "hide_label": false,
                "show": true
            }', true),
            'order' => 1,
            'validations' => 'required',
            'section_id' => $section->id,
        ]);

        $step_id = DB::table('steps')->insertGetId([
            'uid' => '4f830d8e-513b-437e-a7ac-8fab5808eb8b',
            'name' => 'Estimation',
            'order' => 7,
            'public' => false,
            'workflow_id' => $workflow_id,
            'preserve_input_values' => true
        ]);

        $section = factory(\App\Models\Section::class)->create([
            'title' => 'Estimation',
            'description' => null,
            'order' => 1,
            'step_id' => $step_id
        ]);

        factory(\App\Models\Input::class)->create([
            'title' => 'Please add file',
            'data' =>  json_decode('{
                "type": "file", 
                "required":true,
                "label": "Please add file",
                "description": null,
                "image": false,
                "place_holder": null,
                "with_images": false,
                "hide_label": false,
                "show": true
            }', true),
            'order' => 1,
            'validations' => 'required',
            'section_id' => $section->id,
        ]);

        $step_id = DB::table('steps')->insertGetId([
            'uid' => 'c0be0e5f-9524-40ab-8a3f-b7772d062160',
            'name' => 'Proposal',
            'order' => 8,
            'public' => false,
            'workflow_id' => $workflow_id,
            'preserve_input_values' => true
        ]);

        $section = factory(\App\Models\Section::class)->create([
            'title' => 'Proposal',
            'description' => null,
            'order' => 1,
            'step_id' => $step_id
        ]);

        factory(\App\Models\Input::class)->create([
            'title' => 'Please add file',
            'data' =>  json_decode('{
                "type": "file", 
                "required":true,
                "label": "Please add file",
                "description": null,
                "image": false,
                "place_holder": null,
                "with_images": false,
                "hide_label": false,
                "show": true
            }', true),
            'order' => 1,
            'validations' => 'required',
            'section_id' => $section->id,
        ]);

        $step_id = DB::table('steps')->insertGetId([
            'uid' => '5bf45b9d-94b6-4fd3-9643-9f40becfad95',
            'name' => 'Virtual Reality (Render)',
            'order' => 9,
            'public' => false,
            'workflow_id' => $workflow_id,
            'preserve_input_values' => true
        ]);

        $section = factory(\App\Models\Section::class)->create([
            'title' => 'Virtual Reality (Render)',
            'description' => null,
            'order' => 1,
            'step_id' => $step_id
        ]);

        factory(\App\Models\Input::class)->create([
            'title' => 'Please add link to Shapespark',
            'data' =>  json_decode('{
                "type": "text", 
                "required":true,
                "label": "Please add link to Shapespark",
                "description": null,
                "image": false,
                "place_holder": null,
                "with_images": false,
                "hide_label": false,
                "show": true
            }', true),
            'validations' => 'required',
            'section_id' => $section->id,
            'order' => 1
        ]);

        $step_id = DB::table('steps')->insertGetId([
            'uid' => '6e8b5acf-f88a-47fe-bfa7-34be3a3f3af7',
            'name' => 'Project Development Plan',
            'order' => 10,
            'public' => false,
            'workflow_id' => $workflow_id
        ]);

        $section = factory(\App\Models\Section::class)->create([
            'title' => 'Project Development Plan',
            'description' => null,
            'order' => 1,
            'step_id' => $step_id
        ]);

        factory(\App\Models\Input::class)->create([
            'title' => 'Please add the link',
            'data' =>  json_decode('{
                "type": "text", 
                "required":true,
                "label": "Please add the link",
                "description": null,
                "image": false,
                "place_holder": null,
                "with_images": false,
                "hide_label": false,
                "show": true
            }', true),
            'validations' => 'required',
            'section_id' => $section->id,
            'order' => 1
        ]);
    }
}
