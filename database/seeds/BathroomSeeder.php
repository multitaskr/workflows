<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class BathroomSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $workflow_id = DB::table('workflows')->insertGetId([
            'uid' => '5a8f4e68-abc3-4c4d-9d1f-270306239d5c',
            'name' => 'Bathroom',
            'slug' => 'bathroom',
            'description' => 'Design your most used space with water-savings',
            'image' => 'https://cdn.develop.gomultitaskr.com/testing/workflows/wizard%20bathroom%20services.jpg',
            'success_image' => 'https://cdn.develop.gomultitaskr.com/testing/workflows/success/bathroom.svg',
            'public' => 1,
            'order' => 3,
            'vertical' => 'Construction',
            'created_at' => now(),
            'updated_at' => now(),
            'use' => 'projects'
        ]);

        $step_id = DB::table('steps')->insertGetId([
            'uid' => 'a8c90c59-4853-4a8e-be73-5c89114fa65e',
            'name' => 'BATHROOM INFO',
            'order' => 1,
            'public' => true,
            'workflow_id' => $workflow_id
        ]);

        $section = factory(\App\Models\Section::class)->create([
            'title' => 'Type of remodel',
            'description' => "Remodel Type?",
            'order' => 1,
            'step_id' => $step_id
        ]);

        factory(\App\Models\Input::class)->create([
            'title' => 'Type of Bathroom',
            'data' =>  json_decode('{
                "type": "radio", 
                "required":true,
                "label": "Remodel Type",
                "description": null,
                "image": false,
                "place_holder": null,
                "with_images": true,
                "hide_label": true,
                "show": true,
                "options": [
                    {
                        "image": "https://cdn.develop.gomultitaskr.com/testing/workflows/bathroom/bathroom_full_remodel.svg",
                        "value": "Full Remodel",
                        "text": "Full Remodel",
                        "sub_text": "Create your own spa-like sanctuary"
                    },
                    {
                        "image": "https://cdn.develop.gomultitaskr.com/testing/workflows/bathroom/bathroom_partial_remodel.svg",
                        "value": "Partial Remodel",
                        "text": "Partial Remodel",
                        "sub_text": "Improve an area of your most personal space"
                    }
                ]
            }', true),
            'validations' => 'required',
            'section_id' => $section->id,
            'order' => 1
        ]);

        $section = factory(\App\Models\Section::class)->create([
            'title' => 'Project Details',
            'description' => "Now let's review your plan & details to complete your Bathroom construction",
            'order' => 2,
            'step_id' => $step_id
        ]);

        factory(\App\Models\Input::class)->create([
            'title' => 'Owner authorized',
            'data' =>  json_decode('{
                "type": "radio", 
                "required":true,
                "label": "Are you the owner or authorized to make property changes?",
                "description": null,
                "image": false,
                "place_holder": null,
                "with_images": false,
                "hide_label": false,
                "show": true,
                "options": [
                    {
                        "image": null,
                        "value": "Yes",
                        "text": "Yes"
                    },
                    {
                        "image": null,
                        "value": "No",
                        "text": "No"
                    }
                ]
            }', true),
            'validations' => 'required',
            'section_id' => $section->id,
            'order' => 1
        ]);

        $inputLayout = factory(\App\Models\Input::class)->create([
            'title' => 'Layout',
            'data' =>  json_decode('{
                "type": "radio", 
                "required":true,
                "label": "Changing layout or just upgrades?",
                "description": null,
                "image": false,
                "place_holder": null,
                "with_images": false,
                "hide_label": false,
                "show": true,
                "options": [
                    {
                        "image": null,
                        "value": "Changing Layout",
                        "text": "Changing Layout"
                    },
                    {
                        "image": null,
                        "value": "Upgrades",
                        "text": "Upgrades"
                    }
                ]
            }', true),
            'validations' => 'required',
            'section_id' => $section->id,
            'order' => 2
        ]);

        $inputUpgrades = factory(\App\Models\Input::class)->create([
            'title' => 'Upgrades',
            'data' =>  json_decode('{
                "type": "checkbox", 
                "required":true,
                "label": "What areas will be upgraded?",
                "description": null,
                "image": false,
                "place_holder": null,
                "with_images": false,
                "hide_label": false,
                "show": false,
                "options": [
                    {
                        "image": null,
                        "value": "Flooring",
                        "text": "Flooring"
                    },
                    {
                        "image": null,
                        "value": "Shower",
                        "text": "Shower"
                    },
                    {
                        "image": null,
                        "value": "Tub",
                        "text": "Tub"
                    },
                    {
                        "image": null,
                        "value": "Vanity",
                        "text": "Vanity"
                    }
                ]
            }', true),
            'validations' => 'required',
            'section_id' => $section->id,
            'order' => 3
        ]);

        factory(\App\Models\StepLogic::class)->create([
            'logic_data' => json_decode('{"if": "' . $inputLayout->uid . '", "conditional": "=", "value": "Upgrades"}', true),
            'action' => json_decode('{"type": "input", "uid": "' . $inputUpgrades->uid . '", "action": "show"}', true),
            'step_id' => $step_id
        ]);

        factory(\App\Models\Input::class)->create([
            'title' => 'Stage',
            'data' =>  json_decode('{
                "type": "radio", 
                "required":true,
                "label": "What stage are you at in your home project?",
                "description": null,
                "image": false,
                "place_holder": null,
                "with_images": false,
                "hide_label": false,
                "show": true,
                "options": [
                    {
                        "image": null,
                        "value": "Design and Budget",
                        "text": "Design and Budget"
                    },
                    {
                        "image": null,
                        "value": "Ready to Hire",
                        "text": "Ready to Hire"
                    },
                    {
                        "image": null,
                        "value": "Comparing Bids",
                        "text": "Comparing Bids"
                    }
                ]
            }', true),
            'validations' => 'required',
            'section_id' => $section->id,
            'order' => 4
        ]);

        factory(\App\Models\Input::class)->create([
            'title' => 'Start',
            'data' =>  json_decode('{
                "type": "radio", 
                "required":true,
                "label": "When you like to start this project?",
                "description": null,
                "image": false,
                "place_holder": null,
                "with_images": false,
                "hide_label": false,
                "show": true,
                "options": [
                    {
                        "image": null,
                        "value": "0 - 3 Months",
                        "text": "0 - 3 Months"
                    },
                    {
                        "image": null,
                        "value": "3 - 6 Months",
                        "text": "3 - 6 Months"
                    },
                    {
                        "image": null,
                        "value": "6 - 12 Months",
                        "text": "6 - 12 Months"
                    }
                ]
            }', true),
            'validations' => 'required',
            'section_id' => $section->id,
            'order' => 5
        ]);

        factory(\App\Models\Input::class)->create([
            'title' => 'Budget',
            'data' =>  json_decode('{
                "type": "textarea", 
                "required":true,
                "label": "Do you have a Budget in mind for this project?",
                "description": null,
                "image": false,
                "place_holder": null,
                "with_images": false,
                "hide_label": false,
                "show": true
            }', true),
            'validations' => 'required',
            'section_id' => $section->id,
            'order' => 6
        ]);

        factory(\App\Models\Input::class)->create([
            'title' => 'Extra details that you would like to share',
            'data' =>  json_decode('{
                "type": "textarea", 
                "required":false,
                "label": "Extra details that you would like to share",
                "description": null,
                "image": false,
                "place_holder": "",
                "with_images": false,
                "hide_label": false,
                "show": true
            }', true),
            'validations' => 'nullable',
            'section_id' => $section->id,
            'order' => 7
        ]);


        $step_id = DB::table('steps')->insertGetId([
            'uid' => '813c0f0d-175e-475b-beae-1c00dd4ae497',
            'name' => 'Pre-Proposal Document',
            'order' => 2,
            'public' => false,
            'workflow_id' => $workflow_id
        ]);

        $section = factory(\App\Models\Section::class)->create([
            'title' => 'Pre-exp',
            'description' => null,
            'order' => 1,
            'step_id' => $step_id
        ]);

        factory(\App\Models\Input::class)->create([
            'title' => 'Pre-exp',
            'data' =>  json_decode('{
                "type": "preexp", 
                "required":false,
                "label": "",
                "description": null,
                "image": false,
                "place_holder": null,
                "with_images": false,
                "hide_label": false,
                "show": true
            }', true),
            'order' => 1,
            'validations' => 'nullable',
            'section_id' => $section->id,
        ]);

        $step_id = DB::table('steps')->insertGetId([
            'uid' => '85df71ea-e8b4-433e-b86d-f1e72e5f1eab',
            'name' => 'Post-Exploration Questionnaire',
            'order' => 3,
            'public' => false,
            'workflow_id' => $workflow_id
        ]);

        $section = factory(\App\Models\Section::class)->create([
            'title' => 'Post-exp',
            'description' => null,
            'order' => 1,
            'step_id' => $step_id
        ]);

        factory(\App\Models\Input::class)->create([
            'title' => 'Please describe all the details of the Custom ADU below',
            'data' =>  json_decode('{
                "type": "textarea", 
                "required":true,
                "label": "Please describe all the details of the Custom ADU below",
                "description": null,
                "image": false,
                "place_holder": "Explain in detail your main objectives.",
                "with_images": false,
                "hide_label": false,
                "show": true
            }', true),
            'validations' => 'required',
            'section_id' => $section->id,
            'order' => 1
        ]);


        factory(\App\Models\Input::class)->create([
            'title' => "Please upload 4 pictures around the desired work area",
            'data' =>  json_decode('{
                "type": "imagepicker", 
                "required":true,
                "label": "Please upload 4 pictures around the desired work area",
                "description": null,
                "image": false,
                "place_holder": null,
                "with_images": false,
                "hide_label": false,
                "show": true
            }', true),
            'order' => 1,
            'validations' => 'required',
            'section_id' => $section->id,
        ]);

        $step_id = DB::table('steps')->insertGetId([
            'uid' => 'b52aa670-5861-4822-a6b0-358181d456ca',
            'name' => 'Project Development Plan (Contract)',
            'order' => 4,
            'public' => false,
            'workflow_id' => $workflow_id
        ]);

        $section = factory(\App\Models\Section::class)->create([
            'title' => 'Project Development Plan (Contract)',
            'description' => null,
            'order' => 1,
            'step_id' => $step_id
        ]);

        factory(\App\Models\Input::class)->create([
            'title' => 'Project Development Plan',
            'data' =>  json_decode('{
                "type": "quick-books", 
                "required":true,
                "label": "Please add file",
                "description": null,
                "image": false,
                "place_holder": null,
                "with_images": false,
                "hide_label": false,
                "show": true
            }', true),
            'order' => 1,
            'validations' => 'required',
            'section_id' => $section->id,
        ]);

        $step_id = DB::table('steps')->insertGetId([
            'uid' => 'b7f69c02-46c7-4cb9-97dd-704525cb0c24',
            'name' => 'Home Diagnostic Report',
            'order' => 5,
            'public' => false,
            'workflow_id' => $workflow_id,
            'preserve_input_values' => true
        ]);

        $section = factory(\App\Models\Section::class)->create([
            'title' => 'Home Diagnostic Report',
            'description' => null,
            'order' => 1,
            'step_id' => $step_id
        ]);

        factory(\App\Models\Input::class)->create([
            'title' => 'Please add link to Matterport.',
            'data' =>  json_decode('{
                "type": "text", 
                "required":true,
                "label": "Please add link to Matterport.",
                "description": null,
                "image": false,
                "place_holder": null,
                "with_images": false,
                "hide_label": false,
                "show": true
            }', true),
            'validations' => 'required',
            'section_id' => $section->id,
            'order' => 1
        ]);

        factory(\App\Models\Input::class)->create([
            'title' => 'Please add link to Drone Deploy.',
            'data' =>  json_decode('{
                "type": "text", 
                "required":true,
                "label": "Please add link to Drone Deploy.",
                "description": null,
                "image": false,
                "place_holder": null,
                "with_images": false,
                "hide_label": false,
                "show": true
            }', true),
            'validations' => 'required',
            'section_id' => $section->id,
            'order' => 2
        ]);

        factory(\App\Models\Input::class)->create([
            'title' => 'Please add link to Home Inspection Report.',
            'data' =>  json_decode('{
                "type": "text", 
                "required":true,
                "label": "Please add link to Home Inspection Report.",
                "description": null,
                "image": false,
                "place_holder": null,
                "with_images": false,
                "hide_label": false,
                "show": true
            }', true),
            'validations' => 'required',
            'section_id' => $section->id,
            'order' => 3
        ]);

        $step_id = DB::table('steps')->insertGetId([
            'uid' => '93ae4556-b0d7-4a7c-ac9d-c724fadb79e4',
            'name' => '2D Design',
            'order' => 6,
            'public' => false,
            'workflow_id' => $workflow_id,
            'preserve_input_values' => true
        ]);

        $section = factory(\App\Models\Section::class)->create([
            'title' => '2D Design',
            'description' => null,
            'order' => 1,
            'step_id' => $step_id
        ]);

        factory(\App\Models\Input::class)->create([
            'title' => 'Please add file',
            'data' =>  json_decode('{
                "type": "file", 
                "required":true,
                "label": "Please add file",
                "description": null,
                "image": false,
                "place_holder": null,
                "with_images": false,
                "hide_label": false,
                "show": true
            }', true),
            'order' => 1,
            'validations' => 'required',
            'section_id' => $section->id,
        ]);

        $step_id = DB::table('steps')->insertGetId([
            'uid' => '3fb76a0e-fda9-4ead-b09b-04d74f2d3b49',
            'name' => 'Estimation',
            'order' => 7,
            'public' => false,
            'workflow_id' => $workflow_id,
            'preserve_input_values' => true
        ]);

        $section = factory(\App\Models\Section::class)->create([
            'title' => 'Estimation',
            'description' => null,
            'order' => 1,
            'step_id' => $step_id
        ]);

        factory(\App\Models\Input::class)->create([
            'title' => 'Please add file',
            'data' =>  json_decode('{
                "type": "file", 
                "required":true,
                "label": "Please add file",
                "description": null,
                "image": false,
                "place_holder": null,
                "with_images": false,
                "hide_label": false,
                "show": true
            }', true),
            'order' => 1,
            'validations' => 'required',
            'section_id' => $section->id,
        ]);

        $step_id = DB::table('steps')->insertGetId([
            'uid' => 'd8fb14c2-777f-4970-ba21-8915c5914bd2',
            'name' => 'Proposal',
            'order' => 8,
            'public' => false,
            'workflow_id' => $workflow_id,
            'preserve_input_values' => true
        ]);

        $section = factory(\App\Models\Section::class)->create([
            'title' => 'Proposal',
            'description' => null,
            'order' => 1,
            'step_id' => $step_id
        ]);

        factory(\App\Models\Input::class)->create([
            'title' => 'Please add file',
            'data' =>  json_decode('{
                "type": "file", 
                "required":true,
                "label": "Please add file",
                "description": null,
                "image": false,
                "place_holder": null,
                "with_images": false,
                "hide_label": false,
                "show": true
            }', true),
            'order' => 1,
            'validations' => 'required',
            'section_id' => $section->id,
        ]);

        $step_id = DB::table('steps')->insertGetId([
            'uid' => 'c26bf200-2275-4c72-8d35-731c835eb8cd',
            'name' => 'Virtual Reality (Render)',
            'order' => 9,
            'public' => false,
            'workflow_id' => $workflow_id,
            'preserve_input_values' => true
        ]);

        $section = factory(\App\Models\Section::class)->create([
            'title' => 'Virtual Reality (Render)',
            'description' => null,
            'order' => 1,
            'step_id' => $step_id
        ]);

        factory(\App\Models\Input::class)->create([
            'title' => 'Please add link to Shapespark',
            'data' =>  json_decode('{
                "type": "text", 
                "required":true,
                "label": "Please add link to Shapespark",
                "description": null,
                "image": false,
                "place_holder": null,
                "with_images": false,
                "hide_label": false,
                "show": true
            }', true),
            'validations' => 'required',
            'section_id' => $section->id,
            'order' => 1
        ]);

        $step_id = DB::table('steps')->insertGetId([
            'uid' => 'c34f9580-0187-458f-88c4-1c23123a89df',
            'name' => 'Project Development Plan',
            'order' => 10,
            'public' => false,
            'workflow_id' => $workflow_id
        ]);

        $section = factory(\App\Models\Section::class)->create([
            'title' => 'Project Development Plan',
            'description' => null,
            'order' => 1,
            'step_id' => $step_id
        ]);

        factory(\App\Models\Input::class)->create([
            'title' => 'Please add the link',
            'data' =>  json_decode('{
                "type": "text", 
                "required":true,
                "label": "Please add the link",
                "description": null,
                "image": false,
                "place_holder": null,
                "with_images": false,
                "hide_label": false,
                "show": true
            }', true),
            'validations' => 'required',
            'section_id' => $section->id,
            'order' => 1
        ]);
    }
}
