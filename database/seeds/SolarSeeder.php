<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SolarSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $workflow_id = DB::table('workflows')->insertGetId([
            'uid' => '7760b9c6-5c69-4807-b9b2-81125cca9057',
            'name' => 'Solar',
            'slug' => 'solar',
            'description' => 'Uncover electricity saving with solar panel deals',
            'image' => 'https://cdn.develop.gomultitaskr.com/testing/workflows/wizard%20solar%20services.jpg',
            'success_image' => 'https://cdn.develop.gomultitaskr.com/testing/workflows/success/solar.svg',
            'public' => 1,
            'order' => 4,
            'vertical' => 'Solar',
            'created_at' => now(),
            'updated_at' => now(),
            'use' => 'projects'
        ]);

        $step_id = DB::table('steps')->insertGetId([
            'uid' => 'e3b6a11d-5100-4ef9-a746-e9ec2d2e79c1',
            'name' => 'Customer Info Questionnaire',
            'order' => 1,
            'public' => true,
            'workflow_id' => $workflow_id
        ]);

        $section = factory(\App\Models\Section::class)->create([
            'title' => 'Type of Services.',
            'description' => "Type of Services.",
            'order' => 1,
            'step_id' => $step_id
        ]);

        factory(\App\Models\Input::class)->create([
            'title' => 'Type of Services.',
            'data' =>  json_decode('{
                "type": "radio", 
                "required":true,
                "label": "What kind of solar services are you interested in?",
                "description": null,
                "image": false,
                "place_holder": null,
                "with_images": true,
                "hide_label": true,
                "show": true,
                "options": [
                    {
                        "image": "https://cdn.develop.gomultitaskr.com/testing/workflows/solar/solar_panel.svg",
                        "value": "Solar Panels",
                        "text": "Solar Panels",
                        "sub_text": "Convert sunlight into your home’s energy and be less dependent on your Electrical Utility"
                    },
                    {
                        "image": "https://cdn.develop.gomultitaskr.com/testing/workflows/solar/solar_battery.svg",
                        "value": "Home Battery System",
                        "text": "Home Battery System",
                        "sub_text": "Backup your home’s energy in a power outage and have a smart consumption system"
                    },
                    {
                        "image": "https://cdn.develop.gomultitaskr.com/testing/workflows/solar/solar_panel_with_battery.svg",
                        "value": "Solar Panels with Battery",
                        "text": "Solar Panels with Battery",
                        "sub_text": "Be more independent from the grid and have the best return on your investment"
                    }
                ]
            }', true),
            'validations' => 'required',
            'section_id' => $section->id,
            'order' => 1
        ]);

        $section = factory(\App\Models\Section::class)->create([
            'title' => 'Solar Details',
            'description' => "Solar Details",
            'order' => 2,
            'step_id' => $step_id
        ]);

        factory(\App\Models\Input::class)->create([
            'title' => 'Goal',
            'data' =>  json_decode('{
                "type": "radio", 
                "required":true,
                "label": "What is your goal if we accomplish your solar project?",
                "description": null,
                "image": false,
                "place_holder": null,
                "with_images": false,
                "hide_label": false,
                "show": true,
                "options": [
                    {
                        "image": null,
                        "value": "Save money and lower my monthly bill",
                        "text": "Save money and lower my monthly bill"
                    },
                    {
                        "image": null,
                        "value": "Desire to increase my usage and lower my bill (acquiring EV, turn A/C more often, etc.)",
                        "text": "Desire to increase my usage and lower my bill (acquiring EV, turn A/C more often, etc.)"
                    },
                    {
                        "image": null,
                        "value": "Predictable electricity cost.",
                        "text": "Predictable electricity cost."
                    },
                    {
                        "image": null,
                        "value": "Increase my property value.",
                        "text": "Increase my property value."
                    }
                ]
            }', true),
            'validations' => 'required',
            'section_id' => $section->id,
            'order' => 1
        ]);

        factory(\App\Models\Input::class)->create([
            'title' => 'Owner authorized',
            'data' =>  json_decode('{
                "type": "radio", 
                "required":true,
                "label": "Are you the owner or authorized to make property changes?",
                "description": null,
                "image": false,
                "place_holder": null,
                "with_images": false,
                "hide_label": false,
                "show": true,
                "options": [
                    {
                        "image": null,
                        "value": "Yes",
                        "text": "Yes"
                    },
                    {
                        "image": null,
                        "value": "No",
                        "text": "No"
                    }
                ]
            }', true),
            'validations' => 'required',
            'section_id' => $section->id,
            'order' => 2
        ]);

        factory(\App\Models\Input::class)->create([
            'title' => 'Stage',
            'data' =>  json_decode('{
                "type": "radio", 
                "required":true,
                "label": "What stage are you at in your home project?",
                "description": null,
                "image": false,
                "place_holder": null,
                "with_images": false,
                "hide_label": false,
                "show": true,
                "options": [
                    {
                        "image": null,
                        "value": "Design and Budget",
                        "text": "Design and Budget"
                    },
                    {
                        "image": null,
                        "value": "Ready to Hire",
                        "text": "Ready to Hire"
                    },
                    {
                        "image": null,
                        "value": "Comparing Bids",
                        "text": "Comparing Bids"
                    }
                ]
            }', true),
            'validations' => 'required',
            'section_id' => $section->id,
            'order' => 3
        ]);

        factory(\App\Models\Input::class)->create([
            'title' => 'Bill',
            'data' =>  json_decode('{
                "type": "radio", 
                "required":true,
                "label": "What’s your average electricity bill?",
                "description": null,
                "image": false,
                "place_holder": null,
                "with_images": false,
                "hide_label": false,
                "show": true,
                "options": [
                    {
                        "image": null,
                        "value": "$80 - $100",
                        "text": "$80 - $100"
                    },
                    {
                        "image": null,
                        "value": "$100 - $150",
                        "text": "$100 - $150"
                    },
                    {
                        "image": null,
                        "value": "$150 - $200",
                        "text": "$150 - $200"
                    },
                    {
                        "image": null,
                        "value": "Above $250",
                        "text": "Above $250"
                    }
                ]
            }', true),
            'validations' => 'required',
            'section_id' => $section->id,
            'order' => 4
        ]);

        factory(\App\Models\Input::class)->create([
            'title' => 'Old',
            'data' =>  json_decode('{
                "type": "radio", 
                "required":true,
                "label": "How old is your roof?",
                "description": null,
                "image": false,
                "place_holder": null,
                "with_images": false,
                "hide_label": false,
                "show": true,
                "options": [
                    {
                        "image": null,
                        "value": "5 years",
                        "text": "5 years"
                    },
                    {
                        "image": null,
                        "value": "10 years",
                        "text": "10 years"
                    },
                    {
                        "image": null,
                        "value": "15 years",
                        "text": "15 years"
                    },
                    {
                        "image": null,
                        "value": "20 years",
                        "text": "20 years"
                    },
                    {
                        "image": null,
                        "value": "Don’t know",
                        "text": "Don’t know"
                    }
                ]
            }', true),
            'validations' => 'required',
            'section_id' => $section->id,
            'order' => 5
        ]);

        factory(\App\Models\Input::class)->create([
            'title' => 'Trees',
            'data' =>  json_decode('{
                "type": "radio", 
                "required":true,
                "label": "Are there any nearby trees that cast a shade on your roof?",
                "description": null,
                "image": false,
                "place_holder": null,
                "with_images": false,
                "hide_label": false,
                "show": true,
                "options": [
                    {
                        "image": null,
                        "value": "No",
                        "text": "No"
                    },
                    {
                        "image": null,
                        "value": "Yes",
                        "text": "Yes"
                    }
                ]
            }', true),
            'validations' => 'required',
            'section_id' => $section->id,
            'order' => 6
        ]);

        factory(\App\Models\Input::class)->create([
            'title' => 'Electricity Offset',
            'data' =>  json_decode('{
                "type": "radio", 
                "required":true,
                "label": "What’s your desired electricity offset based on your current usage?",
                "description": null,
                "image": false,
                "place_holder": null,
                "with_images": false,
                "hide_label": false,
                "show": true,
                "options": [
                    {
                        "image": null,
                        "value": "100%",
                        "text": "100%"
                    },
                    {
                        "image": null,
                        "value": "100% - 110%",
                        "text": "100% - 110%"
                    },
                    {
                        "image": null,
                        "value": "110% - 120%",
                        "text": "110% - 120%"
                    }
                ]
            }', true),
            'validations' => 'required',
            'section_id' => $section->id,
            'order' => 7
        ]);

        factory(\App\Models\Input::class)->create([
            'title' => 'Start',
            'data' =>  json_decode('{
                "type": "radio", 
                "required":true,
                "label": "When you like to start this project?",
                "description": null,
                "image": false,
                "place_holder": null,
                "with_images": false,
                "hide_label": false,
                "show": true,
                "options": [
                    {
                        "image": null,
                        "value": "0 - 3 Months",
                        "text": "0 - 3 Months"
                    },
                    {
                        "image": null,
                        "value": "3 - 6 Months",
                        "text": "3 - 6 Months"
                    },
                    {
                        "image": null,
                        "value": "6 - 12 Months",
                        "text": "6 - 12 Months"
                    }
                ]
            }', true),
            'validations' => 'required',
            'section_id' => $section->id,
            'order' => 8
        ]);

        factory(\App\Models\Input::class)->create([
            'title' => 'Interest',
            'data' =>  json_decode('{
                "type": "radio", 
                "required":true,
                "label": "When you looking for solar, what is your greatest interest?",
                "description": null,
                "image": false,
                "place_holder": null,
                "with_images": false,
                "hide_label": false,
                "show": true,
                "options": [
                    {
                        "image": null,
                        "value": "Best Technology available",
                        "text": "Best Technology available"
                    },
                    {
                        "image": null,
                        "value": "Most Savings",
                        "text": "Most Savings"
                    },
                    {
                        "image": null,
                        "value": "Higher Warranties",
                        "text": "Higher Warranties"
                    },
                    {
                        "image": null,
                        "value": "All of the above",
                        "text": "All of the above"
                    }
                ]
            }', true),
            'validations' => 'required',
            'section_id' => $section->id,
            'order' => 9
        ]);


        $step_id = DB::table('steps')->insertGetId([
            'uid' => 'd2cd0167-371a-4d14-90fa-87979004f0e2',
            'name' => 'Pre-proposal Document',
            'order' => 2,
            'public' => true,
            'workflow_id' => $workflow_id
        ]);

        $section = factory(\App\Models\Section::class)->create([
            'title' => 'Pre-proposal Document',
            'description' => null,
            'order' => 1,
            'step_id' => $step_id
        ]);

        factory(\App\Models\Input::class)->create([
            'title' => 'Please add file',
            'data' =>  json_decode('{
                "type": "file", 
                "required":true,
                "label": "Please add file",
                "description": null,
                "image": false,
                "place_holder": null,
                "with_images": false,
                "hide_label": false,
                "show": true
            }', true),
            'order' => 1,
            'validations' => 'required',
            'section_id' => $section->id,
        ]);

        $step_id = DB::table('steps')->insertGetId([
            'uid' => '37e51575-c829-4078-baf2-e507326e8050',
            'name' => 'Documents required',
            'order' => 3,
            'public' => true,
            'workflow_id' => $workflow_id
        ]);

        $section = factory(\App\Models\Section::class)->create([
            'title' => 'Documents required',
            'description' => null,
            'order' => 1,
            'step_id' => $step_id
        ]);

        factory(\App\Models\Input::class)->create([
            'title' => 'SDGE Bill',
            'data' =>  json_decode('{
                "type": "file", 
                "required":true,
                "label": "SDGE Bill",
                "description": null,
                "image": false,
                "place_holder": null,
                "with_images": false,
                "hide_label": false,
                "show": true
            }', true),
            'order' => 1,
            'validations' => 'required',
            'section_id' => $section->id,
        ]);

        factory(\App\Models\Input::class)->create([
            'title' => 'SDGE Application',
            'data' =>  json_decode('{
                "type": "file", 
                "required":true,
                "label": "SDGE Application",
                "description": null,
                "image": false,
                "place_holder": null,
                "with_images": false,
                "hide_label": false,
                "show": true
            }', true),
            'order' => 2,
            'validations' => 'required',
            'section_id' => $section->id,
        ]);

        $step_id = DB::table('steps')->insertGetId([
            'uid' => '3c07a341-a10d-48b8-a104-ae9c1471bf8d',
            'name' => 'Proposal',
            'order' => 4,
            'public' => true,
            'workflow_id' => $workflow_id
        ]);

        $section = factory(\App\Models\Section::class)->create([
            'title' => 'Proposal',
            'description' => null,
            'order' => 1,
            'step_id' => $step_id
        ]);

        factory(\App\Models\Input::class)->create([
            'title' => 'Please add file',
            'data' =>  json_decode('{
                "type": "file", 
                "required":true,
                "label": "Please add file",
                "description": null,
                "image": false,
                "place_holder": null,
                "with_images": false,
                "hide_label": false,
                "show": true
            }', true),
            'order' => 1,
            'validations' => 'required',
            'section_id' => $section->id,
        ]);

        $step_id = DB::table('steps')->insertGetId([
            'uid' => 'ece32cc2-f699-4754-9997-432646850a01',
            'name' => 'Customer Protection Guide',
            'order' => 5,
            'public' => true,
            'workflow_id' => $workflow_id
        ]);

        $section = factory(\App\Models\Section::class)->create([
            'title' => 'Customer Protection Guide',
            'description' => null,
            'order' => 1,
            'step_id' => $step_id
        ]);

        factory(\App\Models\Input::class)->create([
            'title' => 'Please add file',
            'data' =>  json_decode('{
                "type": "file", 
                "required":true,
                "label": "Please add file",
                "description": null,
                "image": false,
                "place_holder": null,
                "with_images": false,
                "hide_label": false,
                "show": true
            }', true),
            'order' => 1,
            'validations' => 'required',
            'section_id' => $section->id,
        ]);
    }
}
