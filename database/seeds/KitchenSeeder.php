<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class KitchenSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $workflow_id = DB::table('workflows')->insertGetId([
            'uid' => '5e413612-4417-4855-aad4-5ecd13757373',
            'name' => 'Kitchen',
            'slug' => 'kitchen',
            'description' => 'Remodel your layout for storage and durability',
            'image' => 'https://cdn.develop.gomultitaskr.com/testing/workflows/wizard%20kitchen%20services.jpg',
            'success_image' => 'https://cdn.develop.gomultitaskr.com/testing/workflows/success/kitchen.svg',
            'public' => 1,
            'order' => 2,
            'vertical' => 'Construction',
            'created_at' => now(),
            'updated_at' => now(),
            'use' => 'projects'
        ]);

        $step_id = DB::table('steps')->insertGetId([
            'uid' => 'ab7f9d22-46f9-424e-b11f-71956b3e90d5',
            'name' => 'KITCHEN INFO',
            'order' => 1,
            'public' => true,
            'workflow_id' => $workflow_id
        ]);

        $section = factory(\App\Models\Section::class)->create([
            'title' => 'Type of remodel',
            'description' => "Remodel Type?",
            'order' => 1,
            'step_id' => $step_id
        ]);

        factory(\App\Models\Input::class)->create([
            'title' => 'Type of Kitchen',
            'data' =>  json_decode('{
                "type": "radio", 
                "required":true,
                "label": "Remodel Type",
                "description": null,
                "image": false,
                "place_holder": null,
                "with_images": true,
                "hide_label": true,
                "show": true,
                "options": [
                    {
                        "image": "https://cdn.develop.gomultitaskr.com/testing/workflows/kitchen/kitchen_full_remodel.svg",
                        "value": "Full Remodel",
                        "text": "Full Remodel",
                        "sub_text": "Invest into the heart of your home"
                    },
                    {
                        "image": "https://cdn.develop.gomultitaskr.com/testing/workflows/kitchen/kitchen_partial_remodel.svg",
                        "value": "Partial Remodel",
                        "text": "Partial Remodel",
                        "sub_text": "Add serious value to a section"
                    }
                ]
            }', true),
            'validations' => 'required',
            'section_id' => $section->id,
            'order' => 1
        ]);

        $section = factory(\App\Models\Section::class)->create([
            'title' => 'Project Details',
            'description' => "Now let's review your plan & details to complete your Kitchen construction",
            'order' => 2,
            'step_id' => $step_id
        ]);

        factory(\App\Models\Input::class)->create([
            'title' => 'Owner authorized',
            'data' =>  json_decode('{
                "type": "radio", 
                "required":true,
                "label": "Are you the owner or authorized to make property changes?",
                "description": null,
                "image": false,
                "place_holder": null,
                "with_images": false,
                "hide_label": false,
                "show": true,
                "options": [
                    {
                        "image": null,
                        "value": "Yes",
                        "text": "Yes"
                    },
                    {
                        "image": null,
                        "value": "No",
                        "text": "No"
                    }
                ]
            }', true),
            'validations' => 'required',
            'section_id' => $section->id,
            'order' => 1
        ]);


        factory(\App\Models\Input::class)->create([
            'title' => 'Purpose',
            'data' =>  json_decode('{
                "type": "radio", 
                "required":true,
                "label": "What is the purpose of this construction or remodel?",
                "description": null,
                "image": false,
                "place_holder": null,
                "with_images": false,
                "hide_label": false,
                "show": true,
                "options": [
                    {
                        "image": null,
                        "value": "Rental",
                        "text": "Rental"
                    },
                    {
                        "image": null,
                        "value": "More Space",
                        "text": "More Space"
                    },
                    {
                        "image": null,
                        "value": "Needed Remodel/Repair",
                        "text": "Needed Remodel/Repair"
                    }
                ]
            }', true),
            'validations' => 'required',
            'section_id' => $section->id,
            'order' => 2
        ]);

        $inputLayout = factory(\App\Models\Input::class)->create([
            'title' => 'Layout',
            'data' =>  json_decode('{
                "type": "radio", 
                "required":true,
                "label": "Changing layout or just upgrades?",
                "description": null,
                "image": false,
                "place_holder": null,
                "with_images": false,
                "hide_label": false,
                "show": true,
                "options": [
                    {
                        "image": null,
                        "value": "Changing Layout",
                        "text": "Changing Layout"
                    },
                    {
                        "image": null,
                        "value": "Upgrades",
                        "text": "Upgrades"
                    }
                ]
            }', true),
            'validations' => 'required',
            'section_id' => $section->id,
            'order' => 3
        ]);

        $inputUpgrades = factory(\App\Models\Input::class)->create([
            'title' => 'Upgrades',
            'data' =>  json_decode('{
                "type": "checkbox", 
                "required":true,
                "label": "What areas will be upgraded?",
                "description": null,
                "image": false,
                "place_holder": null,
                "with_images": false,
                "hide_label": false,
                "show": false,
                "options": [
                    {
                        "image": null,
                        "value": "Cabinets",
                        "text": "Cabinets"
                    },
                    {
                        "image": null,
                        "value": "Counter tops",
                        "text": "Counter tops"
                    },
                    {
                        "image": null,
                        "value": "Flooring",
                        "text": "Flooring"
                    }
                ]
            }', true),
            'validations' => 'required',
            'section_id' => $section->id,
            'order' => 4
        ]);

        factory(\App\Models\StepLogic::class)->create([
            'logic_data' => json_decode('{"if": "' . $inputLayout->uid . '", "conditional": "=", "value": "Upgrades"}', true),
            'action' => json_decode('{"type": "input", "uid": "' . $inputUpgrades->uid . '", "action": "show"}', true),
            'step_id' => $step_id
        ]);

        factory(\App\Models\Input::class)->create([
            'title' => 'Stage',
            'data' =>  json_decode('{
                "type": "radio", 
                "required":true,
                "label": "What stage are you at in your home project?",
                "description": null,
                "image": false,
                "place_holder": null,
                "with_images": false,
                "hide_label": false,
                "show": true,
                "options": [
                    {
                        "image": null,
                        "value": "Design and Budget",
                        "text": "Design and Budget"
                    },
                    {
                        "image": null,
                        "value": "Ready to Hire",
                        "text": "Ready to Hire"
                    },
                    {
                        "image": null,
                        "value": "Comparing Bids",
                        "text": "Comparing Bids"
                    }
                ]
            }', true),
            'validations' => 'required',
            'section_id' => $section->id,
            'order' => 5
        ]);

        factory(\App\Models\Input::class)->create([
            'title' => 'Start',
            'data' =>  json_decode('{
                "type": "radio", 
                "required":true,
                "label": "When you like to start this project?",
                "description": null,
                "image": false,
                "place_holder": null,
                "with_images": false,
                "hide_label": false,
                "show": true,
                "options": [
                    {
                        "image": null,
                        "value": "0 - 3 Months",
                        "text": "0 - 3 Months"
                    },
                    {
                        "image": null,
                        "value": "3 - 6 Months",
                        "text": "3 - 6 Months"
                    },
                    {
                        "image": null,
                        "value": "6 - 12 Months",
                        "text": "6 - 12 Months"
                    }
                ]
            }', true),
            'validations' => 'required',
            'section_id' => $section->id,
            'order' => 6
        ]);

        factory(\App\Models\Input::class)->create([
            'title' => 'Budget',
            'data' =>  json_decode('{
                "type": "textarea", 
                "required":true,
                "label": "Do you have a Budget in mind for this project?",
                "description": null,
                "image": false,
                "place_holder": null,
                "with_images": false,
                "hide_label": false,
                "show": true
            }', true),
            'validations' => 'required',
            'section_id' => $section->id,
            'order' => 7
        ]);

        factory(\App\Models\Input::class)->create([
            'title' => 'Extra details that you would like to share',
            'data' =>  json_decode('{
                "type": "textarea", 
                "required":false,
                "label": "Extra details that you would like to share",
                "description": null,
                "image": false,
                "place_holder": "",
                "with_images": false,
                "hide_label": false,
                "show": true
            }', true),
            'validations' => 'required',
            'section_id' => $section->id,
            'order' => 8
        ]);


        $step_id = DB::table('steps')->insertGetId([
            'uid' => '961abd74-7965-47d1-ad2f-8eb6c7f14823',
            'name' => 'Pre-Proposal Document',
            'order' => 2,
            'public' => false,
            'workflow_id' => $workflow_id
        ]);

        $section = factory(\App\Models\Section::class)->create([
            'title' => 'Pre-exp',
            'description' => null,
            'order' => 1,
            'step_id' => $step_id
        ]);

        factory(\App\Models\Input::class)->create([
            'title' => 'Pre-exp',
            'data' =>  json_decode('{
                "type": "preexp", 
                "required":false,
                "label": "",
                "description": null,
                "image": false,
                "place_holder": null,
                "with_images": false,
                "hide_label": false,
                "show": true
            }', true),
            'order' => 1,
            'validations' => 'nullable',
            'section_id' => $section->id,
        ]);

        $step_id = DB::table('steps')->insertGetId([
            'uid' => 'c373d80b-06af-428a-acd3-bcec696e9f18',
            'name' => 'Post-Exploration Questionnaire',
            'order' => 3,
            'public' => false,
            'workflow_id' => $workflow_id
        ]);

        $section = factory(\App\Models\Section::class)->create([
            'title' => 'Post-exp',
            'description' => null,
            'order' => 1,
            'step_id' => $step_id
        ]);

        factory(\App\Models\Input::class)->create([
            'title' => 'Please describe all the details of the Custom ADU below',
            'data' =>  json_decode('{
                "type": "textarea", 
                "required":true,
                "label": "Please describe all the details of the Custom ADU below",
                "description": null,
                "image": false,
                "place_holder": "Explain in detail your main objectives.",
                "with_images": false,
                "hide_label": false,
                "show": true
            }', true),
            'validations' => 'required',
            'section_id' => $section->id,
            'order' => 1
        ]);


        factory(\App\Models\Input::class)->create([
            'title' => "Please upload 4 pictures around the desired work area",
            'data' =>  json_decode('{
                "type": "imagepicker", 
                "required":true,
                "label": "Please upload 4 pictures around the desired work area",
                "description": null,
                "image": false,
                "place_holder": null,
                "with_images": false,
                "hide_label": false,
                "show": true
            }', true),
            'order' => 1,
            'validations' => 'required',
            'section_id' => $section->id,
        ]);

        $step_id = DB::table('steps')->insertGetId([
            'uid' => '465a839f-7340-4803-9356-533769835dda',
            'name' => 'Project Development Plan (Contract)',
            'order' => 4,
            'public' => false,
            'workflow_id' => $workflow_id
        ]);

        $section = factory(\App\Models\Section::class)->create([
            'title' => 'Project Development Plan (Contract)',
            'description' => null,
            'order' => 1,
            'step_id' => $step_id
        ]);

        factory(\App\Models\Input::class)->create([
            'title' => 'Project Development Plan',
            'data' =>  json_decode('{
                "type": "quick-books", 
                "required":true,
                "label": "",
                "description": null,
                "image": false,
                "place_holder": null,
                "with_images": false,
                "hide_label": false,
                "show": true
            }', true),
            'order' => 1,
            'validations' => 'required',
            'section_id' => $section->id,
        ]);

        $step_id = DB::table('steps')->insertGetId([
            'uid' => 'a8086932-d098-4521-9a70-e35709083d04',
            'name' => 'Home Diagnostic Report',
            'order' => 5,
            'public' => false,
            'workflow_id' => $workflow_id,
            'preserve_input_values' => true
        ]);

        $section = factory(\App\Models\Section::class)->create([
            'title' => 'Home Diagnostic Report',
            'description' => null,
            'order' => 1,
            'step_id' => $step_id
        ]);

        factory(\App\Models\Input::class)->create([
            'title' => 'Please add link to Matterport.',
            'data' =>  json_decode('{
                "type": "text", 
                "required":true,
                "label": "Please add link to Matterport.",
                "description": null,
                "image": false,
                "place_holder": null,
                "with_images": false,
                "hide_label": false,
                "show": true
            }', true),
            'validations' => 'required',
            'section_id' => $section->id,
            'order' => 1
        ]);

        factory(\App\Models\Input::class)->create([
            'title' => 'Please add link to Drone Deploy.',
            'data' =>  json_decode('{
                "type": "text", 
                "required":true,
                "label": "Please add link to Drone Deploy.",
                "description": null,
                "image": false,
                "place_holder": null,
                "with_images": false,
                "hide_label": false,
                "show": true
            }', true),
            'validations' => 'required',
            'section_id' => $section->id,
            'order' => 2
        ]);

        factory(\App\Models\Input::class)->create([
            'title' => 'Please add link to Home Inspection Report.',
            'data' =>  json_decode('{
                "type": "text", 
                "required":true,
                "label": "Please add link to Home Inspection Report.",
                "description": null,
                "image": false,
                "place_holder": null,
                "with_images": false,
                "hide_label": false,
                "show": true
            }', true),
            'validations' => 'required',
            'section_id' => $section->id,
            'order' => 3
        ]);

        $step_id = DB::table('steps')->insertGetId([
            'uid' => '33392d1b-20f6-408b-bceb-413fe7a1b7fa',
            'name' => '2D Design',
            'order' => 6,
            'public' => false,
            'workflow_id' => $workflow_id,
            'preserve_input_values' => true
        ]);

        $section = factory(\App\Models\Section::class)->create([
            'title' => '2D Design',
            'description' => null,
            'order' => 1,
            'step_id' => $step_id
        ]);

        factory(\App\Models\Input::class)->create([
            'title' => 'Please add file',
            'data' =>  json_decode('{
                "type": "file", 
                "required":true,
                "label": "Please add file",
                "description": null,
                "image": false,
                "place_holder": null,
                "with_images": false,
                "hide_label": false,
                "show": true
            }', true),
            'order' => 1,
            'validations' => 'required',
            'section_id' => $section->id,
        ]);

        $step_id = DB::table('steps')->insertGetId([
            'uid' => '299d47a7-7df2-4525-a28a-5d2335fbb1b8',
            'name' => 'Estimation',
            'order' => 7,
            'public' => false,
            'workflow_id' => $workflow_id,
            'preserve_input_values' => true
        ]);

        $section = factory(\App\Models\Section::class)->create([
            'title' => 'Estimation',
            'description' => null,
            'order' => 1,
            'step_id' => $step_id
        ]);

        factory(\App\Models\Input::class)->create([
            'title' => 'Please add file',
            'data' =>  json_decode('{
                "type": "file", 
                "required":true,
                "label": "Please add file",
                "description": null,
                "image": false,
                "place_holder": null,
                "with_images": false,
                "hide_label": false,
                "show": true
            }', true),
            'order' => 1,
            'validations' => 'required',
            'section_id' => $section->id,
        ]);

        $step_id = DB::table('steps')->insertGetId([
            'uid' => 'e050f80b-37ed-45d3-ba0f-78b01b114457',
            'name' => 'Proposal',
            'order' => 8,
            'public' => false,
            'workflow_id' => $workflow_id,
            'preserve_input_values' => true
        ]);

        $section = factory(\App\Models\Section::class)->create([
            'title' => 'Proposal',
            'description' => null,
            'order' => 1,
            'step_id' => $step_id
        ]);

        factory(\App\Models\Input::class)->create([
            'title' => 'Please add file',
            'data' =>  json_decode('{
                "type": "file", 
                "required":true,
                "label": "Please add file",
                "description": null,
                "image": false,
                "place_holder": null,
                "with_images": false,
                "hide_label": false,
                "show": true
            }', true),
            'order' => 1,
            'validations' => 'required',
            'section_id' => $section->id,
        ]);

        $step_id = DB::table('steps')->insertGetId([
            'uid' => '217b971a-81a9-4db2-88a8-2aea0f1ab344',
            'name' => 'Virtual Reality (Render)',
            'order' => 9,
            'public' => false,
            'workflow_id' => $workflow_id,
            'preserve_input_values' => true
        ]);

        $section = factory(\App\Models\Section::class)->create([
            'title' => 'Virtual Reality (Render)',
            'description' => null,
            'order' => 1,
            'step_id' => $step_id
        ]);

        factory(\App\Models\Input::class)->create([
            'title' => 'Please add link to Shapespark',
            'data' =>  json_decode('{
                "type": "text", 
                "required":true,
                "label": "Please add link to Shapespark",
                "description": null,
                "image": false,
                "place_holder": null,
                "with_images": false,
                "hide_label": false,
                "show": true
            }', true),
            'validations' => 'required',
            'section_id' => $section->id,
            'order' => 1
        ]);

        $step_id = DB::table('steps')->insertGetId([
            'uid' => 'f305ab7f-d452-4ff3-a9f8-02d1e899e6f6',
            'name' => 'Project Development Plan',
            'order' => 10,
            'public' => false,
            'workflow_id' => $workflow_id
        ]);

        $section = factory(\App\Models\Section::class)->create([
            'title' => 'Project Development Plan',
            'description' => null,
            'order' => 1,
            'step_id' => $step_id
        ]);

        factory(\App\Models\Input::class)->create([
            'title' => 'Please add the link',
            'data' =>  json_decode('{
                "type": "text", 
                "required":true,
                "label": "Please add the link",
                "description": null,
                "image": false,
                "place_holder": null,
                "with_images": false,
                "hide_label": false,
                "show": true
            }', true),
            'validations' => 'required',
            'section_id' => $section->id,
            'order' => 1
        ]);
    }
}
