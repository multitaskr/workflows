<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TestWorkflowSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $workflow_id = DB::table('workflows')->insertGetId([
            'uid' => '3ef83f09-7055-4fae-b1c8-0329d0bcb8d8',
            'name' => 'Test Workflow',
            'slug' => 'test-workflow', 
            'description' => null,
            'vertical' => 'Construction',
            'created_at' => now(),
            'updated_at' => now(),
            'use' => 'test'
        ]);

        $step_id = DB::table('steps')->insertGetId([
            'uid' => '1e8354a9-46ac-4e7e-95ff-34abf849f0c8',
            'name' => 'Step 1',
            'order' => 1,
            'public' => true,
            'workflow_id' => $workflow_id
        ]);

        $section = factory(\App\Models\Section::class)->create([
            'title' => 'Test Section 1',
            'description' => null,
            'order' => 1,
            'step_id' => $step_id
        ]);

        factory(\App\Models\Input::class)->create([
            'title' => 'Text Question?',
            'data' =>  json_decode('{
                "type": "text", 
                "required":true,
                "label": "Text Question?",
                "description": null,
                "image": false,
                "place_holder": null,
                "with_images": false,
                "hide_label": false,
                "show": true
            }', true),
            'validations' => 'required',
            'section_id' => $section->id,
            'order' => 1
        ]);

        factory(\App\Models\Input::class)->create([
            'title' => 'Text Area Question?',
            'data' =>  json_decode('{
                "type": "textarea", 
                "required":true,
                "label": "Text Area Question?",
                "description": null,
                "image": false,
                "place_holder": null,
                "with_images": false,
                "hide_label": false,
                "show": true
            }', true),
            'validations' => 'required',
            'section_id' => $section->id,
            'order' => 2
        ]);

        

        $step_id = DB::table('steps')->insertGetId([
            'uid' => 'a298bfe4-cb6b-42a3-9b78-958cb354c354',
            'name' => 'Step 2',
            'order' => 2,
            'public' => false,
            'workflow_id' => $workflow_id
        ]);

        $section = factory(\App\Models\Section::class)->create([
            'title' => 'Test Section 2',
            'description' => null,
            'order' => 1,
            'step_id' => $step_id
        ]);

        factory(\App\Models\Input::class)->create([
            'title' => 'Number Question?',
            'data' =>  json_decode('{
                "type": "number", 
                "required":true,
                "label": "Number Question?",
                "description": null,
                "image": false,
                "place_holder": null,
                "with_images": false,
                "hide_label": false,
                "show": true
            }', true),
            'validations' => 'required|numeric',
            'section_id' => $section->id,
            'order' => 1
        ]);

        factory(\App\Models\Input::class)->create([
            'title' => 'Email Question?',
            'data' =>  json_decode('{
                "type": "email", 
                "required":true,
                "label": "Email Question?",
                "description": null,
                "image": false,
                "place_holder": null,
                "with_images": false,
                "hide_label": false,
                "show": true
            }', true),
            'validations' => 'required|email',
            'section_id' => $section->id,
            'order' => 2
        ]);

        factory(\App\Models\Input::class)->create([
            'title' => 'Phone Question?',
            'data' =>  json_decode('{
                "type": "phone", 
                "required":true,
                "label": "Phone Question?",
                "description": null,
                "image": false,
                "place_holder": null,
                "with_images": false,
                "hide_label": false,
                "show": true
            }', true),
            'validations' => 'required',
            'section_id' => $section->id,
            'order' => 3
        ]);

        $step_id = DB::table('steps')->insertGetId([
            'uid' => 'b5cf3d65-7bab-4b0c-9fad-6c0608faa882',
            'name' => 'Step 3',
            'order' => 3,
            'public' => false,
            'workflow_id' => $workflow_id
        ]);

        $section = factory(\App\Models\Section::class)->create([
            'title' => 'Test Section 3',
            'description' => null,
            'order' => 1,
            'step_id' => $step_id
        ]);
        
        factory(\App\Models\Input::class)->create([
            'title' => 'Radio Question?',
            'data' =>  json_decode('{
                "type": "radio", 
                "required":true,
                "label": "Radio Question?",
                "description": null,
                "image": false,
                "place_holder": null,
                "with_images": false,
                "hide_label": false,
                "show": true,
                "options": [
                    {
                        "image": null,
                        "value": "option 1",
                        "text": "Option 1"
                    },
                    {
                        "image": null,
                        "value": "Option 2",
                        "text": "Option 2"
                    },
                    {
                        "image": null,
                        "value": "Option 3",
                        "text": "Option 3"
                    },
                    {
                        "image": null,
                        "value": "Option 4",
                        "text": "Option 4"
                    }
                ]
            }', true),
            'validations' => 'required',
            'section_id' => $section->id,
            'order' => 1
        ]);

        factory(\App\Models\Input::class)->create([
            'title' => 'Select Question?',
            'data' =>  json_decode('{
                "type": "select", 
                "required":true,
                "label": "Select Question?",
                "description": null,
                "image": false,
                "place_holder": null,
                "with_images": false,
                "hide_label": false,
                "show": true,
                "options": [
                    {
                        "image": null,
                        "value": "option 1",
                        "text": "Option 1"
                    },
                    {
                        "image": null,
                        "value": "Option 2",
                        "text": "Option 2"
                    },
                    {
                        "image": null,
                        "value": "Option 3",
                        "text": "Option 3"
                    },
                    {
                        "image": null,
                        "value": "Option 4",
                        "text": "Option 4"
                    }
                ]
            }', true),
            'validations' => 'required',
            'section_id' => $section->id,
            'order' => 2
        ]);

        factory(\App\Models\Input::class)->create([
            'title' => 'Checkbox Question?',
            'data' =>  json_decode('{
                "type": "checkbox", 
                "required":true,
                "label": "Checkbox Question?",
                "description": null,
                "image": false,
                "place_holder": null,
                "with_images": false,
                "hide_label": false,
                "show": true,
                "options": [
                    {
                        "image": null,
                        "value": "option 1",
                        "text": "Option 1"
                    },
                    {
                        "image": null,
                        "value": "Option 2",
                        "text": "Option 2"
                    },
                    {
                        "image": null,
                        "value": "Option 3",
                        "text": "Option 3"
                    },
                    {
                        "image": null,
                        "value": "Option 4",
                        "text": "Option 4"
                    }
                ]
            }', true),
            'validations' => 'required',
            'section_id' => $section->id,
            'order' => 3
        ]);

        $step_id = DB::table('steps')->insertGetId([
            'uid' => '17a9273c-6a04-4306-9d54-99ed3db96752',
            'name' => 'Step 4',
            'order' => 4,
            'public' => false,
            'workflow_id' => $workflow_id
        ]);

        $section = factory(\App\Models\Section::class)->create([
            'title' => 'Test Section 4',
            'description' => null,
            'order' => 1,
            'step_id' => $step_id
        ]);

        factory(\App\Models\Input::class)->create([
            'title' => 'File Question?',
            'data' =>  json_decode('{
                "type": "file", 
                "required":true,
                "label": "File Question?",
                "description": null,
                "image": false,
                "place_holder": null,
                "with_images": false,
                "hide_label": false,
                "show": true
            }', true),
            'validations' => 'required',
            'section_id' => $section->id,
            'order' => 1
        ]);

        factory(\App\Models\Input::class)->create([
            'title' => 'Rating Question?',
            'data' =>  json_decode('{
                "type": "rating", 
                "required":true,
                "label": "Rating Question?",
                "description": null,
                "image": false,
                "place_holder": null,
                "with_images": false,
                "hide_label": false,
                "show": true
            }', true),
            'validations' => 'required',
            'section_id' => $section->id,
            'order' => 1
        ]);
    }
}
