<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CfrSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $workflow_id = DB::table('workflows')->insertGetId([
            'uid' => 'e2ad6d7d-b5f8-4c48-82d4-d8994e376316',
            'name' => 'Financial',
            'slug' => 'financial',
            'description' => null,
            'vertical' => 'Financial',
            'created_at' => now(),
            'updated_at' => now(),
            'use' => 'projects'
        ]);

        $step_id = DB::table('steps')->insertGetId([
            'uid' => '90907f0a-410c-49f7-bcff-060befe48266',
            'name' => 'CFR Document',
            'order' => 1,
            'public' => true,
            'workflow_id' => $workflow_id
        ]);

        $section = factory(\App\Models\Section::class)->create([
            'title' => 'CFR Document',
            'description' => "CFR Document",
            'order' => 1,
            'step_id' => $step_id
        ]);

        factory(\App\Models\Input::class)->create([
            'title' => 'CFR Document',
            'data' =>  json_decode('{
                "type": "cfr-document", 
                "required":false,
                "label": "",
                "description": null,
                "image": false,
                "place_holder": null,
                "with_images": false,
                "hide_label": false,
                "show": true
            }', true),
            'order' => 1,
            'validations' => 'required',
            'section_id' => $section->id,
            'tag' => 'cfr-document'
        ]);

        $step_id = DB::table('steps')->insertGetId([
            'uid' => 'd97360d6-841c-4c14-866f-2bb64916abf0',
            'name' => 'CFR Solution',
            'order' => 2,
            'public' => true,
            'customer_edit' => true,
            'workflow_id' => $workflow_id
        ]);

        $section = factory(\App\Models\Section::class)->create([
            'title' => 'CFR Solution',
            'description' => "CFR Solution",
            'order' => 2,
            'step_id' => $step_id
        ]);

        factory(\App\Models\Input::class)->create([
            'title' => 'CFR Solution',
            'data' =>  json_decode('{
                "type": "cfr-solution", 
                "required":false,
                "label": "",
                "description": null,
                "image": false,
                "place_holder": null,
                "with_images": false,
                "hide_label": false,
                "show": true
            }', true),
            'order' => 1,
            'validations' => 'nullable',
            'section_id' => $section->id,
            'tag' => 'cfr-solution'
        ]);
    }
}
