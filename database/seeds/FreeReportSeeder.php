<?php

use Ramsey\Uuid\Uuid;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class FreeReportSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $workflow_id = DB::table('workflows')->insertGetId([
            'uid' => 'e2ad6d7d-b5f3-4c48-82d4-d8494e956748',
            'name' => 'Free Report',
            'slug' => 'free-report',
            'description' => 'Get a free report analysis for growth and savings',
            'image' => 'https://cdn.develop.gomultitaskr.com/testing/workflows/wizard%20free%20report.jpg',
            'success_image' => 'https://cdn.develop.gomultitaskr.com/testing/workflows/success/free_report.svg',
            'public' => 0,
            'order' => 0,
            'vertical' => 'Call Center',
            'created_at' => now(),
            'updated_at' => now(),
            'use' => 'call center'
        ]);

        $step_id = DB::table('steps')->insertGetId([
            'uid' => '4b95b2d2-0efb-4cec-9f78-32cedd9f8120',
            'name' => 'Home Analysis Report',
            'order' => 0,
            'workflow_id' => $workflow_id,
            'public' => true
        ]);

	    $section = factory(\App\Models\Section::class)->create([
            'title' => 'Home Analysis Report',
            'description' => null,
            'order' => 1,
            'step_id' => $step_id,
            'private' => true
        ]);

        
        factory(\App\Models\Input::class)->create([
            'title' => 'Home Analysis Report',
            'data' =>  json_decode('{
                "type": "free-report", 
                "required":false,
                "label": "Home Analysis Report",
                "description": null,
                "image": false,
                "place_holder": "",
                "with_images": false,
                "hide_label": false,
                "show": true
            }', true),
            'validations' => 'nullable',
            'section_id' => $section->id,
            'order' => 0,
            'tag' => 'free-report'
        ]);

        for ($i = 1; $i <= 5; $i++) { 
            $step_id = DB::table('steps')->insertGetId([
                'uid' => (string) Uuid::uuid4(),
                'name' => date("jS", strtotime("January {$i}")) . ' Attempt',
                'order' => $i,
                'workflow_id' => $workflow_id
            ]);
    
            $section = factory(\App\Models\Section::class)->create([
                'title' => 'Call Details',
                'description' => null,
                'order' => 1,
                'step_id' => $step_id
            ]);
    
            factory(\App\Models\Input::class)->create([
                'title' => 'Comments',
                'data' =>  json_decode('{
                    "type": "textarea", 
                    "required":true,
                    "label": "Comments",
                    "description": null,
                    "image": false,
                    "place_holder": "Enter your comments...",
                    "with_images": false,
                    "hide_label": false,
                    "show": true
                }', true),
                'validations' => 'nullable',
                'section_id' => $section->id,
                'order' => 0
            ]);
            
            factory(\App\Models\Input::class)->create([
                'title' => 'Call status',
                'data' =>  json_decode('{
                    "type": "radio", 
                    "required":true,
                    "label": "Call status",
                    "description": null,
                    "image": false,
                    "place_holder": null,
                    "with_images": false,
                    "hide_label": false,
                    "show": true,
                    "options": [
                        {
                            "image": null,
                            "value": "Not answer / left voicemail",
                            "text": "Not answer / left voicemail"
                        },
                        {
                            "image": null,
                            "value": "Appointment scheduled",
                            "text": "Appointment scheduled"
                        },
                        {
                            "image": null,
                            "value": "Call back",
                            "text": "Call back"
                        },
                        {
                            "image": null,
                            "value": "Follow up",
                            "text": "Follow up"
                        },
                        {
                            "image": null,
                            "value": "Wrong number",
                            "text": "Wrong number"
                        },
                        {
                            "image": null,
                            "value": "Not interested",
                            "text": "Not interested"
                        },
                        {
                            "image": null,
                            "value": "Unresponsive",
                            "text": "Unresponsive"
                        }
                    ]
                }', true),
                'validations' => 'required',
                'tag' => 'call-status',
                'section_id' => $section->id,
                'order' => 1
            ]);
        }
    }
}
