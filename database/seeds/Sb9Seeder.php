<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class Sb9Seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $workflow_id = DB::table('workflows')->insertGetId([
            'uid' => 'a046b712-999b-4dc2-8f84-d5fe88a6d5b3',
            'name' => 'Senate Bill 9',
            'slug' => 'senate-bill-9',
            'description' => null,
            'vertical' => 'Construction',
            'created_at' => now(),
            'updated_at' => now(),
            'use' => 'projects'
        ]);

        $step_id = DB::table('steps')->insertGetId([
            'uid' => 'facdccaf-f6e8-4f97-80b4-bc8487bbd559',
            'name' => 'SB9 Process',
            'order' => 1,
            'public' => false,
            'workflow_id' => $workflow_id
        ]);

        $section = factory(\App\Models\Section::class)->create([
            'title' => 'SB9 Completed',
            'description' => "Validation process has finished and a split parcel template has been chosen.",
            'order' => 1,
            'step_id' => $step_id
        ]);

        factory(\App\Models\Input::class)->create([
            'title' => 'SB9',
            'data' =>  json_decode('{
                "type": "sb9", 
                "required":false,
                "label": "",
                "description": null,
                "image": false,
                "place_holder": null,
                "with_images": false,
                "hide_label": false,
                "show": true
            }', true),
            'order' => 1,
            'validations' => 'required',
            'section_id' => $section->id,
            'tag' => 'sb9-completed'
        ]);       
    }
}
