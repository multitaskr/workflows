<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class LandscapeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $workflow_id = DB::table('workflows')->insertGetId([
            'uid' => '993440c4-fb15-4bf8-bf80-6ea1391adfe6',
            'name' => 'Landscape',
            'slug' => 'landscape',
            'description' => 'Make your outdoor space drought-tolerant',
            'image' => 'https://cdn.develop.gomultitaskr.com/testing/workflows/wizard%20landscape%20services.jpg',
            'success_image' => 'https://cdn.develop.gomultitaskr.com/testing/workflows/success/landscape.svg',
            'public' => 1,
            'order' => 5,
            'vertical' => 'Landscape',
            'created_at' => now(),
            'updated_at' => now(),
            'use' => 'projects'
        ]);

        $step_id = DB::table('steps')->insertGetId([
            'uid' => 'b5a6dd42-6d43-4a94-b290-b4265d34fcb2',
            'name' => 'LANDSCAPE INFO',
            'order' => 1,
            'public' => true,
            'workflow_id' => $workflow_id
        ]);

        $section = factory(\App\Models\Section::class)->create([
            'title' => 'Types of projects',
            'description' => "Types of projects? ",
            'order' => 1,
            'step_id' => $step_id
        ]);

        factory(\App\Models\Input::class)->create([
            'title' => 'Type of project?',
            'data' =>  json_decode('{
                "type": "checkbox", 
                "required":true,
                "label": "What project do you are interested in?",
                "description": null,
                "image": false,
                "place_holder": null,
                "with_images": true,
                "hide_label": false,
                "show": true,
                "options": [
                    {
                        "image": "https://cdn.develop.gomultitaskr.com/testing/workflows/landscape/bbq.svg",
                        "value": "BBQ",
                        "text": "BBQ"
                    },
                    {
                        "image": "https://cdn.develop.gomultitaskr.com/testing/workflows/landscape/driveways.svg",
                        "value": "Driveway",
                        "text": "Driveway"
                    },
                    {
                        "image": "https://cdn.develop.gomultitaskr.com/testing/workflows/landscape/fireplace.svg",
                        "value": "Fireplace",
                        "text": "Fireplace"
                    },
                    {
                        "image": "https://cdn.develop.gomultitaskr.com/testing/workflows/landscape/irrigation_system.svg",
                        "value": "Irrigation system",
                        "text": "Irrigation system"
                    },
                    {
                        "image": "https://cdn.develop.gomultitaskr.com/testing/workflows/landscape/landscape.svg",
                        "value": "Landscape",
                        "text": "Landscape"
                    },
                    {
                        "image": "https://cdn.develop.gomultitaskr.com/testing/workflows/landscape/lighting.svg",
                        "value": "Lighting",
                        "text": "Lighting"
                    }
                ]
            }', true),
            'validations' => 'required',
            'section_id' => $section->id,
            'order' => 1
        ]);

        $section = factory(\App\Models\Section::class)->create([
            'title' => 'Types of projects',
            'description' => "Types of projects?",
            'order' => 2,
            'step_id' => $step_id
        ]);

        factory(\App\Models\Input::class)->create([
            'title' => 'Type of project?',
            'data' =>  json_decode('{
                "type": "checkbox", 
                "required":true,
                "label": "Continue adding projects of interest",
                "description": null,
                "image": false,
                "place_holder": null,
                "with_images": true,
                "hide_label": false,
                "show": true,
                "options": [
                    {
                        "image": "https://cdn.develop.gomultitaskr.com/testing/workflows/landscape/patio_overs.svg",
                        "value": "Patio - Covers",
                        "text": "Patio - Covers"
                    },
                    {
                        "image": "https://cdn.develop.gomultitaskr.com/testing/workflows/landscape/pavers.svg",
                        "value": "Pavers",
                        "text": "Pavers"
                    },
                    {
                        "image": "https://cdn.develop.gomultitaskr.com/testing/workflows/landscape/retaining_wall.svg",
                        "value": "Retaining Wall",
                        "text": "Retaining Wall"
                    },
                    {
                        "image": "https://cdn.develop.gomultitaskr.com/testing/workflows/landscape/turf.svg",
                        "value": "Turf",
                        "text": "Turf"
                    },
                    {
                        "image": "https://cdn.develop.gomultitaskr.com/testing/workflows/landscape/pool_spa.svg",
                        "value": "Pool / Spa",
                        "text": "Pool / Spa"
                    },
                    {
                        "image": "https://cdn.develop.gomultitaskr.com/testing/workflows/landscape/water_features.svg",
                        "value": "Water Features",
                        "text": "Water Features"
                    }
                ]
            }', true),
            'validations' => 'required',
            'section_id' => $section->id,
            'order' => 1
        ]);

        $section = factory(\App\Models\Section::class)->create([
            'title' => 'Project Details',
            'description' => "Now let's review your plan & details to complete your landscape project.",
            'order' => 3,
            'step_id' => $step_id
        ]);

        factory(\App\Models\Input::class)->create([
            'title' => 'Owner authorized',
            'data' =>  json_decode('{
                "type": "radio", 
                "required":true,
                "label": "Are you the owner or authorized to make property changes?",
                "description": null,
                "image": false,
                "place_holder": null,
                "with_images": false,
                "hide_label": false,
                "show": true,
                "options": [
                    {
                        "image": null,
                        "value": "Yes",
                        "text": "Yes"
                    },
                    {
                        "image": null,
                        "value": "No",
                        "text": "No"
                    }
                ]
            }', true),
            'validations' => 'required',
            'section_id' => $section->id,
            'order' => 1
        ]);

        factory(\App\Models\Input::class)->create([
            'title' => 'Stage',
            'data' =>  json_decode('{
                "type": "radio", 
                "required":true,
                "label": "What stage are you at in your home project?",
                "description": null,
                "image": false,
                "place_holder": null,
                "with_images": false,
                "hide_label": false,
                "show": true,
                "options": [
                    {
                        "image": null,
                        "value": "Design and Budget",
                        "text": "Design and Budget"
                    },
                    {
                        "image": null,
                        "value": "Ready to Hire",
                        "text": "Ready to Hire"
                    },
                    {
                        "image": null,
                        "value": "Comparing Bids",
                        "text": "Comparing Bids"
                    }
                ]
            }', true),
            'validations' => 'required',
            'section_id' => $section->id,
            'order' => 2
        ]);

        factory(\App\Models\Input::class)->create([
            'title' => 'Start',
            'data' =>  json_decode('{
                "type": "radio", 
                "required":true,
                "label": "When you like to start this project?",
                "description": null,
                "image": false,
                "place_holder": null,
                "with_images": false,
                "hide_label": false,
                "show": true,
                "options": [
                    {
                        "image": null,
                        "value": "0 - 3 Months",
                        "text": "0 - 3 Months"
                    },
                    {
                        "image": null,
                        "value": "3 - 6 Months",
                        "text": "3 - 6 Months"
                    },
                    {
                        "image": null,
                        "value": "6 - 12 Months",
                        "text": "6 - 12 Months"
                    }
                ]
            }', true),
            'validations' => 'required',
            'section_id' => $section->id,
            'order' => 3
        ]);

        factory(\App\Models\Input::class)->create([
            'title' => 'Details',
            'data' =>  json_decode('{
                "type": "textarea", 
                "required":true,
                "label": "Customer project focus and priority:",
                "description": "Explain in detail, what are the customers main objectives.",
                "image": false,
                "place_holder": null,
                "hide_label": false,
                "show": true
            }', true),
            'validations' => 'required',
            'section_id' => $section->id,
            'order' => 4
        ]);

        factory(\App\Models\Input::class)->create([
            'title' => 'Porpose',
            'data' =>  json_decode('{
                "type": "radio", 
                "required":true,
                "label": "What is the purpose of this construction or remodel?",
                "description": null,
                "image": false,
                "place_holder": null,
                "with_images": false,
                "hide_label": false,
                "show": true,
                "options": [
                    {
                        "image": null,
                        "value": "Needed Remodel / Repair",
                        "text": "Needed Remodel / Repair"
                    },
                    {
                        "image": null,
                        "value": "Add Value",
                        "text": "Add Value"
                    },
                    {
                        "image": null,
                        "value": "Owner Choice",
                        "text": "Owner Choice"
                    }
                ]
            }', true),
            'validations' => 'required',
            'section_id' => $section->id,
            'order' => 5
        ]);

        factory(\App\Models\Input::class)->create([
            'title' => 'Budget',
            'data' =>  json_decode('{
                "type": "textarea", 
                "required":true,
                "label": "Do you have a Budget in mind for this project?",
                "description": null,
                "image": false,
                "place_holder": null,
                "with_images": false,
                "hide_label": false,
                "show": true
            }', true),
            'validations' => 'required',
            'section_id' => $section->id,
            'order' => 6
        ]);

        factory(\App\Models\Input::class)->create([
            'title' => 'Extra details that you would like to share',
            'data' =>  json_decode('{
                "type": "textarea", 
                "required":false,
                "label": "Extra details that you would like to share",
                "description": null,
                "image": false,
                "place_holder": "",
                "with_images": false,
                "hide_label": false,
                "show": true
            }', true),
            'validations' => 'required',
            'section_id' => $section->id,
            'order' => 7
        ]);


        $step_id = DB::table('steps')->insertGetId([
            'uid' => '636aa788-2b53-4ea7-9ea6-9a265b2002c8',
            'name' => 'Pre-Proposal Document',
            'order' => 2,
            'public' => false,
            'workflow_id' => $workflow_id
        ]);

        $section = factory(\App\Models\Section::class)->create([
            'title' => 'Pre-exp',
            'description' => null,
            'order' => 1,
            'step_id' => $step_id
        ]);

        factory(\App\Models\Input::class)->create([
            'title' => 'Pre-exp',
            'data' =>  json_decode('{
                "type": "preexp", 
                "required":false,
                "label": "",
                "description": null,
                "image": false,
                "place_holder": null,
                "with_images": false,
                "hide_label": false,
                "show": true
            }', true),
            'order' => 1,
            'validations' => 'nullable',
            'section_id' => $section->id,
        ]);

        $step_id = DB::table('steps')->insertGetId([
            'uid' => '13adb7df-42aa-408d-8ee5-fb7a7a880120',
            'name' => 'Post-Exploration Questionnaire',
            'order' => 3,
            'public' => false,
            'workflow_id' => $workflow_id
        ]);

        $section = factory(\App\Models\Section::class)->create([
            'title' => 'Post-exp',
            'description' => null,
            'order' => 1,
            'step_id' => $step_id
        ]);

        factory(\App\Models\Input::class)->create([
            'title' => 'Please describe all the details of the Custom ADU below',
            'data' =>  json_decode('{
                "type": "textarea", 
                "required":true,
                "label": "Please describe all the details of the Custom ADU below",
                "description": null,
                "image": false,
                "place_holder": "Explain in detail your main objectives.",
                "with_images": false,
                "hide_label": false,
                "show": true
            }', true),
            'validations' => 'required',
            'section_id' => $section->id,
            'order' => 1
        ]);


        factory(\App\Models\Input::class)->create([
            'title' => "Please upload 4 pictures around the desired work area",
            'data' =>  json_decode('{
                "type": "imagepicker", 
                "required":true,
                "label": "Please upload 4 pictures around the desired work area",
                "description": null,
                "image": false,
                "place_holder": null,
                "with_images": false,
                "hide_label": false,
                "show": true
            }', true),
            'order' => 1,
            'validations' => 'required',
            'section_id' => $section->id,
        ]);

        $step_id = DB::table('steps')->insertGetId([
            'uid' => '48aeb00c-0102-4bc2-b120-9a51b644804e',
            'name' => 'Project Development Plan (Contract)',
            'order' => 4,
            'public' => false,
            'workflow_id' => $workflow_id
        ]);

        $section = factory(\App\Models\Section::class)->create([
            'title' => 'Project Development Plan (Contract)',
            'description' => null,
            'order' => 1,
            'step_id' => $step_id
        ]);

        factory(\App\Models\Input::class)->create([
            'title' => 'Project Development Plan',
            'data' =>  json_decode('{
                "type": "quick-books", 
                "required":true,
                "label": "",
                "description": null,
                "image": false,
                "place_holder": null,
                "with_images": false,
                "hide_label": false,
                "show": true
            }', true),
            'order' => 1,
            'validations' => 'required',
            'section_id' => $section->id,
        ]);

        $step_id = DB::table('steps')->insertGetId([
            'uid' => '554608b8-246d-405f-95e2-2cd654fa20a8',
            'name' => 'Home Diagnostic Report',
            'order' => 5,
            'public' => false,
            'workflow_id' => $workflow_id,
            'preserve_input_values' => true
        ]);

        $section = factory(\App\Models\Section::class)->create([
            'title' => 'Home Diagnostic Report',
            'description' => null,
            'order' => 1,
            'step_id' => $step_id
        ]);

        factory(\App\Models\Input::class)->create([
            'title' => 'Please add link to Matterport.',
            'data' =>  json_decode('{
                "type": "text", 
                "required":true,
                "label": "Please add link to Matterport.",
                "description": null,
                "image": false,
                "place_holder": null,
                "with_images": false,
                "hide_label": false,
                "show": true
            }', true),
            'validations' => 'required',
            'section_id' => $section->id,
            'order' => 1
        ]);

        factory(\App\Models\Input::class)->create([
            'title' => 'Please add link to Drone Deploy.',
            'data' =>  json_decode('{
                "type": "text", 
                "required":true,
                "label": "Please add link to Drone Deploy.",
                "description": null,
                "image": false,
                "place_holder": null,
                "with_images": false,
                "hide_label": false,
                "show": true
            }', true),
            'validations' => 'required',
            'section_id' => $section->id,
            'order' => 2
        ]);

        factory(\App\Models\Input::class)->create([
            'title' => 'Please add link to Home Inspection Report.',
            'data' =>  json_decode('{
                "type": "text", 
                "required":true,
                "label": "Please add link to Home Inspection Report.",
                "description": null,
                "image": false,
                "place_holder": null,
                "with_images": false,
                "hide_label": false,
                "show": true
            }', true),
            'validations' => 'required',
            'section_id' => $section->id,
            'order' => 3
        ]);

        $step_id = DB::table('steps')->insertGetId([
            'uid' => 'f2c451ea-9221-475d-b8d2-574c73a60b66',
            'name' => '2D Design',
            'order' => 6,
            'public' => false,
            'workflow_id' => $workflow_id,
            'preserve_input_values' => true
        ]);

        $section = factory(\App\Models\Section::class)->create([
            'title' => '2D Design',
            'description' => null,
            'order' => 1,
            'step_id' => $step_id
        ]);

        factory(\App\Models\Input::class)->create([
            'title' => 'Please add file',
            'data' =>  json_decode('{
                "type": "file", 
                "required":true,
                "label": "Please add file",
                "description": null,
                "image": false,
                "place_holder": null,
                "with_images": false,
                "hide_label": false,
                "show": true
            }', true),
            'order' => 1,
            'validations' => 'required',
            'section_id' => $section->id,
        ]);

        $step_id = DB::table('steps')->insertGetId([
            'uid' => '883de088-fe88-42ce-a331-ab31671a8222',
            'name' => 'Estimation',
            'order' => 7,
            'public' => false,
            'workflow_id' => $workflow_id,
            'preserve_input_values' => true
        ]);

        $section = factory(\App\Models\Section::class)->create([
            'title' => 'Estimation',
            'description' => null,
            'order' => 1,
            'step_id' => $step_id
        ]);

        factory(\App\Models\Input::class)->create([
            'title' => 'Please add file',
            'data' =>  json_decode('{
                "type": "file", 
                "required":true,
                "label": "Please add file",
                "description": null,
                "image": false,
                "place_holder": null,
                "with_images": false,
                "hide_label": false,
                "show": true
            }', true),
            'order' => 1,
            'validations' => 'required',
            'section_id' => $section->id,
        ]);

        $step_id = DB::table('steps')->insertGetId([
            'uid' => 'bf19b5cf-404c-48d2-b35c-f00d9e575075',
            'name' => 'Proposal',
            'order' => 8,
            'public' => false,
            'workflow_id' => $workflow_id,
            'preserve_input_values' => true
        ]);

        $section = factory(\App\Models\Section::class)->create([
            'title' => 'Proposal',
            'description' => null,
            'order' => 1,
            'step_id' => $step_id
        ]);

        factory(\App\Models\Input::class)->create([
            'title' => 'Please add file',
            'data' =>  json_decode('{
                "type": "file", 
                "required":true,
                "label": "Please add file",
                "description": null,
                "image": false,
                "place_holder": null,
                "with_images": false,
                "hide_label": false,
                "show": true
            }', true),
            'order' => 1,
            'validations' => 'required',
            'section_id' => $section->id,
        ]);

        $step_id = DB::table('steps')->insertGetId([
            'uid' => '45ac86cd-e5b5-41bd-932f-36e5701a0b23',
            'name' => 'Virtual Reality (Render)',
            'order' => 9,
            'public' => false,
            'workflow_id' => $workflow_id,
            'preserve_input_values' => true
        ]);

        $section = factory(\App\Models\Section::class)->create([
            'title' => 'Virtual Reality (Render)',
            'description' => null,
            'order' => 1,
            'step_id' => $step_id
        ]);

        factory(\App\Models\Input::class)->create([
            'title' => 'Please add link to Shapespark',
            'data' =>  json_decode('{
                "type": "text", 
                "required":true,
                "label": "Please add link to Shapespark",
                "description": null,
                "image": false,
                "place_holder": null,
                "with_images": false,
                "hide_label": false,
                "show": true
            }', true),
            'validations' => 'required',
            'section_id' => $section->id,
            'order' => 1
        ]);

        $step_id = DB::table('steps')->insertGetId([
            'uid' => '5c21b08b-59b1-4f00-93c9-0968cc825f8a',
            'name' => 'Project Development Plan',
            'order' => 10,
            'public' => false,
            'workflow_id' => $workflow_id
        ]);

        $section = factory(\App\Models\Section::class)->create([
            'title' => 'Project Development Plan',
            'description' => null,
            'order' => 1,
            'step_id' => $step_id
        ]);

        factory(\App\Models\Input::class)->create([
            'title' => 'Please add the link',
            'data' =>  json_decode('{
                "type": "text", 
                "required":true,
                "label": "Please add the link",
                "description": null,
                "image": false,
                "place_holder": null,
                "with_images": false,
                "hide_label": false,
                "show": true
            }', true),
            'validations' => 'required',
            'section_id' => $section->id,
            'order' => 1
        ]);
    }
}
