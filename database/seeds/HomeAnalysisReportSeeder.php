<?php

use Illuminate\Database\Seeder;

class HomeAnalysisReportSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $workflow_id = DB::table('workflows')->insertGetId([
            'uid' => '63ac1be7-bf47-4a20-a62a-8909df526c9b',
            'name' => 'Home Analysis Report',
            'slug' => 'home-analysis-report',
            'description' => 'Get a free report analysis for growth and savings',
            'image' => 'https://cdn.develop.gomultitaskr.com/testing/workflows/wizard%20free%20report.jpg',
            'success_image' => 'https://cdn.develop.gomultitaskr.com/testing/workflows/success/free_report.svg',
            'public' => 1,
            'order' => 0,
            'vertical' => 'Reports',
            'created_at' => now(),
            'updated_at' => now(),
            'use' => 'reports'
        ]);

        $step_id = DB::table('steps')->insertGetId([
            'uid' => '16e3def4-01f6-4e0b-90a6-8203f1b33999',
            'name' => 'Home Analysis Report',
            'order' => 1,
            'workflow_id' => $workflow_id,
            'public' => true
        ]);

        $section = factory(\App\Models\Section::class)->create([
            'title' => 'Home Analysis Report',
            'description' => null,
            'order' => 1,
            'step_id' => $step_id,
            'private' => true
        ]);

        factory(\App\Models\Input::class)->create([
            'title' => 'Notes',
            'data' =>  json_decode('{
                "type": "textarea", 
                "required":false,
                "label": "Notes",
                "description": null,
                "image": false,
                "place_holder": "",
                "with_images": false,
                "hide_label": false,
                "show": true
            }', true),
            'validations' => 'nullable',
            'section_id' => $section->id,
            'order' => 1,
            'tag' => null
        ]);

        $step_id = DB::table('steps')->insertGetId([
            'uid' => '16e3def4-01f6-4e0b-90a6-8203f1b3389a',
            'name' => 'Follow Up',
            'order' => 2,
            'workflow_id' => $workflow_id,
            'public' => false
        ]);

        $section = factory(\App\Models\Section::class)->create([
            'title' => 'Follow Up',
            'description' => null,
            'order' => 1,
            'step_id' => $step_id,
            'private' => true
        ]);

        factory(\App\Models\Input::class)->create([
            'title' => 'Contact Type',
            'data' =>  json_decode('{"original":{"options":[{"image":null,"value":"Phone Call","text":"Phone Call","sub_text":""},{"image":null,"value":"Google Meet","text":"Google Meet","sub_text":""},{"image":null,"value":"In-Person (Property Visit)","text":"In-Person (Property Visit)","sub_text":""},{"image":null,"value":"In-Person (Multitaskr office visit)","text":"In-Person (Multitaskr office visit)","sub_text":""},{"image":null,"value":"Zoom Meet","text":"Zoom Meet","sub_text":""}],"type":"radio","hide_label":false,"label":"Contact Type"},"options":[{"image":null,"value":"Phone Call","text":"Phone Call","sub_text":""},{"image":null,"value":"Google Meet","text":"Google Meet","sub_text":""},{"image":null,"value":"In-Person (Property Visit)","text":"In-Person (Property Visit)","sub_text":""},{"image":null,"value":"In-Person (Multitaskr office visit)","text":"In-Person (Multitaskr office visit)","sub_text":""},{"image":null,"value":"Zoom Meet","text":"Zoom Meet","sub_text":""}],"type":"radio","hide_label":false,"label":"Contact Type","index":0}', true),
            'validations' => 'required',
            'section_id' => $section->id,
            'order' => 2,
            'tag' => null
        ]);

        factory(\App\Models\Input::class)->create([
            'title' => 'Comments',
            'data' =>  json_decode('{
                "type": "textarea", 
                "required":false,
                "label": "Comments",
                "description": null,
                "image": false,
                "place_holder": "",
                "with_images": false,
                "hide_label": false,
                "show": true
            }', true),
            'validations' => 'required',
            'section_id' => $section->id,
            'order' => 2,
            'tag' => null
        ]);
    }
}
