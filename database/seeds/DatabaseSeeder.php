<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            AduSeeder::class,
            FreeReportSeeder::class,
            KitchenSeeder::class,
            BathroomSeeder::class,
            LandscapeSeeder::class,
            SolarSeeder::class,
            TestWorkflowSeeder::class,
            CfrSeeder::class,
            Sb9Seeder::class,
            HomeAnalysisReportSeeder::class
        ]);
    }
}
