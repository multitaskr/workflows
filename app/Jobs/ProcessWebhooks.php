<?php

namespace App\Jobs;

use App\Models\InputValue;
use App\Models\Webhook;
use Illuminate\Support\Arr;
use Illuminate\Bus\Queueable;
use App\Webhooks\WebhooksManager;
use Illuminate\Support\Facades\Log;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

use function Clue\StreamFilter\fun;

class ProcessWebhooks implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $workflow_id;

    public $subject_api;

    public $subject_type;

    public $subject_uid;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(int $workflow_id, string $subject_api, string $subject_type, string $subject_uid)
    {
        $this->workflow_id = $workflow_id;
        $this->subject_api = $subject_api;
        $this->subject_type = $subject_type;
        $this->subject_uid = $subject_uid;
        $this->onConnection('database');
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
       
        $values = $this->getParsedValues();

        $webhooks = Webhook::where('workflow_id', $this->workflow_id)->get();

        foreach ($webhooks as $webhook) {
            $manager = WebhooksManager::make(
                Arr::get($webhook->payload, 'service'), 
                Arr::get($webhook->payload, 'model'),
            );

            $manager->dispatch(
                $webhook->payload,
                $values,
                [
                    'method' => $webhook->method,
                    'url' => $webhook->url,
                ],
                [
                    'subject_api' => $this->subject_api,
                    'subject_type' => $this->subject_type,
                    'subject_uid' => $this->subject_uid,
                ]
            );
        }
    }

    /**
     * Get the vakues parsed / 'input_uid' => 'value'
     *
     * @return void
     */
    public function getParsedValues()
    {
        $values = InputValue::where('subject_api', $this->subject_api)
        ->where('subject_type', $this->subject_type)
        ->where('subject_uid', $this->subject_uid)
        ->with('input')
        ->get();

        return $values->map(function($value){
            return [
                $value->input->uid => $value->value 
            ];
        })->collapse()
        ->toArray();
    }
}
