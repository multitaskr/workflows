<?php

namespace App\Listeners;

use App\Models\SubjectStep;
use Illuminate\Support\Str;
use App\Jobs\SendStepMetadata;
use App\Events\SubjectStepActionHandled;

class SendSubjectStepMeta
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  SubjectStepActionHandled  $event
     * @return void
     */
    public function handle(SubjectStepActionHandled $event)
    {
        return SendStepMetadata::dispatch($event->data, Str::lower($event->data['subject']['api']));
    }
}
