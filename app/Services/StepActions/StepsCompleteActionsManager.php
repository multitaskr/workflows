<?php
namespace App\Services\StepActions;

use App\Services\StepActions\StepComplete\CompleteAndFinish;
use Illuminate\Support\Arr;
use App\Services\StepActions\StepComplete\CompleteAndNext;
use App\Services\StepActions\StepComplete\CompleteAndSkip;

class StepsCompleteActionsManager 
{
    public static $drivers = [
        'complete-next' => CompleteAndNext::class,
        'complete-finish' => CompleteAndFinish::class,
        'complete-skip' => CompleteAndSkip::class,
    ];

    public static function resolve(string $driver){
        if(!Arr::has(self::$drivers, $driver)){
            throw new \Exception("The driver ${$driver} is not suported");
        }

        return new self::$drivers[$driver];
    }

    public static function resolveByNextStep($next)
    {
        if(is_null($next)){
            return self::resolve('complete-next');
        }

        if($next == 'finish'){
            return self::resolve('complete-finish');
        }

        return self::resolve('complete-skip');
    }
}
