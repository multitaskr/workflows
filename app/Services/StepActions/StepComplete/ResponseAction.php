<?php 
namespace App\Services\StepActions\StepComplete;

class ResponseAction
{
    public $success;
    public $error_type;
    public $error_message;
    public $data;

    public function __construct(bool $success, string  $error_type = null, string $error_message =  null, array $data = [])
    {
        $this->success = $success;
        $this->error_type = $error_type;
        $this->error_message = $error_message;
        $this->data = $data;
    }

    public static function response(bool $success, string $error_type = null, string $error_message = null, array $data = [])
    {
        return new self($success, $error_type, $error_message, $data);
    }
}
