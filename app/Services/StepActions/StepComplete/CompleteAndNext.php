<?php
namespace App\Services\StepActions\StepComplete;

use App\Models\SubjectStep;
use Illuminate\Support\Carbon;
use App\Events\SubjectStepActionHandled;
use App\Services\StepActions\SubjectStepData;
use App\Services\Multitaskr\Facades\Utilities;
use App\Services\StepActions\StepComplete\ResponseAction;
use App\Services\StepActions\Contracts\StepCompleteActionContract;

class CompleteAndNext implements StepCompleteActionContract
{
    public function handle(SubjectStep $subjectStep,
        $subStatus = null, 
        $user = null, 
        $note = null,  
        $nextStep = null) : ResponseAction{
        
        $subjectStep->update([
            'status' => SubjectStep::COMPLETED_STATUS,
            'sub_status' => $subStatus !== false ? $subStatus: $subjectStep->sub_status,
            'time_worked' => $subjectStep->calculateTimeWorked(),  
        ]);    

        $nextStep = SubjectStep::where('order', $subjectStep->order + 1)
        ->where('subject_api', $subjectStep->subject_api)
        ->where('subject_type', $subjectStep->subject_type)
        ->where('subject_uid', $subjectStep->subject_uid)
        ->with('step')
        ->first();

        if($nextStep){
            $nextStep->update([
                'status' => SubjectStep::IN_PROGRES_STATUS,
                'started_at' => Carbon::now()
            ]);
        }

        $metadata  = $this->getMeta(
            new SubjectStepData(
                $subjectStep->subject_api, 
                $subjectStep->subject_type, 
                $subjectStep->subject_uid
            ),
            $subjectStep,
        );

        SubjectStepActionHandled::dispatch($metadata);

        if($note){
            $this->createNote([
                'body' => $note,
                'subject_uid' => $subjectStep->subject_uid,
                'subject_type' => $subjectStep->subject_type,
                'subject_api' => $subjectStep->subject_api,
                'created_by' => $user? $user->name . ' ' . $user->last_name : null,
                'created_by_uid' => $user? $user->uid : null
            ]);
        }

        return ResponseAction::response(
            true,
            null,
            null,
            []
        );
    }

    public function getMeta(SubjectStepData $data, SubjectStep $subjectStep  = null) : array
    {
        $nextStep = SubjectStep::where('order', $subjectStep->order + 1)
        ->where('subject_api', $subjectStep->subject_api)
        ->where('subject_type', $subjectStep->subject_type)
        ->where('subject_uid', $subjectStep->subject_uid)
        ->with('step')
        ->first();

        return [
            'steps' => [
                'prev_step_name' => $subjectStep->name,
                'prev_step_status' => $subjectStep->status,
                'prev_step_sub_status' => $subjectStep->sub_status,
                'prev_step_uid' =>  $subjectStep->step->uid,
                'prev_subject_step_uid' => $subjectStep->uid,
                'current_step_name' => $nextStep ? $nextStep->name : null,
                'current_step_status' => $nextStep ? $nextStep->status : null,
                'current_step_sub_status' => $nextStep ? $nextStep->sub_status : null,
                'current_step_uid' => $nextStep ? $nextStep->step->uid : null,
                'current_subject_step_uid' => $nextStep ? $nextStep->uid : null,
                'progress' => SubjectStep::calculateStepsProgress($data->api, $data->type, $data->uid)
            ],
            'subject' => [
                'api' => $data->api,
                'type' => $data->type,
                'uid' => $data->uid,
            ],
            'event' => 'complete'
        ];;
    }
    
    public function validateNextStep(SubjectStepData $data, string $nextStep = null, SubjectStep $subjectStep = null) : ResponseAction
    {
        return ResponseAction::response(
            true,
            null,
            null,
            []
        );
    }

    public function createNote($data = [])
    {
        Utilities::createPrivateNote($data);
    }
}
