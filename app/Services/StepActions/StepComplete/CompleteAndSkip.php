<?php
namespace App\Services\StepActions\StepComplete;

use App\Models\SubjectStep;
use Illuminate\Support\Carbon;
use App\Events\SubjectStepActionHandled;
use App\Services\StepActions\SubjectStepData;
use App\Services\Multitaskr\Facades\Utilities;
use App\Services\StepActions\StepComplete\ResponseAction;
use App\Services\StepActions\Contracts\StepCompleteActionContract;

class CompleteAndSkip implements StepCompleteActionContract
{
    public function handle(SubjectStep $subjectStep,
        $subStatus = null, 
        $user = null, 
        $note = null,  
        $nextStep = null) : ResponseAction{
        
        
        $response = $this->validateNextStep(
            new SubjectStepData(
                $subjectStep->subject_api, 
                $subjectStep->subject_type, 
                $subjectStep->subject_uid
            ),
            $nextStep, 
            $subjectStep
        );

        if(!$response->success){
            return $response;
        }

        $subjectStep->update([
            'status' => SubjectStep::COMPLETED_STATUS,
            'sub_status' => $subStatus !== false ? $subStatus: $subjectStep->sub_status,
            'time_worked' => $subjectStep->calculateTimeWorked(),  
        ]);

        foreach ($response->data['skipped_steps'] as $skippedStep) {
            $skippedStep->update([
                'status' => SubjectStep::COMPLETED_STATUS,
                'time_worked' => $skippedStep->time_worked ?: 0
            ]);
        }

        
        $response->data['next_step']->update([
            'status' => SubjectStep::IN_PROGRES_STATUS,
            'started_at' => Carbon::now()
        ]);

        $metadata = $this->getMeta(new SubjectStepData(
                $subjectStep->subject_api, 
                $subjectStep->subject_type, 
                $subjectStep->subject_uid
            ),
            $response->data['next_step']
        );

        SubjectStepActionHandled::dispatch($metadata);

        if($note){
            $this->createNote([
                'body' => $note,
                'subject_uid' => $subjectStep->subject_uid,
                'subject_type' => $subjectStep->subject_type,
                'subject_api' => $subjectStep->subject_api,
                'created_by' => $user? $user->name . ' ' . $user->last_name : null,
                'created_by_uid' => $user? $user->uid : null
            ]);
        }

        return ResponseAction::response(
            true,
            null,
            null,
            []
        );
    }   

    public function getMeta(SubjectStepData $data, SubjectStep $subjectStep  = null) : array
    {
        $newPrevStep = SubjectStep::where('order', $subjectStep->order - 1)
        ->where('subject_api', $data->api)
        ->where('subject_type', $data->type)
        ->where('subject_uid', $data->uid)
        ->first();

        return [
            'steps' => [
                'prev_step_name' => $newPrevStep ? $newPrevStep->name : null,
                'prev_step_status' => $newPrevStep ? $newPrevStep->status : null,
                'prev_step_sub_status' => $newPrevStep ? $newPrevStep->sub_status : null,
                'prev_step_uid' => $newPrevStep ? $newPrevStep->step->uid : null,
                'prev_subject_step_uid' => $newPrevStep ? $newPrevStep->uid : null,
                'current_step_name' => $subjectStep->name,
                'current_step_status' => $subjectStep->status,
                'current_step_sub_status' => $subjectStep->sub_status,
                'current_step_uid' =>  $subjectStep->step->uid,
                'current_subject_step_uid' => $subjectStep->uid,
                'progress' => SubjectStep::calculateStepsProgress($data->api, $data->type, $data->uid)
            ],
            'subject' => [
                'api' => $data->api,
                'type' => $data->type,
                'uid' => $data->uid,
            ],
            'event' => 'complete'
        ];
    }

    public function validateNextStep(SubjectStepData $data, string $nextStep, SubjectStep $subjectStep = null) : ResponseAction
    {
        $nextStep = SubjectStep::whereUid($nextStep)->first();

        if($nextStep->status == SubjectStep::COMPLETED_STATUS){
            return ResponseAction::response(
                false,
                'validation',
                'The next step cannot be a completed step',
                []
            );
        }

        if($nextStep->order < $subjectStep->order){
            return ResponseAction::response(
                false,
                'validation',
                'The next step cannot be a previous step',
                []
            );
        }

        $skippedSteps = SubjectStep::where('order', '>', $subjectStep->order)
        ->where('order', '<', $nextStep->order)
        ->where([
            'subject_api' => $data->api, 
            'subject_type' => $data->type, 
            'subject_uid' => $data->uid, 
        ])
        ->with('step')
        ->get();

        foreach ($skippedSteps as $skippedStep) {
            if($skippedStep->step->required){
                return ResponseAction::response(
                    false,
                    'validation',
                    "Exists required steps before the selected step",
                    []
                );
            }
        }

        return ResponseAction::response(
            true,
            null,
            null,
            [
                'next_step' => $nextStep,
                'skipped_steps' => $skippedSteps
            ]
        );
    }

    public function createNote($data = [])
    {
        Utilities::createPrivateNote($data);
    }
}
