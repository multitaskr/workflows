<?php
namespace App\Services\StepActions;

use App\Services\StepActions\StepRollback\RollbackAndPrev;
use Illuminate\Support\Arr;

class StepsRollbackActionsManager
{
    public static $drivers = [
        'rollback-prev' => RollbackAndPrev::class
    ];

    public static function resolve(string $driver){
        if(!Arr::has(self::$drivers, $driver)){
            throw new \Exception("The driver ${$driver} is not suported");
        }

        return new self::$drivers[$driver];
    }
}
