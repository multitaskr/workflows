<?php
namespace App\Services\StepActions;

use Illuminate\Support\Arr;
use App\Services\StepActions\StepsClone\StepsCloneByWorkflowAction;

class StepsCloneActionsManager 
{
    public static $drivers = [
        'workflow' => StepsCloneByWorkflowAction::class
    ];

    public static function resolve(string $driver){
        if(!Arr::has(self::$drivers, $driver)){
            throw new \Exception("The driver ${$driver} is not suported");
        }

        return new self::$drivers[$driver];
    }
}
