<?php
namespace App\Services\StepActions\StepsClone;

use App\Events\SubjectStepActionHandled;
use App\Models\SubjectStep;
use App\Models\Workflow;
use App\Services\StepActions\Contracts\StepsCloneActionContract;
use App\Services\StepActions\SubjectStepData;
use Carbon\Carbon;

class StepsCloneByWorkflowAction  implements StepsCloneActionContract
{
    public function handle(SubjectStepData $data, $workflow = null)
    {
        $workflow = Workflow::whereUid($workflow)->first();

        if(!$workflow->clone_steps){
            return [];
        };

        $subject_steps = SubjectStep::where('subject_api', $data->api)
        ->where('subject_type', $data->type)
        ->where('subject_uid', $data->uid)
        ->get();

        if(!$subject_steps->isEmpty()){
            return $subject_steps;
        }

        $subject_steps = collect();

        foreach ($workflow->steps as $step) {
            $subject_step = SubjectStep::create([
                'name' => $step->name,
                'order' => $step->order,
                'estimated_time' => $step->estimated_time,
                'status' => SubjectStep::PENDING_STATUS,
                'sub_status' =>  null,
                'public' => $step->public,
                'customer_edit' => $step->customer_edit,
                'hidden_from_customer' => $step->hidden_from_customer,
                'subject_api' => $data->api,
                'subject_type' => $data->type,
                'subject_uid' => $data->uid,
                'step_id' => $step->id
            ]);

            if($step->order == 1) {
                $subject_step->update([
                    'status' => SubjectStep::IN_PROGRES_STATUS,
                    'started_at' => Carbon::now(),
                ]);
            }

            $subject_steps->push($subject_step);
        }  

        $meta_data = $this->getMeta($data);

        SubjectStepActionHandled::dispatch($meta_data);

        return $subject_steps;
    }


    public function getMeta(SubjectStepData $data) : array
    {
        $subject_step = SubjectStep::where([
            'subject_api' => $data->api,
            'subject_type' => $data->type,
            'subject_uid' => $data->uid,
            'order' => 1
        ])->with('step')
       ->first();

        return [
            'steps' => [
                'prev_step_name' => null,
                'prev_step_status' => null,
                'prev_step_sub_status' => null,
                'prev_step_uid' => null,
                'prev_subject_step_uid' => null,
                'current_step_name' => $subject_step->name,
                'current_step_status' => $subject_step->status,
                'current_step_sub_status' => $subject_step->sub_status,
                'current_step_uid' => $subject_step->step->uid,
                'current_subject_step_uid' => $subject_step->uid,
                'progress' => 0
            ],
            'subject' => [
                'api' => $data->api,
                'type' => $data->type,
                'uid' => $data->uid,
            ],
            'event' => 'clone'
        ];
    }
}
