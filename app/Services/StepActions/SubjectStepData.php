<?php
namespace App\Services\StepActions;

class SubjectStepData
{
    public $api;

    public $type;

    public $uid;

    public function __construct($api, $type, $uid)
    {
        $this->api =  $api;
        $this->type = $type;
        $this->uid = $uid; 
    }
}
