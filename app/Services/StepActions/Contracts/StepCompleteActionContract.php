<?php
namespace App\Services\StepActions\Contracts;

use App\Models\SubjectStep;
use App\Services\StepActions\StepComplete\ResponseAction;
use App\Services\StepActions\SubjectStepData;

interface StepCompleteActionContract{
    public function handle(
        SubjectStep $subjectStep,
        $subStatus = null, 
        $user = null, 
        $note = null,  
        $nextStep = null
    ) : ResponseAction;

    public  function getMeta(SubjectStepData $data, SubjectStep $subjectStep = null) : array;

    public  function validateNextStep(SubjectStepData $data, string $nextStep, SubjectStep $subject_step =  null) : ResponseAction;

    public function createNote($data = []);
}