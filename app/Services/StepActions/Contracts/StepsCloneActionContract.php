<?php
namespace App\Services\StepActions\Contracts;

use App\Services\StepActions\SubjectStepData;


interface StepsCloneActionContract{
    public function handle(SubjectStepData $data, $procesor = null);

    public  function getMeta(SubjectStepData $data) : array;
}