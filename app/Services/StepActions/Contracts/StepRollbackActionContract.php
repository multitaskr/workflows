<?php
namespace App\Services\StepActions\Contracts;

use App\Models\SubjectStep;
use App\Services\StepActions\SubjectStepData;

interface StepRollbackActionContract{
    public function handle(
        SubjectStep $subjectStep,
        $subStatus = null, 
        $user = null, 
        $note = null 
    );

    public  function getMeta(SubjectStepData $data, SubjectStep $subjectStep = null) : array;

    public function createNote($data = []);
}