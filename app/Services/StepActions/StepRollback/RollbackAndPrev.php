<?php
namespace App\Services\StepActions\StepRollback;

use App\Models\SubjectStep;
use Illuminate\Support\Carbon;
use App\Events\SubjectStepActionHandled;
use App\Services\StepActions\SubjectStepData;
use App\Services\Multitaskr\Facades\Utilities;
use App\Services\StepActions\Contracts\StepRollbackActionContract;


class RollbackAndPrev implements StepRollbackActionContract
{
    public function handle(
        SubjectStep $subjectStep,
        $subStatus = null, 
        $user = null, 
        $note = null){

        if($subjectStep->order == 1){
            return $subjectStep;
        }
        
        $subjectStep->update([
            'status' => SubjectStep::PENDING_STATUS,
            'sub_status' => $subStatus !== false ? $subStatus: $subjectStep->sub_status,
        ]);    

        $newPrevStep = SubjectStep::where('order', $subjectStep->order - 1)
        ->where('subject_api', $subjectStep->subject_api)
        ->where('subject_type', $subjectStep->subject_type)
        ->where('subject_uid', $subjectStep->subject_uid)
        ->first();

        if($newPrevStep){
            $newPrevStep->update([
                'status' => SubjectStep::IN_PROGRES_STATUS,
                'started_at' => Carbon::now()
            ]);
        }


        $metadata  = $this->getMeta(
            new SubjectStepData(
                $subjectStep->subject_api, 
                $subjectStep->subject_type, 
                $subjectStep->subject_uid
            ),
            $newPrevStep,
        );

        SubjectStepActionHandled::dispatch($metadata);

        if($note){
            $this->createNote([
                'body' => $note,
                'subject_uid' => $subjectStep->subject_uid,
                'subject_type' => $subjectStep->subject_type,
                'subject_api' => $subjectStep->subject_api,
                'created_by' => $user? $user->name . ' ' . $user->last_name : null,
                'created_by_uid' => $user? $user->uid : null
            ]);
        }

        return $subjectStep;
    }

    public function getMeta(SubjectStepData $data, SubjectStep $subjectStep  = null) : array
    {
        $newPrevStep = SubjectStep::where('order', $subjectStep->order - 1)
        ->where('subject_api', $data->api)
        ->where('subject_type', $data->type)
        ->where('subject_uid', $data->uid)
        ->first();

        return [
            'steps' => [
                'prev_step_name' => $newPrevStep ? $newPrevStep->name : null,
                'prev_step_status' => $newPrevStep ? $newPrevStep->status : null,
                'prev_step_sub_status' => $newPrevStep ? $newPrevStep->sub_status : null,
                'prev_step_uid' => $newPrevStep ? $newPrevStep->step->uid : null,
                'prev_subject_step_uid' => $newPrevStep ? $newPrevStep->uid : null,
                'current_step_name' => $subjectStep->name,
                'current_step_status' => $subjectStep->status,
                'current_step_sub_status' => $subjectStep->sub_status,
                'current_step_uid' =>  $subjectStep->step->uid,
                'current_subject_step_uid' => $subjectStep->uid,
                'progress' => SubjectStep::calculateStepsProgress($data->api, $data->type, $data->uid)
            ],
            'subject' => [
                'api' => $data->api,
                'type' => $data->type,
                'uid' => $data->uid,
            ],
            'event' => 'complete'
        ];
    }

    public function createNote($data = [])
    {
        Utilities::createPrivateNote($data);
    }
}
