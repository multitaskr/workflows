<?php

namespace App\Services\Multitaskr;

use Illuminate\Container\Container;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Log;
use App\Services\Multitaskr\Exceptions\HostNotDefined;
use ResourceBundle;

class Properties extends Client
{
    // Api host
    private $host;

    public function __construct($host)
    {
        if (!$host) {
            throw new HostNotDefined('Please define a host');
        }

        $this->host = $host;
    }

    /**
     * Load property data from properties of given user
     *
     * @param string $user_uid
     * @return void
     */
    public function loadPropertyDataOfUser($user_uid)
    {
        $response = $this->request('post', $this->host . '/property-data/user/' . $user_uid);

        if($response->failed()){
            return null;
        }

        return json_decode($response->body());
    }
}
