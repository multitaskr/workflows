<?php

namespace App\Services\Multitaskr\Facades;

use Illuminate\Support\Facades\Facade;

class Deals extends Facade 
{
    protected static function getFacadeAccessor() { return 'deals'; }
}
