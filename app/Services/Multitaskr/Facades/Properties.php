<?php

namespace App\Services\Multitaskr\Facades;

use Illuminate\Support\Facades\Facade;

class Properties extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'properties';
    }
}
