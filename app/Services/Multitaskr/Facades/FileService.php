<?php
namespace App\Services\Multitaskr\Facades;

use Illuminate\Support\Facades\Facade;

class FileService extends Facade 
{
    protected static function getFacadeAccessor() { return 'file_service'; }
}
