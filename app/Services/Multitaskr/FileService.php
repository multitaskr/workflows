<?php 
namespace App\Services\Multitaskr;

use Illuminate\Support\Facades\Http;
use App\Services\Multitaskr\Exceptions\HostNotDefined;
use Illuminate\Support\Facades\App;

class FileService extends Client
{
    // Api host
    private $host;

    public function __construct($host)
    {
        if (!$host){
            throw new HostNotDefined('Please define a host');
        }

        $this->host = $host;
    }

    /**
     * Insert file for permission
     *
     * @param string $uid
    **/
    public function store($params = [])
    {
        if(App::environment('testing')){
            return [];
        }

        $response = $this->request('post', $this->host . '/files', $params);

        if ($response->failed()) {
            return false;
        }

        return json_decode($response->body())->data;
    }
}