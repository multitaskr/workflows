<?php
namespace App\Services\Multitaskr;

use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Storage;
use App\Services\Multitaskr\Exceptions\MethodNotAllowedException;


class Client
{
    protected static $methods = [
        'get',
        'post',
        'patch',
        'put',
        'delete'
    ];

    public function request(string $method, string $url, ?array $data = null)
    {
        if(!in_array($method, self::$methods)){
            throw new MethodNotAllowedException('Http method is not allowed');
        }

        $response = Http::withHeaders([
            "X-Requested-With" => "XMLHttpRequest"
        ])->{$method}($url , $data);

        return $response;
    }  
    
    public function requestWithFiles(string $method, string $url, ?array $data = [], ?array $files = [])
    {
        if(!in_array($method, self::$methods)){
            throw new MethodNotAllowedException('Http method is not allowed');
        }

        $response = Http::withHeaders([
            "X-Requested-With" => "XMLHttpRequest"
        ]);

        foreach($files as $k => $file){
            $response->attach('file', Storage::get($file), $file);
        }
        
        $response = $response->post($url , $data);
       
        return $response;
    }  
}