<?php

namespace App\Services\Multitaskr;

use App\Services\Multitaskr\Exceptions\HostNotDefined;
use Illuminate\Support\Facades\App;


class Deals extends Client
{
    // Api host
    private $host;

    public function __construct($host)
    {
        if (!$host){
            throw new HostNotDefined('Please define a host');
        }

        $this->host = $host;
    }

    /**
     * Get the deal detail by its uid's from Deals API
     *
     * @param string $dealUid
     * @param bool $withUser
     * @return void
     */
    public function getDeal($dealUid, $withUser = false, $withOwner = false)
    {

        if (App::environment('testing')) {
            return [];
        }

        $params = [];

        if ($withUser) { $params['user'] = true; }
        if ($withOwner) { $params['owner'] = true; }

        $response = $this->request('get', $this->host . '/deals/' . $dealUid, $params);
        
        if ($response->failed()){
            return null;
        }

        $json = json_decode($response->body());
        return $json->data;
    }
    
    /**
     * Get the deal detail by its uid's from Deals API
     *
     * @param string $dealUid
     * @param bool $withUser
     * @return void
     */
    public function getInvoices($params = [])
    {

        if (App::environment('testing')) {
            return [];
        }

        $response = $this->request('get', $this->host . '/invoices', $params);

        if ($response->failed()){
            return null;
        }

        $json = json_decode($response->body());
        return $json->data;
    }
}
