<?php
namespace App\Services\Multitaskr;

use Illuminate\Support\ServiceProvider;

class MultitaskrServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('utilities', function ($app) {
            return new Utilities(env('UTILITIES_HOST'),  env('UTILITIES_PRIVATE_HOST'));
        });

        $this->app->bind('file_service', function ($app) {
            return new FileService(env('FILES_HOST'));
        });

        $this->app->bind('deals', function ($app) {
            return new Deals(env('DEALS_HOST'));
        });

        $this->app->bind('properties', function ($app) {
            return new Properties(env('PROPERTIES_HOST'));
        });

        $this->app->bind('sso', function ($app) {
            return new Sso(env('SSO_HOST'));
        });
    }
}