<?php

namespace App\Services\Multitaskr;

use Faker\Generator;
use Illuminate\Container\Container;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Log;
use App\Services\Multitaskr\Exceptions\HostNotDefined;

class Sso extends Client
{
    // Api host
    private $host;

    private $faker;

    public function __construct($host)
    {
        if (!$host) {
            throw new HostNotDefined('Please define a host');
        }

        $this->host = $host;

        $this->faker = Container::getInstance()->make(Generator::class);
    }

    /**
     * Get the user info from SSO API
     *
     * @param string $value
     * @return void
     */
    public function getUser($value = null)
    {
        if (App::environment('testing')) {

            return (object) [
                'email' => $this->faker->unique()->safeEmail,
                'uid' => $this->faker->uuid,
                'full_name' => $this->faker->name,
                'name' => $this->faker->name,
                'last_name' => $this->faker->name,
            ];
        }

        if (!$value) {
            return null;
        }

        $response = $this->request('get', $this->host . "/users/{$value}");

        if ($response->failed()) {
            return null;
        }

        return json_decode($response->body())->data;
    }
}
