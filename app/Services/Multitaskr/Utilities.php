<?php
namespace App\Services\Multitaskr;

use Faker\Generator;
use Illuminate\Container\Container;
use Illuminate\Support\Facades\App;
use App\Services\Multitaskr\Exceptions\HostNotDefined;

class Utilities extends Client
{
    // Api host
    private $host;

    private $faker;

    public function __construct($host, $privateHost)
    {
        if (!$host){
            throw new HostNotDefined('Please define a host');
        }

        $this->host = $host;
        $this->privateHost = $privateHost;

        $this->faker = Container::getInstance()->make(Generator::class);
    }

    /**
     * 
     *
     * @param string $uid
     * @return void
     */
    public function createFiles($data, $files)
    {
        if(App::environment('testing')){
            return [];
        }

        $response = $this->requestWithFiles('post', $this->host . "utilities/private-files", $data, $files);
        
        if($response->failed()){
            return [];
        }

        return json_decode($response->body())->data;
    }

    /**
     * Insert Input Value by Input Uid
     *
     * @param string $uid
     **/
    public function createPrivateNote($params = [])
    {
        if (App::environment('testing')) {
            return [];
        }

        $response = $this->request('post', $this->privateHost . '/comments', $params);
       
        if ($response->failed()) {
            return false;
        }

        return json_decode($response->body())->data;
    }
}
