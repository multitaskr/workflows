<?php 
namespace App\Webhooks;

use App\Models\Webhook;
use App\Models\Workflow;
use Illuminate\Support\Arr;
use App\Jobs\ProcessWebhooks;
use App\Webhooks\Services\Sso\User;
use App\Webhooks\Services\Custom\Custom;
use App\Webhooks\Exceptions\ModelNotExistsException;
use App\Webhooks\Services\Properties\Property;

class WebhooksManager 
{
    const SERVICES = [ 
        "sso" => [
            "user" => User::class
        ],
        "properties" => [
            "property" => Property::class
        ],
        "custom" => Custom::class
    ];


    /**
     * Maker instance of model service
     *
     * @param string $service
     * @param string|null $model
     * @return void
     */
    public static function make(string $service, string $model = null)
    {
        if($service === 'custom'){
            $model = self::SERVICES['custom']; 
            return new $model;
        }        

        $model = Arr::get(self::SERVICES, "$service.$model", null);

        if(!$model){
            throw new ModelNotExistsException("Webhook service model does not exists");
        }

        return new $model;
    }

    /**
     * 
     *
     * @param Workflow $workflow
     * @return void
     */
    public static function queueWebhooks(Workflow $workflow,  string $subjec_api, string $subject_type, string $subject_uid)
    {
        if(!self::hasWebhooks($workflow)){
            return;
        }

        ProcessWebhooks::dispatch(
            $workflow->id, 
            $subjec_api, 
            $subject_type, 
            $subject_uid
        );
    }

    /**
     * Return the service property 
     *
     * @return array
     */
    public static function services()
    {
        return self::SERVICES;
    }

    /**
     * Return services key 
     *
     * @return array
     */
    public static function servicesList()
    {
        return array_keys(self::SERVICES);
    }

    /**
     * Return models of selected service
     *
     * @param string $service
     * @return array
     */    
    public static function serviceModels(string $service)
    {
        if($service === "custom") return [];

        return array_keys( Arr::get(self::SERVICES, $service, []) );
    }

    /**
     * Get model columns
     *
     * @param string $service
     * @param string $model
     * @return array
     */
    public static function modelColumns(string $service, string $model)
    {
        $model = Arr::get(self::SERVICES, "$service.$model", null);

        return $model ? $model::columns() : [];
    }

    /**
     * Get if workflow has webhooks
     *
     * @param Workflow $workflow
     * @return boolean
     */
    public static function hasWebhooks(Workflow $workflow)
    {
        return Webhook::where('workflow_id', $workflow->id)->exists();
    }
}
