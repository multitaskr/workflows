<?php
namespace App\Webhooks\Services\Contracts;

Interface ModelContract{

    public function dispatch(array $config,  array $body, array $requestParams, array $subject = []);

}