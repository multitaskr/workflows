<?php 
namespace App\Webhooks\Services\Custom;

use App\Models\Workflow;
use Illuminate\Support\Arr;
use Illuminate\Http\Request;
use App\Webhooks\Services\Client;
use App\Webhooks\Services\Traits\Utils;
use App\Webhooks\Services\Contracts\ModelContract;

class Custom extends Client implements ModelContract
{
    use Utils;

    /**
     * Parde data to create webhook instance
     *
     * @param Request $request
     * @return void
     */
    public function parseData(Request $request)
    {
    
        return [
            'url' => $request->url,
            'method' => $request->method,
            'workflow_id' => Workflow::whereUid($request->workflow)->first()->id,
            'payload' => array_merge($request->payload, [
                'service' => $request->service,
            ])
        ];
    }

     /**
     * Trigger 
     *
     * @param array $config
     * @param array $body
     * @param array $subject
     * @param array $requestParams
     * @return void
     */
    public function dispatch(array $config,  array $body, array $requestParams, array $subject = [])
    {
        $body = $this->parseInputsBody($config, $body);

        $request_method = Arr::get($requestParams, 'method', 'POST');

        $request_url = Arr::get($requestParams, 'url', null);

        if(!$request_url || empty($body)) {
            return null;
        }

        $this->request($request_method, $request_url, $body);
    }
}
