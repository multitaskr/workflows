<?php 
namespace App\Webhooks\Services\Traits;

use Illuminate\Support\Arr;

/**
 * 
 */
trait Utils
{
    public function parseInputsBody(array $config, array $body )
    {
        $bodyConfig = $config['body'];

        $parsedBody = [];

        $isIncomplete = false;

        foreach ($bodyConfig as $input => $config) {

            if(Arr::has($body, $input)){
                $value = Arr::get($body, $input);
                $parsedBody[$config['body_param']] = $value;
            }else{
                $isIncomplete = true;
                break;
            }   
        }

        if($isIncomplete){
            return [];
        }

        return $parsedBody;
    }
}
