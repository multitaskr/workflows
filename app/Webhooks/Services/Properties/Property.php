<?php
namespace App\Webhooks\Services\Properties;

use Illuminate\Support\Arr;
use App\Webhooks\Services\Traits\Utils;
use App\Webhooks\Services\Contracts\ModelContract;

class Property extends PropertiesService implements ModelContract
{
    use Utils;

    /**
     * Get service model columns
     *
     * @return void
     */
    public static function columns()
    {
        return [
            'name' => 'Name',
            'street_name' => 'Street Name',
            'street_number' => 'Street Number',
            'unit' => 'Unit',
            'city' => 'City',
            'state' => 'State',
            'zip_code' => 'Zip Code',
            'address' => 'Address',
            'latitude' => 'Latitude',
            'longitude' => 'Longitude',
        ];
    }

    /**
     * Trigger 
     *
     * @param array $config
     * @param array $body
     * @param array $subject
     * @param array $requestParams
     * @return void
     */
    public function dispatch(array $config,  array $body, array $requestParams, array $subject = [])
    {
        $subject_body = $this->getProperty($subject);
       
        $request_method = Arr::get($requestParams, 'method', 'POST');

        $request_url = Arr::get($requestParams, 'url', null);

        $inputs_body = $this->parseInputsBody($config, $body);

        if(empty($subject_body) || empty($inputs_body) || !$request_url  ) {
            return null;
        }

        $this->request(
            $request_method, 
            $request_url, 
            array_merge($subject_body,['body' =>  $inputs_body])
        );
    }


    public function getProperty(array $subject)
    {
        $subject_api = Arr::get($subject, 'subject_api');
        $subject_type = Arr::get($subject, 'subject_type');
        $subject_uid = Arr::get($subject, 'subject_uid');

        if($subject_api == 'properties' && $subject_type == 'Property'){
            return [
                'model' => 'property',
                'uid' =>  $subject_uid
            ];
        }

        return [];
    }

}
