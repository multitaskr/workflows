<?php 
namespace App\Webhooks\Services\Properties;

use App\Models\Workflow;
use Illuminate\Http\Request;
use App\Webhooks\Services\Client;

abstract class PropertiesService extends Client
{
    /**
     * Webhook service  url
     *
     * @return void
     */
    public function webhookUrl()
    {
        return env('PROPERTIES_HOST', null) . "/webhooks/workflows";
    }

    /**
     * Get webhook method
     *
     * @return string
     */
    public function method()
    {
        return 'POST';
    }

    /**
     * Parde data to create webhook instance
     *
     * @param Request $request
     * @return void
     */
    public function parseData(Request $request)
    {
    
        return [
            'url' => $this->webhookUrl(),
            'method' => $this->method(),
            'workflow_id' => Workflow::whereUid($request->workflow)->first()->id,
            'payload' => array_merge($request->payload, [
                'service' => $request->service,
                'model' => $request->model
            ])
        ];
    }
}