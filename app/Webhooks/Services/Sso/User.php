<?php
namespace App\Webhooks\Services\Sso;

use App\Webhooks\Services\Contracts\ModelContract;
use App\Webhooks\Services\Traits\Utils;
use Illuminate\Support\Arr;
use Deals;

class User extends SsoService implements ModelContract
{
    use Utils;

    /**
     * Get service model columns
     *
     * @return void
     */
    public static function columns()
    {
        return [
            'name' => 'Name',
            'last_name' => 'Last Name',
            'email' => 'Email',
            'phone' => 'Phone',
            'birth_date' => 'Birth Date',
            'ssn' => 'SSN',
            'language' => 'Language',
            'metadata' => 'Metadata',
            'homeowner' => 'Homeowner'
        ];
    }

    /**
     * Trigger 
     *
     * @param array $config
     * @param array $body
     * @param array $subject
     * @param array $requestParams
     * @return void
     */
    public function dispatch(array $config,  array $body, array $requestParams, array $subject = [])
    {
        $subject_body = $this->getUser($subject);

        $request_method = Arr::get($requestParams, 'method', 'POST');

        $request_url = Arr::get($requestParams, 'url', null);

        $inputs_body = $this->parseInputsBody($config, $body);

        if(empty($subject_body) || empty($inputs_body) || !$request_url  ) {
            return null;
        }

        $this->request(
            $request_method, 
            $request_url, 
            array_merge($subject_body,['body' =>  $inputs_body])
        );
    }


    public function getUser(array $subject)
    {
        $subject_api = Arr::get($subject, 'subject_api');
        $subject_type = Arr::get($subject, 'subject_type');
        $subject_uid = Arr::get($subject, 'subject_uid');

        if($subject_api == 'sso' && $subject_type == 'User'){
            return [
                'model' => 'user',
                'uid' =>  $subject_uid
            ];
        }

        if($subject_api == 'deals' && $subject_type == 'Deals'){
            $deal = Deals::getDeal($subject_uid, $withUser = true);

            if($deal->user){
                return [
                    'model' => 'user',
                    'uid' =>  $deal->user->uid
                ];
            }
        }

        return [];
    }
}
