<?php
namespace App\Webhooks\Services;

use Illuminate\Support\Str;
use Illuminate\Support\Facades\Http;
use App\Webhooks\Exceptions\MethodNotAllowedException;

class Client
{
    protected static $methods = [
        'get',
        'post',
        'patch',
        'put',
        'delete'
    ];

    public function request(string $method, string $url, ?array $data = null)
    {
        $method = Str::lower($method);

        if(!in_array($method, self::$methods)){
            throw new MethodNotAllowedException('Http method is not allowed');
        }

        $response = Http::withHeaders([
            "X-Requested-With" => "XMLHttpRequest"
        ])->{$method}($url , $data);

        return $response;
    }  
}
