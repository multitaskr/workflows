<?php

namespace App\Models;

use App\Filters\Shared\SortFilter;
use App\Filters\Shared\FieldsFilter;
use App\Filters\SubjectStep\StepFilter;
use App\Filters\SubjectStep\SubjectApiFilter;
use App\Filters\SubjectStep\SubjectTypeFilter;
use App\Filters\SubjectStep\SubjectUidFilter;
use App\Models\Traits\Filterable;
use App\Models\Traits\Uidable;
use App\Services\StepActions\StepsCloneActionsManager;
use App\Services\StepActions\StepsCompleteActionsManager;
use App\Services\StepActions\SubjectStepData;
use App\Services\StepActions\StepComplete\ResponseAction;
use App\Services\StepActions\StepsRollbackActionsManager;
use Carbon\CarbonInterval;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Carbon;

class SubjectStep extends Model
{
    use Uidable, SoftDeletes, Filterable;

     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

     /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'created_at' => 'datetime:Y-m-d\TH:i:sP',
        'updated_at' => 'datetime:Y-m-d\TH:i:sP',
        'deleted_at' => 'datetime:Y-m-d\TH:i:sP',
        'public' => 'boolean',
        'customer_edit' => 'boolean',
        'hidden_from_customer' => 'boolean',
    ];

    public const COMPLETED_STATUS = 'completed';
    public const SKIP_STATUS = 'skipped';
    public const IN_PROGRES_STATUS = 'in progress';
    public const PENDING_STATUS = 'pending';

     /**
     * Return the filters that should be use by default
     *
     * @return array
     */
    public function filters(): array
    { 
        return  [
            'fields' => new FieldsFilter('subject_steps'),
            'sort' => new SortFilter('subject_steps'),
            'subject_uid' => SubjectUidFilter::class,
            'subject_type' => SubjectTypeFilter::class,
            'subject_api' => SubjectApiFilter::class,
            'step' => StepFilter::class
        ];
    }

    /**
     * Get the step that owns the SubjectStep
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function step(): BelongsTo
    {
        return $this->belongsTo(Step::class);
    }

    /**
     * Get the deal step's time worked.
     *
     * @param  string  $value
     * @return void
     */
    public function getTimeWorkedForHummansAttribute($value)
    {
        if(!$this->time_worked)
            return null;
      
        $hours = floor($this->time_worked);

        $minutes = round(60*($this->time_worked - $hours));

        return CarbonInterval::hours($hours)->minutes($minutes)->cascade()->forHumans();
    }

     /**
     * Get available statuses
     *
     * @return array
     */
    public static function getStatuses()
    {
        return [
            self::PENDING_STATUS,
            self::IN_PROGRES_STATUS,
            self::COMPLETED_STATUS
        ];
    }

    public static function cloneFromWorkflow(Workflow $workflow, string $subject_api, string $subject_type, string $subject_uid)
    {
        $action = StepsCloneActionsManager::resolve('workflow');
        return $action->handle(
            new SubjectStepData($subject_api, $subject_type, $subject_uid),
            $workflow->uid
        );
    }

    /**
     * Restart steps from given workflow and subject
     *
     * @param Workflow $workflow
     * @param string $subject_api
     * @param string $subject_type
     * @param string $subject_uid
     * @return void
     */
    public static function restartFromWorkflow(Workflow $workflow, string $subject_api, string $subject_type, string $subject_uid)
    {
        if(!$workflow->clone_steps){
            return [];
        };

        self::where('subject_api', $subject_api)
        ->where('subject_type', $subject_type)
        ->where('subject_uid', $subject_uid)
        ->where('order', ">", 1 )
        ->update([
            'status' => self::PENDING_STATUS,
            'sub_status' => null
        ]);

        $subject_step = self::where('subject_api', $subject_api)
        ->where('subject_type', $subject_type)
        ->where('subject_uid', $subject_uid)
        ->where('order', 1 )
        ->first();

        if($subject_step){
            $subject_step->update([
                'status' => self::IN_PROGRES_STATUS,
                'sub_status' => null,
                'started_at' => Carbon::log()
            ]);
        }   

        return [
            'prev_step_name' =>  null,
            'prev_step_status' =>  null,
            'prev_step_sub_status' =>  null,
            'prev_subject_step_uid' => null,
            'prev_step_uid' => null,
            'current_step_name' => $subject_step ? $subject_step->name : null,
            'current_step_status' => $subject_step ? $subject_step->status : null,
            'current_step_sub_status' => $subject_step ? $subject_step->sub_status : null,
            'current_subject_step_uid' => $subject_step ? $subject_step->uid : null,
            'current_step_uid' => $subject_step ? $subject_step->step->uid : null,
            'progress' => 0
        ];
    }

    /**
     * Complete the step
     *
     * @return void
     */
    public function complete($sub_status = false, $user = null, $note = null, $newSubjectStepUid = null) : ResponseAction
    {
        $action = StepsCompleteActionsManager::resolveByNextStep($newSubjectStepUid);

        return $action->handle($this, $sub_status, $user, $note, $newSubjectStepUid);
    }
    
    /**
     * Complete the step
     *
     * @return void
     */
    public function rollback($sub_status = false, $user = null, $note = null)
    {
        $action = StepsRollbackActionsManager::resolve('rollback-prev');
        
        return $action->handle($this, $sub_status, $user, $note);
    }

    public function calculateTimeWorked()
    {
        $hours = Carbon::parse($this->started_at)->floatDiffInHours(Carbon::now());

        return $this->time_worked ? $this->time_worked + $hours : $hours;
    }


    public static function calculateStepsProgress($subject_api, $subject_type, $subject_uid)
    {
        $completed = self::where('subject_api', $subject_api)
        ->where('subject_type', $subject_type)
        ->where('subject_uid', $subject_uid)
        ->where('status', self::COMPLETED_STATUS)
        ->count();

        $total = self::where('subject_api', $subject_api)
        ->where('subject_type', $subject_type)
        ->where('subject_uid', $subject_uid)
        ->count();

        return round((($completed / $total) * 100), 2);
    }

    public static function importFromDeal($deal)
    {
        $subject_steps = self::where('subject_api', 'deals')
        ->where('subject_type', 'Deal')
        ->where('subject_uid', $deal->uid)
        ->get();

        if(!$subject_steps->isEmpty()){
            return $subject_steps;
        }

        $current_step = null;
        $prev_step = null;

        foreach ($deal->old_steps as $step) {

            $parentStep = Step::whereUid($step->step_uid)->first();

            if(!$parentStep) continue;

            $subject_step = self::create([
                'name' => $step->name,
                'order' => $step->order,
                'estimated_time' => $step->estimated_time,
                'status' => $step->status,
                'sub_status' =>  $step->sub_status,
                'public' => $step->public,
                'customer_edit' => $step->customer_edit,
                'hidden_from_customer' => $step->hidden_from_customer,
                'subject_api' => 'deals',
                'subject_type' => 'Deal',
                'subject_uid' => $deal->uid,
                'step_id' => $parentStep->id
            ]);

            if($step->status ==  self::IN_PROGRES_STATUS){
                $current_step = $subject_step;
            }

        }

        if($current_step){
            self::where('uid', '!=', $current_step->uid)
            ->where('order', '<', $current_step->order)
            ->where('status', '!=', self::COMPLETED_STATUS)
            ->where([
                'subject_api' => 'deals',
                'subject_type' => 'Deal',
                'subject_uid' => $deal->uid,
            ])->update([
                'status' => self::COMPLETED_STATUS
            ]);

            self::where('uid', '!=', $current_step->uid)
            ->where('order', '>', $current_step->order)
            ->where('status', '!=', self::PENDING_STATUS)
            ->where([
                'subject_api' => 'deals',
                'subject_type' => 'Deal',
                'subject_uid' => $deal->uid,
            ])->update([
                'status' => self::PENDING_STATUS
            ]);


            $prev_step = self::where([
                'subject_api' => 'deals',
                'subject_type' => 'Deal',
                'subject_uid' => $deal->uid,
                'order' => $current_step->order - 1
            ])->with('step')
            ->first();

            $current_step->load('step');

            return [
                'prev_step_name' => $prev_step ? $prev_step->name : null,
                'prev_step_status' => $prev_step ? $prev_step->status : null,
                'prev_step_sub_status' => $prev_step ? $prev_step->sub_status : null,
                'prev_subject_step_uid' => $prev_step ?  $prev_step->uid : null,
                'prev_step_uid' => $prev_step ? $prev_step->step->uid : null,
                'current_step_name' => $current_step->name,
                'current_step_status' => $current_step->status,
                'current_step_sub_status' => $current_step->sub_status,
                'current_subject_step_uid' => $current_step->uid,
                'current_step_uid' => $current_step->step->uid,
                'progress' => self::calculateStepsProgress('deals', 'Deal', $deal->uid)
            ];
        }else{
            $prev_step = self::where('status', self::COMPLETED_STATUS)
            ->where([
                'subject_api' => 'deals',
                'subject_type' => 'Deal',
                'subject_uid' => $deal->uid,
            ])->orderBy('order', 'desc')
            ->with('step')
            ->first();

            if(!$prev_step){
                $current_step = self::where([
                    'subject_api' => 'deals',
                    'subject_type' => 'Deal',
                    'subject_uid' => $deal->uid,
                    'order' => 1
                ])->with('step')
                ->first();

                $current_step->update(['status' => self::IN_PROGRES_STATUS]);

                return [
                    'prev_step_name' =>  null,
                    'prev_step_status' =>  null,
                    'prev_step_sub_status' =>  null,
                    'prev_subject_step_uid' => null,
                    'prev_step_uid' => null,
                    'current_step_name' => $current_step->name,
                    'current_step_status' => $current_step->status,
                    'current_step_sub_status' => $current_step->sub_status,
                    'current_subject_step_uid' => $current_step->uid,
                    'current_step_uid' => $current_step->step->uid,
                    'progress' => self::calculateStepsProgress('deals', 'Deal', $deal->uid)
                ];
            }else{
                self::where('uid', '!=', $prev_step->uid)
                ->where('order', '<', $prev_step->order)
                ->where('status', '!=', self::COMPLETED_STATUS)
                ->where([
                    'subject_api' => 'deals',
                    'subject_type' => 'Deal',
                    'subject_uid' => $deal->uid,
                ])->update([
                    'status' => self::COMPLETED_STATUS
                ]);

                $current_step = self::where([
                    'subject_api' => 'deals',
                    'subject_type' => 'Deal',
                    'subject_uid' => $deal->uid,
                    'order' => $prev_step->order + 1
                ])->with('step')
                ->first();

                

                if($current_step) {
                    self::where('uid', '!=', $current_step->uid)
                    ->where('order', '>', $current_step->order)
                    ->where('status', '!=', self::PENDING_STATUS)
                    ->where([
                        'subject_api' => 'deals',
                        'subject_type' => 'Deal',
                        'subject_uid' => $deal->uid,
                    ])->update([
                        'status' => self::PENDING_STATUS
                    ]);
                    
                    $current_step->update(['status' => self::IN_PROGRES_STATUS]);
                }

                return [
                    'prev_step_name' => $prev_step->name,
                    'prev_step_status' => $prev_step->status,
                    'prev_step_sub_status' => $prev_step->sub_status,
                    'prev_subject_step_uid' =>  $prev_step->uid,
                    'prev_step_uid' =>  $prev_step->step->uid,
                    'current_step_name' => $current_step ? $current_step->name : null,
                    'current_step_status' => $current_step ? $current_step->status : null,
                    'current_step_sub_status' => $current_step ? $current_step->sub_status : null,
                    'current_subject_step_uid' => $current_step ? $current_step->uid : null,
                    'current_step_uid' => $current_step ? $current_step->step->uid : null,
                    'progress' => self::calculateStepsProgress('deals', 'Deal', $deal->uid)
                ];
            }
        }
    }

}
