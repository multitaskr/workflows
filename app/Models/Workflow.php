<?php

namespace App\Models;

use App\Models\Traits\Uidable;
use App\Models\Traits\Filterable;
use App\Filters\Shared\SortFilter;
use App\Filters\Shared\ExtendFilter;
use App\Filters\Shared\FieldsFilter;
use App\Filters\Workflow\NamesFilter;
use App\Filters\Workflow\SearchFilter;
use Illuminate\Database\Eloquent\Model;
use App\Filters\Workflow\VerticalFilter;
use Cviebrock\EloquentSluggable\Sluggable;
use App\Filters\Workflow\NoVerticalsFilter;
use App\Filters\Workflow\TagsFilter;
use App\Filters\Workflow\UseFilter;
use App\Models\Traits\Tagable;
use Illuminate\Database\Eloquent\SoftDeletes;

class Workflow extends Model
{
    use Uidable, SoftDeletes, Filterable, Sluggable, Tagable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [
        'uid',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'created_at' => 'datetime:Y-m-d\TH:i:sP',
        'updated_at' => 'datetime:Y-m-d\TH:i:sP',
        'deleted_at' => 'datetime:Y-m-d\TH:i:sP',
        'public' => 'boolean',
        'protected' => 'boolean',
        'requires_financing' => 'boolean',
        'clone_steps' => 'boolean'
    ];

    /**
     * Return the filters that should be use by default
     *
     * @return array
     */
    public function filters(): array
    {
        return  [
            'fields' => new FieldsFilter('workflows'),
            'sort' => new SortFilter('workflows'),
            'search' => SearchFilter::class,
            'vertical' => VerticalFilter::class,
            'extend' => new ExtendFilter(['steps', 'steps.sections', 'tags']),
            'no-verticals' => NoVerticalsFilter::class,
            'use' => UseFilter::class,
            'tags' => TagsFilter::class,
            'names' => NamesFilter::class
        ];
    }

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }

    /**
     * Get the steps of the workflow
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function steps()
    {
        return $this->hasMany(Step::class)->orderBy('order', 'ASC');
    }

    /**
     * Get all of the sections for the workflow.
     */
    public function sections()
    {
        return $this->hasManyThrough(Section::class, Step::class);
    }

    public function tags()
    {
        return $this->morphToMany(Tag::class, 'taggable');
    }
}
