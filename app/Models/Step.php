<?php

namespace App\Models;

use App\Models\Traits\Uidable;
use App\Models\Traits\Filterable;
use App\Filters\Shared\SortFilter;
use App\Filters\Step\PublicFilter;
use App\Filters\Step\SearchFilter;
use App\Filters\Shared\ExtendFilter;
use App\Filters\Shared\FieldsFilter;
use App\Filters\Step\WorkflowFilter;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Step extends Model
{
    use Uidable, SoftDeletes, Filterable;

     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];


    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'created_at' => 'datetime:Y-m-d\TH:i:sP',
        'updated_at' => 'datetime:Y-m-d\TH:i:sP',
        'deleted_at' => 'datetime:Y-m-d\TH:i:sP',
        'value' => 'array',
        'hidden_from_customer' => 'boolean'
    ];

    /**
     * Return the filters that should be use by default
     *
     * @return array
     */
    public function filters(): array
    { 
        return  [
            'fields' => new FieldsFilter('workflows'),
            'sort' => new SortFilter('workflows'),
            'search' => SearchFilter::class,
            'workflow' => WorkflowFilter::class,
            'public' => PublicFilter::class,
            'extend' => new ExtendFilter(['workflow', 'sections']),
        ];
    }

    /**
     * Get the workflow of the step
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function workflow()
    {
        return $this->belongsTo(Workflow::class);
    }

    /**
     * Get the logics of the step
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function logics()
    {
        return $this->hasMany(StepLogic::class);
    }

    /**
     * Get the sections of the step
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function sections()
    {
        return $this->hasMany(Section::class)->orderBy('order', 'ASC');
    }

    /**
     * Get the inputs of the step
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasManyThrough
     */
    public function inputs()
    {
        return $this->hasManyThrough(Input::class, Section::class);
    }

    /**
     * Get the step of given workflow
     *
     * @param  $query
     * @param  int $workflowId
     * @return void
     */
    public function scopeByWorkflow($query, int $workflowId)
    {
        return $query->where('workflow_id', $workflowId);
    }

    /**
     * Get the public steps 
     *
     * @param  $query
     * @return void
     */
    public function scopePublic($query)
    {
        return $query->where('public', true);
    }
}
