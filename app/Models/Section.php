<?php

namespace App\Models;

use App\Models\Traits\Uidable;
use App\Models\Traits\Filterable;
use App\Filters\Shared\SortFilter;
use App\Filters\Shared\ExtendFilter;
use App\Filters\Shared\FieldsFilter;
use App\Filters\Section\SearchFilter;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Section extends Model
{
    use Uidable, SoftDeletes, Filterable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'description', 'hint', 'order', 'step_id'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'created_at' => 'datetime:Y-m-d\TH:i:sP',
        'updated_at' => 'datetime:Y-m-d\TH:i:sP',
        'deleted_at' => 'datetime:Y-m-d\TH:i:sP',
    ];

    /**
     * Return the filters that should be use by default
     *
     * @return array
     */
    public function filters(): array
    { 
        return  [
            'fields' => new FieldsFilter('sections'),
            'sort' => new SortFilter('sections'),
            'search' => SearchFilter::class,
            'extend' => new ExtendFilter(['step', 'inputs']),
        ];
    }

    /**
     * Get the workflow of the step
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function step()
    {
        return $this->belongsTo(Step::class);
    }

    /**
     * Get the inputs of the step
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function inputs()
    {
        return $this->hasMany(Input::class);
    }

    /**
     * Get all of the values for the setion.
     */
    public function values()
    {
        return $this->hasManyThrough(InputValue::class, Input::class);
    }

    /**
     * Get the sections of given step
     *
     * @param  $query
     * @param  int $workflowId
     * @return void
     */
    public function scopeByStep($query, int $stepId)
    {
        return $query->where('step_id', $stepId);
    }

   /**
     *  Get input publuc sections
     *
     * @param  $query
     * @return void
     */
    public function scopePrivate($query, $val)
    {
        return $query->where('private', $val);
    }
}
