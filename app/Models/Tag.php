<?php

namespace App\Models;

use App\Models\Traits\Uidable;
use App\Filters\Tag\SearchFilter;
use App\Models\Traits\Filterable;
use App\Filters\Shared\SortFilter;
use App\Filters\Shared\FieldsFilter;
use App\Filters\Tag\TagsFilter;
use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\SoftDeletes;

class Tag extends Model
{
    use Uidable, SoftDeletes, Sluggable, Filterable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'created_at' => 'datetime:Y-m-d\TH:i:sP',
        'updated_at' => 'datetime:Y-m-d\TH:i:sP',
        'deleted_at' => 'datetime:Y-m-d\TH:i:sP',
    ];

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }

    /**
     * Get the workflow's tag
     * @return \Illuminate\Database\Eloquent\Relations\MorphedByMany
     */

    public function workflows()
    {
        return $this->morphedByMany(Workflow::class, 'taggable');
    }

    /**
     * Return the filters that should be use by default
     *
     * @return array
     */
    public function filters(): array
    {
        return  [
            'fields'    => new FieldsFilter('workflows'),
            'sort'      => new SortFilter('workflows'),
            'search'    => SearchFilter::class,
            'find'      => TagsFilter::class,
        ];
    }
}
