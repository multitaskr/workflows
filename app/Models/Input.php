<?php

namespace App\Models;

use App\Models\Traits\Uidable;
use App\Models\Traits\Filterable;
use App\Filters\Shared\SortFilter;
use App\Filters\Input\SearchFilter;
use App\Filters\Shared\ExtendFilter;
use App\Filters\Shared\FieldsFilter;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Input extends Model
{
    use Uidable, SoftDeletes, Filterable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'data', 'validations', 'order', 'section_id'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'created_at' => 'datetime:Y-m-d\TH:i:sP',
        'updated_at' => 'datetime:Y-m-d\TH:i:sP',
        'deleted_at' => 'datetime:Y-m-d\TH:i:sP',
        'data' => 'array'
    ];

    /**
     * Return the filters that should be use by default
     *
     * @return array
     */
    public function filters(): array
    { 
        return  [
            'fields' => new FieldsFilter('inputs'),
            'sort' => new SortFilter('inputs'),
            'search' => SearchFilter::class,
            'extend' => new ExtendFilter(['section']),
        ];
    }

    /**
     * Get the input section
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function section()
    {
        return $this->belongsTo(Section::class);
    }

    /**
     * Get the inputs of the given section
     *
     * @param  $query
     * @param  int $sectionId
     * @return void
     */
    public function scopeBySection($query, int $sectionId)
    {
        return $query->where('section_id', $sectionId);
    }
}
