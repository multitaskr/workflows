<?php
namespace App\Models\Traits;

use App\Models\Tag;
use Illuminate\Support\Str;

trait Tagable{
    
    /**
     * Relation
     *
     * @return void
     */
    public function tags()
    {
        return$this->morphToMany(Tag::class, 'taggable');
    }

    /**
     * Attach tags
     *
     * @param array $tags
     * @return static
     */
    public function syncTags(array $tags)
    {
        if(empty($tags)) 
            return $this;

        $tags = collect(self::findOrCreateMany($tags));

        $this->tags()->sync($tags->pluck('id')->toArray());

        return $this;
    }

    /**
     * Find or create many tags
     *
     * @param array $tags
     * @return void
     */
    public static function findOrCreateMany(array $tags)
    {
        $tags = collect($tags)->map(function ($tag) {
            
            return Tag::firstOrCreate([
                'slug' => Str::slug($tag)
            ], [
                "name" => $tag,
            ]);
        });

        return $tags;
    }
}