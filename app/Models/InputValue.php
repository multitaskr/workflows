<?php

namespace App\Models;

use Auth;
use Utilities;
use Carbon\Carbon;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use App\Models\Traits\Uidable;
use App\Filters\Shared\RawFilter;
use App\Models\Traits\Filterable;
use App\Filters\Shared\SortFilter;
use Illuminate\Support\Facades\Log;
use App\Filters\Shared\FieldsFilter;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use App\Filters\InputValue\SubjectApiFilter;
use App\Filters\InputValue\SubjectUidFilter;
use App\Filters\InputValue\SubjectTypeFilter;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Services\Multitaskr\Facades\FileService;
use App\Webhooks\WebhooksManager;

class InputValue extends Model
{
    use Uidable, SoftDeletes, Filterable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'value', 'subject_uid', 'subject_type', 'subject_api', 'input_id', 'created_at'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'created_at' => 'datetime:Y-m-d\TH:i:sP',
        'updated_at' => 'datetime:Y-m-d\TH:i:sP',
        'deleted_at' => 'datetime:Y-m-d\TH:i:sP',
        'value' => 'array'
    ];


    protected static $jsonValues = [
        'maps',
        'checkbox'
    ];

    /**
     * Return the filters that should be use by default
     *
     * @return array
     */
    public function filters(): array
    { 
        return  [
            'fields' => new FieldsFilter('input_values'),
            'sort' => new SortFilter('input_values'),
            'subject_uid' => SubjectUidFilter::class,
            'subject_type' => SubjectTypeFilter::class,
            'subject_api' => SubjectApiFilter::class,
            'raw' => RawFilter::class,
        ];
    }

    protected const LOCAL_URL = '/workflows/files/';

    /**
     * Get the input of the value
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function input()
    {
        return $this->belongsTo(Input::class);
    }

    /**
     * Get values by inputs
     *
     * @return void
     */
    public function scopeWhereInputs($query, $inputs, $uid, $type, $api)
    {
        return $query->whereIn('input_id', $inputs)
        ->where('subject_uid', $uid)
        ->where('subject_type', $type)
        ->where('subject_api', $api);
    }

    /**
     * Get values by input tags
     *
     * @param Type $var
     * @return void
     */
    public function scopeWhereTags($query, $tags, $uid, $type, $api)
    {
        $uid = explode(',', $uid);
        $type = explode(',', $type);
        $api = explode(',', $api);

        return $query->whereIn('subject_uid', $uid)
        ->whereIn('subject_type', $type)
        ->whereIn('subject_api', $api)
        ->whereHas('input', function($query) use($tags){
            $query->whereIn('tag', $tags);
        });
    }

    /**
     * Get values by input tags
     *
     * @param Type $var
     * @return void
     */
    public function scopeSubject($query, $subject_uid, $subject_type, $subject_api)
    {
        return $query->where('subject_uid', $subject_uid)
        ->where('subject_type', $subject_type)
        ->where('subject_api', $subject_api);
    }

    static public function createValues(array $request)
    {
        $values = [];

        $created = Carbon::now();
        
        $workflow = null;

        if (Arr::exists($request, 'values')) {
            foreach ( $request['values'] as $inputUid => $value) {
                if (!is_null($value)) {
                    $input = Input::whereUid($inputUid)->first();

                    if(!$workflow){
                        $workflow = $input->section->step->workflow;
                    }
                   
                    if(in_array($input->data['type'], ['imagepicker', 'file', 'pdf'])){
    
                        if(!is_array($value)) $value = [$value];
                        
                        $uploaded_files = [];
        
                        foreach($value as $file) {
                            $filename = Str::afterLast(Storage::disk('do')
                            ->putFile('private/workflows', $file, 'private'), '/');

                            $file_service = FileService::store([
                                'url' => ['/private/workflows/' . $filename],
                                'subject_uid' => $request['subject_uid'],
                                'subject_type' =>  $request['subject_type'],
                                'subject_api' => $request['subject_api'],
                                'permissions' => isset($request['user_uid']) ? [$request['user_uid']] : []
                            ]);

                            $list = collect($file_service);

                            if ($list->count()) {
                                $uploaded_files[] =  [
                                    'name' => $file->getClientOriginalName(), 
                                    'url' => $list->first()->url
                                ];
                            }
                        }
                        
                        $value = $uploaded_files;
                    }
                    
                    $values[] = $model =  self::create([
                        'value' =>  in_array($input->data['type'], self::$jsonValues)  ? json_decode($value) : $value,
                        'subject_uid' => $request['subject_uid'],
                        'subject_type' => $request['subject_type'],
                        'subject_api' => $request['subject_api'],
                        'input_id' => $input->id,
                        'created_at' => $created
                    ]);

                }
            }
        }

        if(!empty($request['values']) && $workflow){
            WebhooksManager::queueWebhooks(
                $workflow, 
                $request['subject_api'], 
                $request['subject_type'], 
                $request['subject_uid']
            );
        }

        return collect($values);
    }

}
