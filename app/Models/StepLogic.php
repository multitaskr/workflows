<?php

namespace App\Models;

use App\Models\Traits\Uidable;
use App\Filters\Shared\SortFilter;
use App\Filters\Shared\ExtendFilter;
use App\Filters\Shared\FieldsFilter;
use App\Models\Traits\Filterable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class StepLogic extends Model
{
    use Uidable, SoftDeletes, Filterable;


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'logic_data', 'action', 'step_id'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'created_at' => 'datetime:Y-m-d\TH:i:sP',
        'updated_at' => 'datetime:Y-m-d\TH:i:sP',
        'deleted_at' => 'datetime:Y-m-d\TH:i:sP',
        'logic_data' => 'array',
        'action' => 'array'
    ];

    /**
     * Return the filters that should be use by default
     *
     * @return array
     */
    public function filters(): array
    { 
        return  [
            'fields' => new FieldsFilter('step_logics'),
            'sort' => new SortFilter('step_logics'),
            'extend' => new ExtendFilter(['step']),
        ];
    }

    /**
     * Get the workflow of the step
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function step()
    {
        return $this->belongsTo(Step::class);
    }
}   
