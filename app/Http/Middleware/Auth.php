<?php

namespace App\Http\Middleware;

use Closure;
use App\Models\User;
use Firebase\JWT\JWT;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\File;
use Auth as FacadeAuth;

class Auth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $roles = null)
    {
        if(App::environment('testing')){
            return $next($request);
        }

        $publicKey = $this->getPublicKey();

        if(!$publicKey){
            return response()->json(['message' => 'Unauthorized'], 403);
        }

        $bearerToken = $this->getBearerToken($request);

        if(!$bearerToken){
            return response()->json(['message' => 'Unauthorized'], 403);
        }

        $token = JWT::decode($bearerToken, $publicKey, ['RS256']);

        if($token->data->deleted_at){
            return response()->json(['message' => 'Unauthorized'], 401);
        }
        
        if($roles) {
            if(!$this->validateRoles($roles, $token->data)){
                return response()->json(['message' => 'Unauthorized'], 401);
            }
        }

        FacadeAuth::login(new User((array) $token->data));

        return $next($request);
    }

    /**
     * Get the public key
     *
     * @return File
     */
    public function getPublicKey()
    {
        if(!file_exists(storage_path('oauth-public.key'))){
            return null;
        }

        return File::get(storage_path('oauth-public.key'));
    }

    /**
     * Get the bearer token from the request headers.
     *
     * @return string|null
    */
    public function getBearerToken($request)
    {
        $header = $request->header('Authorization', '');

        if ($header && Str::startsWith($header, 'Bearer ')) {
            return Str::substr($header, 7);
        }

        $cookie = $request->cookie('auth__multitaskr_local');
        $cookie = str_replace('Bearer%20', 'Bearer ', $cookie);
        
        if ($cookie && Str::startsWith($cookie, 'Bearer')) {
            return Str::substr($cookie, 7);
        }
    }

    /**
     * Validate the roles
     *
     * @param string $roles
     * @param object $data
     * @return boolean
     */
    public function validateRoles($roles, $data)
    {
        $roles = explode('|', $roles);
        
        if(empty(array_intersect($data->roles, $roles))){
            return false;
        }

        return true;
    }
}
