<?php

namespace App\Http\Middleware;

use Auth;
use Closure;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\App;

class CustomerStep
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(App::environment('testing')){
            return $next($request);
        }

        $roles = Auth::user()->roles;

        if(Arr::first($roles) == 'Customer'){
            if(($request->step && !$request->step->customer_edit) 
            || ($request->section && !$request->section->step->customer_edit)){
                return response()->json(['message' => 'Unauthorized'], 403);
            }
        }
        
        return $next($request);
    }
}
