<?php

namespace App\Http\Middleware;

use App\Models\TimeLog;
use Closure;

class NoEndedTimers
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $notEndedTimer = TimeLog::where('subject_step_id', $request->subject_step->id)
        ->whereNull('ended_at')
        ->first();

        if($notEndedTimer){
            return response()->json(['message' => 'Unauthorized'], 403);
        }

        return $next($request);
    }
}
