<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;

class PublicStep
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(($request->step && !$request->step->public) 
        || ($request->section && !$request->section->step->public)
        || ($request->step && $request->step->workflow && $request->step->workflow->protected)) {
            if (!Auth::check()) {
                return response()->json(['message' => 'Unauthorized'], 403);
            } else {
                $roles = Auth::user()->roles;

                if (Arr::first($roles) == 'Customer') {
                    return response()->json(['message' => 'Unauthorized'], 403);
                }
            }
        }

        return $next($request);
    }
}
