<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\App;

class ValidateNotRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $roles)
    {
        if(App::environment('testing')){
            return $next($request);
        }
        
        $roles = explode('|', $roles);

        if(!empty(array_intersect(auth()->user()->roles, $roles))){
            return response()->json(['message' => 'Unauthorized'], 401);
        }
        
        return $next($request);
    }
}
