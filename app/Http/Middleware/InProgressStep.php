<?php

namespace App\Http\Middleware;

use App\Models\SubjectStep;
use Closure;

class InProgressStep
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if($request->subject_step->status !== SubjectStep::IN_PROGRES_STATUS){
            return response()->json(['message' => 'Unauthorized'], 403);
        }

        return $next($request);
    }
}
