<?php

namespace App\Http\Middleware;

use Closure;
use App\Models\User;
use Firebase\JWT\JWT;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Auth as FacadeAuth;

class RetriveUser
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if(App::environment('testing')){
            return $next($request);
        }

        $publicKey = $this->getPublicKey();


        $bearerToken = $this->getBearerToken($request);

        if(!$bearerToken || !$publicKey){
            return  $next($request);
        }

        $token = JWT::decode($bearerToken, $publicKey, ['RS256']);

        if($token->data->deleted_at){
            return response()->json(['message' => 'Unauthorized'], 403);
        }
        

        FacadeAuth::login(new User((array) $token->data));

        return $next($request);
    }

     /**
     * Get the public key
     *
     * @return File
     */
    public function getPublicKey()
    {
        if(!file_exists(storage_path('oauth-public.key'))){
            return null;
        }

        return File::get(storage_path('oauth-public.key'));
    }

    /**
     * Get the bearer token from the request headers.
     *
     * @return string|null
    */
    public function getBearerToken($request)
    {
        $header = $request->header('Authorization', '');
     
        if (Str::startsWith($header, 'Bearer ')) {
            return Str::substr($header, 7);
        }
    }
}
