<?php

namespace App\Http\Requests;

use App\Models\Step;
use App\Models\Workflow;
use Illuminate\Support\Arr;
use Illuminate\Foundation\Http\FormRequest;
use phpDocumentor\Reflection\Types\Nullable;

class StepRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'name' => 'required|string|max:255',
            'description' => 'nullable|string|max:1000',
            'color' => 'nullable|string|max:255',
            'public' => 'required|boolean', 
            'estimated_time' => 'sometimes|numeric',
            'customer_edit' => 'nullable|sometimes|boolean',
            'preserve_input_values' => 'nullable|sometimes|boolean',
            'hidden_from_customer' => 'nullable|sometimes|boolean',
            'workflow' => 'required|exists:workflows,uid'
        ];

        if(!$this->isCreateRequest()){
            $rules['order'] = 'required|numeric';
        }

        return $rules;
    }

    /**
     * Check if the request is for create method
     *
     * @return boolean
     */
    public function isCreateRequest()
    {
        return $this->step ? false : true;
    }

    /**
     * Get the validated data from the request.
     *
     * @return array
     */
    public function validated()
    {
        $validated =  $this->validator->validated();

        $workflow_id = $this->getWorkflowId($validated['workflow']);

        Arr::set($validated, 'workflow_id', $workflow_id);

        if($this->isCreateRequest()){
            Arr::set($validated, 'order', $this->getOrder($workflow_id));
        }
        
        return $validated;
    }

    /**
     * Get the workflow id by uid
     *
     * @param string $workflowUid
     * @return int
     */
    public function getWorkflowId($workflowUid)
    {
        $workflow = Workflow::whereUid($workflowUid)->first();

        return $workflow->id;
    }

    /**
     * Get the order by workflow id
     *
     * @param string $workflowUid
     * @return int
     */
    public function getOrder($workflowId)
    {
        $step = Step::byWorkflow($workflowId)->orderBy('order', 'desc')->first();

        if(!$step){
            return 1;
        }

        return $step->order + 1;
    }
}
