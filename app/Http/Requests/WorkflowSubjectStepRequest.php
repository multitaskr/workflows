<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class WorkflowSubjectStepRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'subject_api' => 'required|string|max:255',
            'subject_type' => 'required|string|max:255',
            'subject_uid' => 'required|string|max:255',
            'sub_status' => 'sometimes|nullable',
            'note' => 'sometimes|nullable',
        ];
    }
}
