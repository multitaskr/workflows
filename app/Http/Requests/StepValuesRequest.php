<?php

namespace App\Http\Requests;

use App\Models\InputValue;
use App\Models\Step;
use Illuminate\Foundation\Http\FormRequest;

class StepValuesRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [];
        
        foreach ($this->step->inputs as $input) {
	        if(!$input->validations) {
                $rules['values.' . $input->uid] = 'nullable';
                continue;
            }

            $rules['values.' . $input->uid] = $input->validations;
        }

        $rules = array_merge($rules, [
            'subject_uid' => 'required|string|max:36',
            'subject_type' => 'required|string',
            'subject_api' => 'required|string',
            'user_uid' => 'sometimes|string'
        ]);
    
        return $rules;
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        $attributes = [];

        foreach ($this->step->inputs as $input) {
            $attributes['values.' . $input->uid] = $input->title;
        }
        
        return $attributes;
    }

    /**
     * Get the validated data from the request.
     *
     * @return array
     */
    public function validated()
    {
        $validated =  $this->validator->validated();

        if(!$this->step->preserve_input_values){
            InputValue::whereInputs(
                $this->step->inputs->pluck('id')->toArray(), 
                $validated['subject_uid'], 
                $validated['subject_type'], 
                $validated['subject_api'] 
            )->delete();
        }

        return $validated;
    }
}
