<?php

namespace App\Http\Requests;

use App\Models\Step;
use Illuminate\Support\Arr;
use Illuminate\Foundation\Http\FormRequest;

class StepLogicRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'logic_data' => 'required|json',
            'action' => 'required|json',
            'step' => 'required|exists:steps,uid'
        ];
    }

    /**
     * Get the validated data from the request.
     *
     * @return array
     */
    public function validated()
    {
        $validated =  $this->validator->validated();

        $step_id = $this->getStepId($validated['step']);
       
        Arr::set($validated, 'step_id', $step_id);
        
        Arr::set($validated, 'logic_data', json_decode($validated['logic_data']));

        return $validated;
    }

    /**
     * Get the workflow id by uid
     *
     * @param string $workflowUid
     * @return int
     */
    public function getStepId($stepUid)
    {
        $workflow = Step::whereUid($stepUid)->first();

        return $workflow->id;
    }
}
