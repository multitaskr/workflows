<?php

namespace App\Http\Requests;

use App\Models\Step;
use App\Models\Section;
use Illuminate\Support\Arr;
use Illuminate\Foundation\Http\FormRequest;

class SectionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'title' => 'required|string|max:255',
            'description' => 'nullable|string|max:1000',
            'hint' => 'nullable|string|max:255',
            'step' => 'required|exists:steps,uid'
        ];

        if(!$this->isCreateRequest()){
            $rules['order'] = 'required|numeric';
        }

        return $rules;
    }

    /**
     * Check if the request is for create method
     *
     * @return boolean
     */
    public function isCreateRequest()
    {
        return $this->section ? false : true;
    }

    /**
     * Get the validated data from the request.
     *
     * @return array
     */
    public function validated()
    {
        $validated =  $this->validator->validated();

        $step_id = $this->getStepId($validated['step']);

        Arr::set($validated, 'step_id', $step_id);

        if($this->isCreateRequest()){
            Arr::set($validated, 'order', $this->getOrder($step_id));
        }
        
        return $validated;
    }

    /**
     * Get the workflow id by uid
     *
     * @param string $workflowUid
     * @return int
     */
    public function getStepId($workflowUid)
    {
        $workflow = Step::whereUid($workflowUid)->first();

        return $workflow->id;
    }

    /**
     * Get the order by workflow id
     *
     * @param string $workflowUid
     * @return int
     */
    public function getOrder($workflowId)
    {
        $section = Section::byStep($workflowId)->orderBy('order', 'desc')->first();

        if(!$section){
            return 1;
        }

        return $section->order + 1;
    }
}
