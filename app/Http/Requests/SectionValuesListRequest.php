<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SectionValuesListRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'subject_uid' => 'required|string|max:36',
            'subject_type' => 'required|string',
            'subject_api' => 'required|string'
        ];
    }
}
