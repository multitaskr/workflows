<?php

namespace App\Http\Requests;

use App\Models\Input;
use App\Models\Section;
use Illuminate\Support\Arr;
use Illuminate\Foundation\Http\FormRequest;

class InputRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'title' => 'required|string|max:255',
            'data' => 'required|json',
            'validations' => 'nullable|string:max:1000',
            'section' => 'required|exists:sections,uid'
        ];

        if(!$this->isCreateRequest()){
            $rules['order'] = 'required|numeric';
        }

        return $rules;
    }

    /**
     * Check if the request is for create method
     *
     * @return boolean
     */
    public function isCreateRequest()
    {
        return $this->input ? false : true;
    }

     /**
     * Get the validated data from the request.
     *
     * @return array
     */
    public function validated()
    {
        $validated =  $this->validator->validated();

        $section_id = $this->getSectionId($validated['section']);

        Arr::set($validated, 'data', json_decode($validated['data'], true));

        Arr::set($validated, 'section_id', $section_id);

        if($this->isCreateRequest()){
            Arr::set($validated, 'order', $this->getOrder($section_id));
        }
        
        return $validated;
    }

    /**
     * Get the section id by uid
     *
     * @param string $workflowUid
     * @return int
     */
    public function getSectionId($sectionUid)
    {
        $section = Section::whereUid($sectionUid)->first();

        return $section->id;
    }

    /**
     * Get the order by workflow id
     *
     * @param string $workflowUid
     * @return int
     */
    public function getOrder($sectionId)
    {
        $order = Input::bySection($sectionId)->max('order');

        return $order ?  $order + 1 : 1;
    }
}
