<?php

namespace App\Http\Requests;

use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Storage;
use Illuminate\Foundation\Http\FormRequest;

class WorkflowRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules =  [
            'name' => 'required|string|max:255',
            'description' => 'nullable|string|max:1000',
            'vertical' => 'sometimes|string|max:255',
            'public' => 'required|boolean',
            'protected' => 'required|boolean',
            'use' => 'sometimes|nullable|string|max:255',
            'tags' => 'sometimes|array',
            'tags.*' => 'sometimes|string|max:255',
            'requires_financing' => 'required|boolean',
            'financing_text' => 'sometimes|nullable|string|max:10000',
            'clone_steps' => 'required|boolean',
        ];

        if ($this->isCreateRequest()) {
            $rules['image'] = 'required_if:public,1|file|max:25000';
            $rules['success_image'] = 'required_if:public,1|file|max:25000';
            $rules['mobile_icon'] = 'required_if:public,1|file|max:25000';
        } else {
            if (!$this->workflow->image) $rules['image'] = 'required_if:public,1|file|max:25000';
            if (!$this->workflow->success_image) $rules['success_image'] = 'required_if:public,1|file|max:25000';
            if (!$this->workflow->mobile_icon) $rules['mobile_icon'] = 'required_if:public,1|file|max:25000';
        }

        return $rules;
    }

    /**
     * Check if the request is for create method
     *
     * @return boolean
     */
    public function isCreateRequest()
    {
        return $this->workflow ? false : true;
    }

    /**
     * Get the validated data from the request.
     *
     * @return array
     */
    public function validated()
    {
        $validated =  $this->validator->validated();

        if ($this->file('image')) {
            $image_url = Storage::disk('do')->url(
                Storage::disk('do')
                    ->putFile('workflows/workflows', $this->file('image'), 'public')
            );

            Arr::set($validated, 'image', $image_url);
        }

        if ($this->file('success_image')) {
            $image_url = Storage::disk('do')->url(
                Storage::disk('do')
                    ->putFile('workflows/workflows', $this->file('success_image'), 'public')
            );

            Arr::set($validated, 'success_image', $image_url);
        }

        if ($this->file('mobile_icon')) {
            $image_url = Storage::disk('do')->url(
                Storage::disk('do')
                    ->putFile('workflows/workflows', $this->file('mobile_icon'), 'public')
            );

            Arr::set($validated, 'mobile_icon', $image_url);
        }

        return $validated;
    }
}
