<?php

namespace App\Http\Requests;

use App\Rules\WebhookInputRule;
use App\Rules\WebhookModelRule;
use App\Rules\WebhookPayloadRule;
use App\Rules\WebhookServiceRule;
use Illuminate\Foundation\Http\FormRequest;

class WebhookRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'service' => ['required', 'string', 'max:255', new WebhookServiceRule],
            'model' => ['required_unless:service,custom', 'string', 'max:255', new WebhookModelRule($this->service)],
            'url' => ['required_if:service,custom'],
            'method' => ['required_if:service,custom'],
            'payload' => ['required', 'array'],
            'payload.body' => ['required'],
            'payload.body.*' => [new WebhookInputRule],
            'payload.body.*.body_param' => ['required', new WebhookPayloadRule($this->service, $this->model)],
            'workflow' => ['required', 'exists:workflows,uid']
        ];
    }
}
