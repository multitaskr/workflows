<?php

namespace App\Http\Requests;

use App\Rules\NextSubjectStep;
use Illuminate\Foundation\Http\FormRequest;

class SubjectStepCompleteRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'sub_status' => ['sometimes', 'nullable'],
            'note' => ['sometimes', 'nullable'],
            'subject_step_uid' =>['required', new NextSubjectStep]
        ];
    }
}
