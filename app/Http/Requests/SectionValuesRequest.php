<?php

namespace App\Http\Requests;

use App\Models\Section;
use App\Models\InputValue;
use Illuminate\Foundation\Http\FormRequest;

class SectionValuesRequest extends FormRequest
{
    public $inputs;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [];

        foreach ($this->section->inputs as $input) {
            if(!$input->validations) {
                $rules['values.' . $input->uid] = 'nullable';
                continue;
            }
            
            $rules['values.' . $input->uid] = $input->validations;
        }

        $rules = array_merge($rules, [
            'subject_uid' => 'required|string|max:36',
            'subject_type' => 'required|string',
            'subject_api' => 'required|string'
        ]);
        
        return $rules;
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        $attributes = [];

        foreach ($this->section->inputs as $input) {
            $attributes['values.' . $input->uid] = $input->title;
        }
        
        return $attributes;
    }

    /**
     * Get the validated data from the request.
     *
     * @return array
     */
    public function validated()
    {
        $validated =  $this->validator->validated();
    
        InputValue::whereInputs(
            $this->section->inputs->pluck('id')->toArray(), 
            $validated['subject_uid'], 
            $validated['subject_type'], 
            $validated['subject_api'] 
        )->delete();

        return $validated;
    }
}
