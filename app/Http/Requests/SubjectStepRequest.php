<?php

namespace App\Http\Requests;

use App\Models\SubjectStep;
use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class SubjectStepRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'sub_status' => ['sometimes', 'nullable']
        ];
    }
}
