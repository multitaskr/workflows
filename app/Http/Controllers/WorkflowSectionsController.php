<?php

namespace App\Http\Controllers;

use App\Http\Resources\SectionResource;
use App\Models\Workflow;
use Illuminate\Http\Request;

class WorkflowSectionsController extends Controller
{
    /**
     * Get Sections of the workflow
     *
     * @param Workflow $workflow
     * @return void
     */
    public function index(Request $requets, Workflow $workflow)
    {
        $sections = $workflow->sections()->with('step')->get();

        return SectionResource::collection($sections);
    }
}
