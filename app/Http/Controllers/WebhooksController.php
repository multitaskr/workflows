<?php

namespace App\Http\Controllers;

use App\Http\Requests\WebhookRequest;
use App\Http\Resources\WebhookResource;
use App\Models\Webhook;
use App\Webhooks\WebhooksManager;
use Illuminate\Http\Request;

class WebhooksController extends Controller
{
    /**
     * Store a new webhook
     *
     * @param WebhooksManager $webhookManager
     * @param WebhookRequest $request
     * @return void
     */
    public function store(WebhooksManager $webhookManager, WebhookRequest $request)
    {
        $webhookService = $webhookManager::make($request->service, $request->model);

        $webhook = Webhook::create($webhookService->parseData($request));

        return new WebhookResource($webhook);
    }

    /**
     * Update a webhook
     *
     * @param WebhooksManager $webhookManager
     * @param Webhook $webhook
     * @param WebhookRequest $request
     * @return void
     */
    public function update(WebhooksManager $webhookManager,Webhook $webhook,  WebhookRequest $request)
    {
        $webhookService = $webhookManager::make($request->service, $request->model);

        $webhook->update($webhookService->parseData($request));

        return new WebhookResource($webhook);
    }

    /**
     * Delete a webhook
     *
     * @param Webhook $webhook
     * @return void
     */
    public function destroy(Webhook $webhook)
    {
        $webhook->delete();

        return response([
            'message' => "The webhook has beed deleted"
        ], 200);
    }
    
}
