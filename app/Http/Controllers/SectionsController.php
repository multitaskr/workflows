<?php

namespace App\Http\Controllers;

use App\Http\Requests\ListRequest;
use App\Http\Requests\SectionRequest;
use App\Http\Requests\SectionOrderRequest;
use App\Http\Resources\SectionResource;
use App\Models\Section;

class SectionsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param \App\Http\Requests\ListRequest $request
     * @return \App\Http\Resources\SectionResource
     */
    public function index(ListRequest $request)
    {
        return SectionResource::collection(
            Section::filter($request->query())
                ->paginate($request->get('limit', 25))
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\SectionRequest  $request
     * @return \App\Http\Resources\SectionResource
     */
    public function store(SectionRequest $request)
    {
        $section = Section::create($request->validated());

        return new SectionResource($section);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Section
     * @return \App\Http\Resources\SectionResource
     */
    public function show(Section $section)
    {
        $section->load('step.workflow', 'inputs');

        return new SectionResource($section);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\SectionRequest  $request
     * @param  \App\Models\Section $section
     * @return \App\Http\Resources\SectionResource
     */
    public function update(SectionRequest $request, Section $section)
    {
        $section->update($request->validated());

        $section->load('step');

        return new SectionResource($section);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Section $section
     * @return \Illuminate\Http\Response
     */
    public function destroy(Section $section)
    {
        $section->delete();

        return response([
            'message' => 'Section has been deleted'
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function order(SectionOrderRequest $request)
    {
        foreach ($request->sections as $key => $uid) {
            Section::where('uid', $uid)
                ->update(['order' => $key + 1]);
        }

        return response([
            'message' => 'Sections have been reordered.'
        ], 200);
    }
}
