<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Storage;

class FilesController extends Controller
{
    public function show(Request $request, $file_name)
    {
        if (Storage::disk('do')->exists('private/workflows/'. $file_name)) {
            $mime_type = Storage::disk('do')
            ->mimeType('private/workflows/'. $file_name);

            $file = Storage::disk('do')
            ->get('private/workflows/'. $file_name);
            
            return response()->make($file, 200, ['Content-Type' => $mime_type]);
        }else{
            return response()->json([
                'message' => 'File not found'
            ], 404);
        }
    }
}
