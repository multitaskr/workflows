<?php

namespace App\Http\Controllers;

use App\Models\Input;
use App\Http\Requests\InputRequest;
use App\Http\Requests\ListRequest;
use App\Http\Resources\InputResource;
use App\Http\Requests\InputOrderRequest;

class InputsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param \App\Http\Requests\ListRequest $request
     * @return \App\Http\Resources\InputResource
     */
    public function index(ListRequest $request)
    {
        return InputResource::collection(
            Input::filter($request->query())
                ->paginate($request->get('limit', 25))
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\InputRequest $request
     * @return \App\Http\Resources\InputResource
     */
    public function store(InputRequest $request)
    {
        $input = Input::create($request->validated());

        return new InputResource($input);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Input $input
     * @return \App\Http\Resources\InputResource
     */
    public function show(Input $input)
    {
        $input->load('section.step.workflow');

        return new InputResource($input);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\InputRequest $request
     * @param  \App\Models\Input $input
     * @return \App\Http\Resources\InputResource
     */
    public function update(InputRequest $request, Input $input)
    {
        $input->update($request->validated());

        $input->load('section');

        return new InputResource($input);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Input $input
     * @return \App\Http\Resources\InputResource
     */
    public function destroy(Input $input)
    {
        $input->delete();

        return response([
            'message' => 'Input has been deleted'
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function order(InputOrderRequest $request)
    {
        foreach ($request->inputs as $key => $uid) {
            Input::where('uid', $uid)
                ->update(['order' => $key + 1]);
        }

        return response([
            'message' => 'Inputs have been reordered.'
        ], 200);
    }
}
