<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Webhooks\WebhooksManager;

class WebhooksManagerController extends Controller
{
    /**
     * Get weebhook services
     *
     * @param WebhooksManager $webhooksManager
     * @return response
     */
    public function services(WebhooksManager $webhooksManager)
    {
        return response([
            'data' => $webhooksManager::servicesList()
        ], 200);
    }

    /**
     * Get service models
     *
     * @param WebhooksManager $webhooksManager
     * @param string $service
     * @return void
     */
    public function models(WebhooksManager $webhooksManager, string $service)
    {
        return response([
            'data' => $webhooksManager::serviceModels($service)
        ], 200);
    }

    /**
     * Get model columns
     *
     * @param WebhooksManager $webhooksManager
     * @param string $service
     * @param string $model
     * @return void
     */
    public function columns(WebhooksManager $webhooksManager, string $service, string $model)
    {
        return response([
            'data' => $webhooksManager::modelColumns($service, $model)
        ], 200);
    }
}
