<?php

namespace App\Http\Controllers;

use App\Http\Resources\TagResource;
use App\Models\Tag;
use Illuminate\Http\Request;

class WorkflowsTagsController extends Controller
{
    /**
     * Get Sections of the workflow
     */
    public function index(Request $request)
    {
        return TagResource::collection(
            Tag::filter($request->query())
                ->with('workflows')
                ->paginate($request->get('limit', 10))
        );
    }
}
