<?php

namespace App\Http\Controllers;

use App\Models\Step;
use App\Http\Requests\ListRequest;
use App\Http\Requests\StepRequest;
use App\Http\Requests\StepOrderRequest;
use App\Http\Resources\StepResource;

class StepsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \App\Http\Requests\ListRequest
     * @return \App\Http\Resources\StepResource
     */
    public function index(ListRequest $request)
    {
        return StepResource::collection(
            Step::filter($request->query())
                ->paginate($request->get('limit', 25))
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StepRequest $request
     * @return \App\Http\Resources\StepResource
     */
    public function store(StepRequest $request)
    {
        $step = Step::create($request->validated());

        return new StepResource($step);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Step  $step
     * @return \App\Http\Resources\StepResource
     */
    public function show(Step $step)
    {
        $step->load('workflow', 'sections.inputs', 'logics');

        return new StepResource($step);
    }

    /**
     * Update the specified resource in storage.
     * 
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Step  $step
     * @return \App\Http\Resources\StepResource
     */
    public function update(StepRequest $request, Step $step)
    {
        $step->update($request->validated());

        $step->load('workflow');

        return new StepResource($step);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Step  $step
     * @return \Illuminate\Http\Response
     */
    public function destroy(Step $step)
    {
        $step->delete();

        return response([
            'message' => 'Step has been deleted'
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function order(StepOrderRequest $request)
    {
        foreach ($request->steps as $key => $uid) {
            Step::where('uid', $uid)
                ->update(['order' => $key + 1]);
        }

        return response([
            'message' => 'Steps have been reordered.'
        ], 200);
    }
}
