<?php

namespace App\Http\Controllers;

use App\Models\Section;
use App\Models\InputValue;
use Illuminate\Http\Request;
use App\Http\Resources\InputValueResource;
use App\Http\Requests\SectionValuesRequest;
use App\Http\Requests\SectionValuesListRequest;

class SectionValuesController extends Controller
{
    /**
     * Store the inout values
     *
     * @param \App\Http\Requests\SectionValuesRequest $request
     * @return \App\Http\Resources\InputValueResource
     */
    public function store(Section $section, SectionValuesRequest $request)
    {
        $values = InputValue::createValues($request->validated());

        return InputValueResource::collection($values);
    }

    /**
     * Get the input values of the section
     *
     * @param Section $section
     * @param SectionValuesListRequest $request
     * @return \App\Http\Resources\InputValueResource
     */
    public function show(Section $section, SectionValuesListRequest $request)
    {
        $values = InputValue::whereInputs(
            $section->inputs->pluck('id')->toArray(), 
            $request->subject_uid,
            $request->subject_type,
            $request->subject_api
        )->get();

        return InputValueResource::collection($values);
    }
}
