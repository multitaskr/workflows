<?php

namespace App\Http\Controllers\Privates;

use App\Models\Input;
use App\Models\InputValue;
use App\Http\Controllers\Controller;
use App\Http\Resources\InputValueResource;
use App\Http\Requests\InputValueUpdateRequest;
use App\Http\Resources\InputResource;

class InputsController extends Controller
{
    /**
     * Undocumented function
     * 
     * @param InputValueUpdateRequest $request
     * @param Input $input
     * @return InputValueResource
     */
    public function value(InputValueUpdateRequest $request, Input $input)
    {
        InputValue::whereInputs(
            [$input->id], 
            $request->subject_uid, 
            $request->subject_type, 
            $request->subject_api 
        )->delete();

        $values = [];
        $values[$input->uid] = $request->value;

        $input_values = InputValue::createValues([
            'subject_uid' => $request->subject_uid, 
            'subject_type' => $request->subject_type, 
            'subject_api' => $request->subject_api,
            'values' => $values
        ]);

        return $input_values->count() ? new InputValueResource($input_values->first()) : [
            'data' => null
        ];
    }

    /**
     * Undocumented function
     * 
     * @param Input $input
     * @return InputResource
     */
    public function show(Input $input)
    {
        $input->load('section.step.workflow');

        return new InputResource($input);
    }
}
