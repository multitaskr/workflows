<?php

namespace App\Http\Controllers\Privates;

use App\Models\Workflow;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\StepResource;

class WorkflowsStepController extends Controller
{
    /**
     * Get the workflows steps
     *
     * @param Workflow $workflow
     * @return \App\Http\Resources\StepResource
     */
    public function index(Workflow $workflow)
    {
        return StepResource::collection($workflow->steps);
    }

    /**
     * Get the public step of the workflow
     *
     * @param Workflow $workflow
     * @return \App\Http\Resources\StepResource
     */
    public function public(Workflow $workflow)
    {
        $step = $workflow->steps()->public()->first();

        if(!$step){
            return response([ 'data' => [
                    'message' => "The workflow doesn't have a public step"
                ]
            ], 200);
        }

        $step->load('workflow', 'sections.inputs', 'logics');

        return new StepResource($step);
    }
}
