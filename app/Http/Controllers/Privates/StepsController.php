<?php

namespace App\Http\Controllers\Privates;

use App\Models\Step;
use App\Models\InputValue;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\StepResource;
use App\Http\Resources\InputValueResource;
use App\Http\Requests\StepValuesListRequest;

class StepsController extends Controller
{
    /**
     * Undocumented function
     *
     * @param Step $step
     * @return void
     */
    public function show(Step $step)
    {
        return new StepResource($step);
    }

    /**
     * Undocumented function
     *
     * @param Step $step
     * @param StepValuesListRequest $request
     * @return \App\Http\Resources\InputValueResource
     */
    public function values(Step $step, StepValuesListRequest $request)
    {
        $values = InputValue::whereInputs(
            $step->inputs->pluck('id')->toArray(), 
            $request->subject_uid,
            $request->subject_type,
            $request->subject_api
        )->with('input')->get();

        return InputValueResource::collection($values);
    }
}
