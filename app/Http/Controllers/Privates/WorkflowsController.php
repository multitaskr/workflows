<?php

namespace App\Http\Controllers\Privates;

use App\Models\Workflow;
use Illuminate\Http\Request;
use App\Http\Requests\ListRequest;
use App\Http\Controllers\Controller;
use App\Http\Resources\WorkflowResource;

class WorkflowsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \App\Http\Requests\ListRequest $request
     * @return \App\Http\Resources\WorkflowResource
     */
    public function index(ListRequest $request)
    {
        return WorkflowResource::collection(
            Workflow::filter($request->query())
                ->with('tags')
                ->paginate($request->get('limit', 25))
        );
    }
    
    /**
     * Get workflow
     *
     * @param Workflow $workflow
     * @return App\Http\Resources\WorkflowResource
     */
    public function show(Workflow $workflow)
    {
        return new WorkflowResource(
            $workflow->load('steps', 'steps.sections.inputs')
        );    
    }

    /**
     * Get workflow by slug
     *
     * @param Workflow $workflow
     * @return App\Http\Resources\WorkflowResource
     */
    public function showBySlug($slug)
    {
        $workflow = Workflow::whereSlug($slug)->firstOrFail();

        $workflow->load('steps', 'steps.sections.inputs');

        return new WorkflowResource($workflow);    
    }
}
