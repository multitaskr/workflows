<?php

namespace App\Http\Controllers\Privates;

use App\Models\Workflow;
use App\Models\SubjectStep;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\SubjectStepResource;
use App\Http\Requests\WorkflowSubjectStepRequest;

class WorkflowSubjectStepsController extends Controller
{
      /**
     * Clone workfloe steps
     *
     * @param Workflow $workflow
     * @param WorkflowSubjectStepRequest $request
     * @return void
     */
    public function store(Workflow $workflow, WorkflowSubjectStepRequest $request)
    {
        $steps = SubjectStep::cloneFromWorkflow(
            $workflow, 
            $request->subject_api,
            $request->subject_type,
            $request->subject_uid);
        
        return SubjectStepResource::collection($steps);
    }

    /**
     * Restart steps 
     *
     * @param Workflow $workflow
     * @param WorkflowSubjectStepRequest $request
     * @return void
     */
    public function restart(Workflow $workflow, WorkflowSubjectStepRequest $request)
    {
        $stepMetaData = SubjectStep::restartFromWorkflow(
            $workflow, 
            $request->subject_api,
            $request->subject_type,
            $request->subject_uid
        );


        return response()->json([
            'data' => $stepMetaData
        ]);
    }
}
