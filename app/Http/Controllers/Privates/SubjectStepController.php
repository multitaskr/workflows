<?php

namespace App\Http\Controllers\Privates;

use Deals;
use App\Models\Step;
use App\Models\SubjectStep;
use Illuminate\Http\Request;
use App\Http\Requests\ListRequest;
use App\Http\Controllers\Controller;
use App\Http\Requests\SubjectStepRequest;
use App\Http\Resources\SubjectStepResource;
use App\Http\Requests\WorkflowSubjectStepRequest;

class SubjectStepController extends Controller
{
    /**
     * Get subject steps
     *
     * @param ListRequest $request
     * @return void
     */
    public function index(ListRequest $request)
    {
        return SubjectStepResource::collection(
            SubjectStep::filter($request->query())
                ->with('step')
                ->paginate($request->get('limit', 25))
        );
    }

    /**
     * Get subject step
     *
     * @param SubjectStep $subjectStep
     * @return void
     */
    public function show(SubjectStep $subjectStep)
    {
        return new SubjectStepResource($subjectStep->load('step'));
    }

    /**
     * Get subject step from step
     *
     * @return void
     */
    public function fromStep(Step $step, WorkflowSubjectStepRequest $request)
    {
        $subjectStep = SubjectStep::where([
            'subject_api' => $request->subject_api,
            'subject_type' => $request->subject_type,
            'subject_uid' => $request->subject_uid,
            'step_id' => $step->id
        ])->first();

        if(!$subjectStep){
            return response()->json([
                'data' => null
            ]);
        }

        return  new SubjectStepResource($subjectStep->load('step'));
    }

    /**
     * Update subject step from step
     *
     * @return void
     */
    public function fromStepComplete(Step $step, WorkflowSubjectStepRequest $request)
    {

        $subjectStep = SubjectStep::where([
            'subject_api' => $request->subject_api,
            'subject_type' => $request->subject_type,
            'subject_uid' => $request->subject_uid,
            'step_id' => $step->id
        ])->first();
        
        
        if(!$subjectStep){
            return response()->json([
                'data' => null
            ]);
        }
        
        $subjectStep->complete($request->sub_status, null, $request->note);
        
        return  new SubjectStepResource($subjectStep->load('step'));
    }
    
    /**
     * Complete subject step
     *
     * @param SubjectStep $subjectStep
     * @param SubjectStepRequest $request
     * @return void
     */
    public function complete(SubjectStep $subjectStep, SubjectStepRequest $request)
    {
        $subjectStep->complete($request->sub_status, null, $request->note);

        return new SubjectStepResource($subjectStep);
    }

     /**
     * rollback subject step
     *
     * @param SubjectStep $subjectStep
     * @param SubjectStepRequest $request
     * @return void
     */
    public function rollback(SubjectStep $subjectStep, SubjectStepRequest $request)
    {
        $subjectStep->rollback($request->sub_status, null, $request->note);

        return new SubjectStepResource($subjectStep);
    }
    
    public function importFromDeals(Request $request)
    {
        $deal = Deals::getDeal($request->deal_uid);

        return response()->json([
            'data' =>  SubjectStep::importFromDeal($deal)
        ], 200);
    }
}
