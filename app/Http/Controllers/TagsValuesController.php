<?php

namespace App\Http\Controllers;

use App\Models\InputValue;
use Illuminate\Http\Request;
use App\Http\Requests\TagsValuesRequest;
use App\Http\Resources\InputValueResource;

class TagsValuesController extends Controller
{   
    /**
     * Get tag values
     *
     * @param TagsValuesRequest $request
     * @return \App\Http\Resources\InputValueResource 
     */
    public function show(TagsValuesRequest $request)
    {
        $values = InputValue::whereTags(
            $request->tags,
            $request->subject_uid,
            $request->subject_type,
            $request->subject_api
        )->get();

        return InputValueResource::collection($values);
    }
}
