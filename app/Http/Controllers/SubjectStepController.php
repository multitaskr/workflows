<?php

namespace App\Http\Controllers;

use App\Models\Step;
use App\Models\SubjectStep;
use App\Http\Requests\ListRequest;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\SubjectStepRequest;
use App\Http\Resources\SubjectStepResource;
use App\Http\Requests\SubjectStepCompleteRequest;
use App\Http\Requests\SubjectStepStepCompleteRequest;

class SubjectStepController extends Controller
{
    /**
     * Get subject steps
     *
     * @param ListRequest $request
     * @return void
     */
    public function index(ListRequest $request)
    {
        return SubjectStepResource::collection(
            SubjectStep::filter($request->query())
                ->with('step')
                ->paginate($request->get('limit', 25))
        );
    }

    /**
     * Get subject step
     *
     * @param SubjectStep $subjectStep
     * @return void
     */
    public function show(SubjectStep $subjectStep)
    {
        return new SubjectStepResource($subjectStep->load('step'));
    }


    /**
     * Complete subject step
     *
     * @param SubjectStep $subjectStep
     * @param SubjectStepRequest $request
     * @return void
     */
    public function complete(SubjectStep $subjectStep, SubjectStepCompleteRequest $request)
    {
        $response = $subjectStep->complete($request->sub_status, Auth::user(), $request->note, $request->subject_step_uid);
       
        if(!$response->success && $response->error_type = 'validation'){
            return response()->json([
                "errors" => [
                    "subject_step_uid" => [
                       $response->error_message
                    ]
                ]
            ], 422);
        }

        return new SubjectStepResource($subjectStep);
    }

    /**
     * Update subject step from step
     *
     * @return void
     */
    public function fromStepComplete(Step $step, SubjectStepStepCompleteRequest $request)
    {
        $subjectStep = SubjectStep::where([
            'subject_api' => $request->subject_api,
            'subject_type' => $request->subject_type,
            'subject_uid' => $request->subject_uid,
            'step_id' => $step->id
        ])->first();
        
        
        if(!$subjectStep){
            return response()->json([
                'data' => null
            ]);
        }

        if($subjectStep->status !== 'in progress'){
            return  response()->json(['message' => 'Unauthorized'], 403);
        }

        $response = $subjectStep->complete($request->sub_status, Auth::user(), $request->note, $request->subject_step_uid);


        if(!$response->success && $response->error_type = 'validation'){
            return response()->json([
                "errors" => [
                    "subject_step_uid" => [
                       $response->error_message
                    ]
                ]
            ], 422);
        }
        
        return  new SubjectStepResource($subjectStep->load('step'));
    }
    

     /**
     * rollback subject step
     *
     * @param SubjectStep $subjectStep
     * @param SubjectStepRequest $request
     * @return void
     */
    public function rollback(SubjectStep $subjectStep, SubjectStepRequest $request)
    {
        $subjectStep->rollback($request->sub_status, Auth::user(), $request->note);

        return new SubjectStepResource($subjectStep);
    }

    /**
     * Update subject step
     *
     * @param SubjectStep $subjectStep
     * @param SubjectStepRequest $request
     * @return void
     */
    public function update(SubjectStep $subjectStep, SubjectStepRequest $request)
    {
        $subjectStep->setStatus($request->status, Auth::user(), $request->sub_status);

        return new SubjectStepResource($subjectStep);
    }
}
