<?php

namespace App\Http\Controllers;

use App\Models\Workflow;
use App\Models\SubjectStep;
use Illuminate\Http\Request;
use App\Http\Requests\WorkflowSubjectStepRequest;
use App\Http\Resources\SubjectStepResource;

class WorkflowSubjectStepsController extends Controller
{
    /**
     * Clone workfloe steps
     *
     * @param Workflow $workflow
     * @param WorkflowSubjectStepRequest $request
     * @return void
     */
    public function store(Workflow $workflow, WorkflowSubjectStepRequest $request)
    {
        $steps = SubjectStep::cloneFromWorkflow(
            $workflow, 
            $request->subject_api,
            $request->subject_type,
            $request->subject_uid);
        
        return SubjectStepResource::collection($steps);
    }
}
