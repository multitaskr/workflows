<?php

namespace App\Http\Controllers;

use App\Models\Tag;
use Illuminate\Http\Request;
use App\Http\Requests\ListRequest;
use App\Http\Resources\TagResource;

class TagsController extends Controller
{
    public function index(ListRequest $request)
    {
        return TagResource::collection(
            Tag::filter($request->query())
            ->paginate($request->get('limit', 1000))
        );
    }
}
