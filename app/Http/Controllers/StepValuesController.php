<?php

namespace App\Http\Controllers;

use App\Models\Step;
use App\Models\InputValue;
use App\Http\Requests\StepValuesRequest;
use App\Http\Resources\InputValueResource;
use App\Http\Requests\StepValuesListRequest;
use Auth;

class StepValuesController extends Controller
{
    /**
     * Store the inout values
     *
     * @param \App\Http\Requests\StepValuesRequest $request
     * @return \App\Http\Resources\InputValueResource
     */
    public function store(Step $step, StepValuesRequest $request)
    {    
        $values = InputValue::createValues($request->validated());

        return InputValueResource::collection($values);
    }

    /**
     * Undocumented function
     *
     * @param Step $step
     * @param StepValuesListRequest $request
     * @return \App\Http\Resources\InputValueResource
     */
    public function show(Step $step, StepValuesListRequest $request)
    {
        $values = InputValue::whereInputs(
            $step->inputs->pluck('id')->toArray(), 
            $request->subject_uid,
            $request->subject_type,
            $request->subject_api
        )->get();

        return InputValueResource::collection($values);
    }
}
