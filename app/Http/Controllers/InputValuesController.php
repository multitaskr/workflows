<?php

namespace App\Http\Controllers;

use App\Http\Requests\ListRequest;
use App\Http\Resources\InputValueResource;
use App\Models\InputValue;
use Illuminate\Http\Request;

class InputValuesController extends Controller
{
    public function index(ListRequest $request)
    {
        return InputValueResource::collection(
            InputValue::filter($request->query())
                ->with('input.section.step.workflow')
                ->paginate($request->get('limit', 25))
        );
    }

    public function show(InputValue $input_value)
    {
        $input_value->load('input.section.step.workflow');

        return new InputValueResource($input_value);
    }

    /**
     * Get tag values by raw filter
     *
     * @param TagsValuesRequest $request
     * @return \App\Http\Resources\InputValueResource 
     */
    public function raw(Request $request)
    {
        $values = InputValue::with('input')
            ->where('input_values.subject_api', $request->subject_api)
            ->where('input_values.subject_type', $request->subject_type)
            ->where('input_values.subject_uid', $request->subject_uid)
            ->whereHas('input', function($query) use($request) {
                $query->whereRaw($request->raw);
            })->get();


        return InputValueResource::collection($values);
    }
}
