<?php

namespace App\Http\Controllers;

use App\Models\Workflow;
use Illuminate\Support\Arr;
use Illuminate\Http\Request;
use App\Http\Resources\StepResource;
use Illuminate\Support\Facades\Auth;

class WorkflowsStepsController extends Controller
{
    /**
     * Get the public step of the workflow
     *
     * @param string $slug
     * @return \App\Http\Resources\StepResource
     */
    public function public($slug)
    {
        $workflow = Workflow::whereSlug($slug)->first();

        if (!$workflow) {
            $workflow = Workflow::where('uid', $slug)
                ->orWhere('name', 'like', "%" . ucwords(str_replace('-', ' ', $slug)) . "%")
                ->firstOrFail();
        }
        
        if ($workflow->protected) {
            if (!Auth::check()) {
                return response()->json(['message' => 'Unauthorized'], 403);
            } else {
                $roles = Auth::user()->roles;
                if (Arr::first($roles) == 'Customer') {
                    return response()->json(['message' => 'Unauthorized'], 403);
                }
            }
        }

        $step = $workflow->steps()->public()
        ->with(['sections' =>function($q){
            $q->private(false);
        }, 'sections.inputs'])
        ->first();

        if(!$step){
            return  response([
                'data' => []
            ], 200);
        }

        $step->load('workflow', 'logics');


        return new StepResource($step);
    }
}
