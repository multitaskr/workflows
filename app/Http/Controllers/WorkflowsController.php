<?php

namespace App\Http\Controllers;

use App\Models\Workflow;
use App\Http\Requests\ListRequest;
use App\Http\Requests\WorkflowRequest;
use App\Http\Resources\WorkflowResource;

class WorkflowsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \App\Http\Requests\ListRequest $request
     * @return \App\Http\Resources\WorkflowResource
     */
    public function index(ListRequest $request)
    {
        return WorkflowResource::collection(
            Workflow::filter($request->query())
                ->with('tags')
                ->paginate($request->get('limit', 25))
        );
    }

    /**
     * Display a listing of the resource.
     *
     * @param  \App\Http\Requests\ListRequest $request
     * @return \App\Http\Resources\WorkflowResource
     */
    public function public(ListRequest $request)
    {
        return WorkflowResource::collection(
            Workflow::filter($request->query())
                ->where('public', 1)
                ->where('protected', false)
                ->orderBy('order', 'ASC')
                ->with('tags')
                ->paginate($request->get('limit', 25))
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\WorkflowRequest  $request
     * @return \App\Http\Resources\WorkflowResource
     */
    public function store(WorkflowRequest $request)
    {
        $workflow = Workflow::create($request->validated());

        $workflow->syncTags($request->get('tags', []));

        return new WorkflowResource($workflow->load('tags'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Workflow $workflow
     * @return \App\Http\Resources\WorkflowResource
     */
    public function show(Workflow $workflow)
    {
        $workflow->load('steps', 'steps.sections.inputs', 'tags');

        return new WorkflowResource($workflow);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\WorkflowRequest  $request
     * @param  \App\Models\Workflow $workflow
     * @return \App\Http\Resources\WorkflowResource
     */
    public function update(WorkflowRequest $request, Workflow $workflow)
    {
        $workflow->update($request->validated());

        $workflow->syncTags($request->get('tags', []));

        return new WorkflowResource($workflow->load('tags'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Workflow $workflow
     * @return \Illuminate\Http\Response
     */
    public function destroy(Workflow $workflow)
    {
        $workflow->delete();

        return response([
            'message' => 'Workflow has been deleted'
        ], 200);
    }
}
