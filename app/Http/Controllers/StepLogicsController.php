<?php

namespace App\Http\Controllers;

use App\Http\Requests\ListRequest;
use App\Models\StepLogic;
use Illuminate\Http\Request;
use App\Http\Requests\StepLogicRequest;
use App\Http\Resources\StepLogicResource;

class StepLogicsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param \App\Http\Requests\ListRequest  $request
     * @return \App\Http\Resources\StepLogicResource
     */
    public function index(ListRequest $request)
    {
        return StepLogicResource::collection(
            StepLogic::filter($request->query())
            ->paginate($request->get('limit', 25))
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StepLogicRequest  $request
     * @return \App\Http\Resources\StepLogicResource
     */
    public function store(StepLogicRequest $request)
    {
        $stepLogic = StepLogic::create($request->validated());

        return new StepLogicResource($stepLogic);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\StepLogic  $stepLogic
     * @return \App\Http\Resources\StepLogicResource
     */
    public function show(StepLogic $stepLogic)
    {
        $stepLogic->load('step');

        return new StepLogicResource($stepLogic);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\StepLogicRequest  $request
     * @param  \App\Models\StepLogic  $stepLogic
     * @return \App\Http\Resources\StepLogicResource
     */
    public function update(StepLogicRequest $request, StepLogic $stepLogic)
    {
        $stepLogic->update($request->validated());

        $stepLogic->load('step');

        return new StepLogicResource($stepLogic);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\StepLogic  $stepLogic
     * @return \Illuminate\Http\Response
     */
    public function destroy(StepLogic $stepLogic)
    {
        $stepLogic->delete();

        return  response([
            'message' => 'Step Logic has been deleted'
        ], 200);
    }
}
