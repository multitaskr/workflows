<?php

namespace App\Http\Resources;

use Illuminate\Support\Arr;
use Illuminate\Http\Resources\Json\JsonResource;

class SectionResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return $this->prepareResponse($request);
    }

    /**
     * Prepare the resource response 
     *
     * @param $request
     * @return array
     */
    public function prepareResponse($request)
    {       

        $data = array_merge(parent::toArray($request), [
            'step' => new StepResource($this->whenLoaded('step'))
        ]);

        if($request->has(['subject_uid', 'subject_type', 'subject_api'])){   
            $data['values'] = InputValueResource::collection($this->values()->subject(
                $request->subject_uid,
                $request->subject_type,
                $request->subject_api
                )->get());
        }
        
        return Arr::except($data, ['id', 'step_id']);
    }
}
