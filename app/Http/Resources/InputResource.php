<?php

namespace App\Http\Resources;

use Illuminate\Support\Arr;
use Illuminate\Http\Resources\Json\JsonResource;

class InputResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return $this->prepareResponse($request);
    }

    /**
     * Prepare the resource response 
     *
     * @param $request
     * @return array
     */
    public function prepareResponse($request)
    {
        $data = array_merge(parent::toArray($request), [
            'section' => new SectionResource($this->whenLoaded('step'))
        ]);

        return Arr::except(parent::toArray($data), ['id', 'section_id']);
    }
}
