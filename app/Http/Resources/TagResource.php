<?php

namespace App\Http\Resources;

use Illuminate\Support\Arr;
use Illuminate\Http\Resources\Json\JsonResource;

class TagResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return $this->prepareResponse($request);
    }

     /**
     * Prepare the resource response 
     *
     * @param $request
     * @return array
     */
    public function prepareResponse($request)
    {
        return Arr::except(parent::toArray($request), ['id']);
    }
}
