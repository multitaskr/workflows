<?php

namespace App\Http\Resources;

use Illuminate\Support\Arr;
use Illuminate\Http\Resources\Json\JsonResource;

class SubjectStepResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return $this->prepareResponse($request);
    }

     /**
     * Prepare the resource response 
     *
     * @param $request
     * @return array
     */
    public function prepareResponse($request)
    {
        $data = array_merge(
            parent::toArray($request),[
                'time_worked_for_hummans' => $this->time_worked_for_hummans,
            ]
        );

        return Arr::except($data, ['id', 'step_id']);
    }
}
