<?php

namespace App\Providers;

use App\Models\Input;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Route;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to your controller routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';
    protected $private_namespace = 'App\Http\Controllers\Privates';

    /**
     * The path to the "home" route for your application.
     *
     * @var string
     */
    public const HOME = '/home';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        Route::bind('input', function ($value) {
            return Input::where('uid', $value)->orWhere('tag', $value)->firstOrFail();
        });

        parent::boot();
    }

    /**
     * Define the routes for the application.
     *
     * @return void
     */
    public function map()
    {
        $this->mapPrivateRoutes();
        $this->mapApiRoutes();
    }

    /**
     * Define the "api" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapApiRoutes()
    {
        Route::middleware('api')
            ->prefix('workflows')
            ->namespace($this->namespace)
            ->group(base_path('routes/api.php'));
    }

    /**
     * Define the "private" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapPrivateRoutes()
    {
        Route::middleware('api')
            ->domain(config('app.private_domain'))
            ->namespace($this->private_namespace)
            ->as('private.')
            ->group(base_path('routes/private.php'));
    }
}
