<?php

namespace App\Rules;

use App\Models\Input;
use Illuminate\Support\Str;
use Illuminate\Contracts\Validation\Rule;

class WebhookInputRule implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        return Input::whereUid(Str::afterLast($attribute, '.'))->exists();
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'The selected :attribute is invalid.';
    }
}
