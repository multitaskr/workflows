<?php

namespace App\Rules;


class CreditCard 
{
    public $requiredFields = [
        'card_number',
        'owner',
        'expiration_date',
        'ccv'
    ];

    
    public function validate($attribute, $value, $parameters, $validator)
    {
        $value = json_decode($value, true);

        if(!is_array($value)){
            $customMessage = "The field must be a valid JSON string";

            $validator->addReplacer('maps', 
                function($message, $attribute, $rule, $parameters) use ($customMessage) {
                    return \str_replace(':message', $customMessage, $message);
                }
            );

            return false;
        }

        $value = array_filter($value);

        foreach ($this->requiredFields as $field) {
            if(!array_key_exists($field, $value)){
                $customMessage = "The field {$field} is required.";

                $validator->addReplacer('maps', 
                    function($message, $attribute, $rule, $parameters) use ($customMessage) {
                        return \str_replace(':message', $customMessage, $message);
                    }
                );

                return false;
            }
        }

        return true;
    }
}
