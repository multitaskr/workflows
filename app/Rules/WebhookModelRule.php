<?php

namespace App\Rules;

use App\Webhooks\WebhooksManager;
use Illuminate\Contracts\Validation\Rule;

class WebhookModelRule implements Rule
{
    public $service;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($service)
    {
        $this->service = $service;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if(!$this->service) return false;

        return in_array($value, WebhooksManager::serviceModels($this->service));
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'The selected :attribute is invalid.';
    }
}
