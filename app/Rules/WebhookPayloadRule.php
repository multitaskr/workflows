<?php

namespace App\Rules;

use Illuminate\Support\Arr;
use App\Webhooks\WebhooksManager;
use Illuminate\Contracts\Validation\Rule;

class WebhookPayloadRule implements Rule
{
    public $service;

    public $model;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($service, $model)
    {
        $this->service = $service;
        $this->model = $model;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if($this->service === 'custom') return true;

        if(!$this->service || !$this->model) return false;

        return Arr::has(WebhooksManager::modelColumns($this->service, $this->model),$value);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'The selected :attribute is invalid.';
    }
}
