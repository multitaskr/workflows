<?php
namespace App\Filters\Shared;

use App\Filters\FilterAbstract;
use Illuminate\Database\Eloquent\Builder;

class RawFilter extends FilterAbstract
{
    public function filter(Builder $builder, $value)
    {
        return $builder->whereRaw($value);
    }
}