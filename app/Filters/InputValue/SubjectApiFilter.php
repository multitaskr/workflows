<?php
namespace App\Filters\InputValue;

use App\Filters\FilterAbstract;
use Illuminate\Database\Eloquent\Builder;

class SubjectApiFilter extends FilterAbstract
{
    
    public function filter(Builder $builder, $value)
    {
        return $builder->where('subject_api', $value);
    }
}