<?php

namespace App\Filters\Workflow;

use App\Filters\FilterAbstract;
use Illuminate\Database\Eloquent\Builder;

class TagsFilter extends FilterAbstract
{

    public function filter(Builder $builder, $value)
    {
        $tags = explode(',', $value);

        $builder->whereHas('tags', function ($query) use ($tags) {
            $query->whereIn('slug', $tags)->orWhereIn('name', $tags);
        });

        return $builder;
    }
}
