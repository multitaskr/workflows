<?php
namespace App\Filters\Workflow;

use App\Filters\FilterAbstract;
use Illuminate\Database\Eloquent\Builder;

class NamesFilter extends FilterAbstract
{
    public function filter(Builder $builder, $value)
    {
        $values = explode(',', $value);

        return $builder->whereIn('name', $values);
    }
}