<?php
namespace App\Filters\Workflow;

use App\Filters\FilterAbstract;
use Illuminate\Database\Eloquent\Builder;

class NoVerticalsFilter extends FilterAbstract
{
    public function filter(Builder $builder, $value)
    {
        $values = explode(',', $value);

        return $builder->whereNotIn('vertical', $values);
    }
}