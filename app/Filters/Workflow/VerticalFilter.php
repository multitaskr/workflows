<?php
namespace App\Filters\Workflow;

use App\Filters\FilterAbstract;
use Illuminate\Database\Eloquent\Builder;

class VerticalFilter extends FilterAbstract
{
    public function filter(Builder $builder, $value)
    {
        return $builder->where('vertical', $value);
    }
}