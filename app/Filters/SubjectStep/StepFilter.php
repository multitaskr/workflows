<?php
namespace App\Filters\SubjectStep;

use App\Models\Step;
use App\Filters\FilterAbstract;
use Illuminate\Database\Eloquent\Builder;

class StepFilter extends FilterAbstract
{
    
    public function filter(Builder $builder, $value)
    {
        $step = Step::whereUid($value)->first();

        return $builder->where('step_id', $step? $step->id : null);
    }
}