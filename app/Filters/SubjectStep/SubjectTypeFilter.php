<?php
namespace App\Filters\SubjectStep;

use App\Filters\FilterAbstract;
use Illuminate\Database\Eloquent\Builder;

class SubjectTypeFilter extends FilterAbstract
{
    
    public function filter(Builder $builder, $value)
    {
        return $builder->where('subject_type', $value);
    }
}