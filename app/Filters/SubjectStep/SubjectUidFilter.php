<?php
namespace App\Filters\SubjectStep;

use App\Filters\FilterAbstract;
use Illuminate\Database\Eloquent\Builder;

class SubjectUidFilter extends FilterAbstract
{
    
    public function filter(Builder $builder, $value)
    {
        return $builder->where('subject_uid', $value);
    }
}