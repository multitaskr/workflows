<?php
namespace App\Filters\Step;

use App\Filters\FilterAbstract;
use Illuminate\Database\Eloquent\Builder;

class PublicFilter extends FilterAbstract
{
    /**
	 * filter's value mapper 
	 *
	 * @return void
	 */
    public function mappings()
	{
		return [
            'true' => 1,
            'false' => 0
        ];
    }
    


    public function filter(Builder $builder, $value)
    {
       $value = $this->resolveFilterValue($value);

       if(is_null($value)){
           return $builder;
       }
       
       return $builder->where('public', $value);
    }
}