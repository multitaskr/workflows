<?php
namespace App\Filters\Step;

use App\Filters\FilterAbstract;
use Illuminate\Database\Eloquent\Builder;

class WorkflowFilter extends FilterAbstract
{
    public function filter(Builder $builder, $value)
    {
        return $builder->whereHas('workflow', function($query) use($value) {
            $query->where('workflows.uid', $value);
        });
    }
}