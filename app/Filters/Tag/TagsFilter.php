<?php

namespace App\Filters\Tag;

use App\Filters\FilterAbstract;
use Illuminate\Database\Eloquent\Builder;

class TagsFilter extends FilterAbstract
{

    public function filter(Builder $builder, $value)
    {
        $tags = explode(',', $value);

        return $builder->whereIn('slug', $tags)->orWhereIn('name', $tags);
    }
}
