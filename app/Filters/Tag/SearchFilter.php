<?php

namespace App\Filters\Tag;

use App\Filters\FilterAbstract;
use Illuminate\Database\Eloquent\Builder;

class SearchFilter extends FilterAbstract
{

    public function filter(Builder $builder, $value)
    {
        return $builder->where('name', 'like', "%$value%");
    }
}
