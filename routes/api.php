<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/live', function () {
    return response([], 200);
});

Route::group(['prefix' => 'public', 'as' => 'public.'], function () {

    Route::get('/', 'WorkflowsController@public')->name('workflows.public');
    Route::get('/workflows-tags', 'WorkflowsTagsController@index')->name('workflows.tags.index');
    Route::get('/{slug}/steps', 'WorkflowsStepsController@public')->name('workflows.steps.show')->middleware('retrive-user');

    Route::group(['middleware' => 'public-step'], function () {
        Route::post('steps/{step}/values', 'StepValuesController@store')->name('steps.values.store');
        Route::get('steps/{step}/values', 'StepValuesController@show')->name('steps.values.show');
        Route::post('sections/{section}/values', 'SectionValuesController@store')->name('sections.values.store');
        Route::get('sections/{section}/values', 'SectionValuesController@show')->name('sections.values.show');
    });
});

Route::group(['middleware' => 'auth'], function () {
    Route::get('values', 'InputValuesController@index')->name('values.index');
    Route::get('/values/raw', 'InputValuesController@raw')->name('values.raw');
    Route::get('values/{input_value}', 'InputValuesController@show')->name('values.show');

    Route::get('steps/{step}/values', 'StepValuesController@show')->name('steps.values.show');
    Route::get('sections/{section}/values', 'SectionValuesController@show')->name('sections.values.show');
    Route::get('/tags/values', 'TagsValuesController@show')->name('tags.values.show');

    Route::post('steps/{step}/values', 'StepValuesController@store')
        ->name('steps.values.store')
        ->middleware('customer-step');

    Route::post('sections/{section}/values', 'SectionValuesController@store')
        ->name('sections.values.store')
        ->middleware('customer-step');
    Route::get('/subject-steps', 'SubjectStepController@index')->name('subject-step.index');
    Route::get('/subject-steps/{subject_step}', 'SubjectStepController@show')->name('subject-step.show');

    Route::group(['middleware' => 'step-in-progress'], function(){
        Route::post('/subject-steps/{subject_step}/complete', 'SubjectStepController@complete')->name('subject-step.complete');
        Route::post('/subject-steps/{subject_step}/rollback', 'SubjectStepController@rollback')->name('subject-step.rollback');
    });

    Route::post('/subject-steps/step/{step}', 'SubjectStepController@fromStepComplete')->name('subject-step.from-step-update');
});

Route::group(['middleware' => ['auth', 'validate-not-role:Customer']], function () {

    Route::get('/', 'WorkflowsController@index')->name('workflows.index');
    Route::post('/', 'WorkflowsController@store')->name('workflows.store');


    Route::resource('steps', 'StepsController')->except('create', 'edit');
    Route::post('steps/order', 'StepsController@order')->name('steps.order');
    Route::resource('step-logics', 'StepLogicsController')->except('create', 'edit');
    Route::resource('sections', 'SectionsController')->except('create', 'edit');
    Route::post('sections/order', 'SectionsController@order')->name('sections.order');
    Route::resource('inputs', 'InputsController')->except('create', 'edit');
    Route::post('inputs/order', 'InputsController@order')->name('inputs.order');
    Route::get('/files/{file_name}', 'FilesController@show')->name('files.show');
    Route::get('/webhooks-manager/services', 'WebhooksManagerController@services')->name('webhooks-manager.services');
    Route::get('/webhooks-manager/services/{service}/models', 'WebhooksManagerController@models')->name('webhooks-manager.services.models');
    Route::get('/webhooks-manager/services/{service}/models/{model}', 'WebhooksManagerController@columns')->name('webhooks-manager.services.models.columns');
    Route::post('/webhooks', 'WebhooksController@store')->name('webhooks.store');
    Route::match(['put', 'patch'], '/webhooks/{webhook}', 'WebhooksController@update')->name('webhooks.update');
    Route::delete('/webhooks/{webhook}', 'WebhooksController@destroy')->name('webhooks.destroy');
    Route::apiResource('tags', 'TagsController')->only('index');

    Route::group(['prefix' => '/{workflow}'], function () {
        Route::get('/', 'WorkflowsController@show')->name('workflows.show');
        Route::match(['put', 'patch'], '/', 'WorkflowsController@update')->name('workflows.update');
        Route::delete('/', 'WorkflowsController@destroy')->name('workflows.destroy');
        Route::get('/sections', 'WorkflowSectionsController@index')->name('workflows.sections');
        Route::post('/clone-steps', 'WorkflowSubjectStepsController@store')->name('workflows.clone-steps');
    });
});
