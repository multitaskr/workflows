<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Private Routes
|--------------------------------------------------------------------------
|
| Here is where you can register Private routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "private" middleware group. Enjoy building your Private!
|
*/

Route::get('live', function(){
    return response([], 200);
})->name('live');

Route::get('/public/workflows/{workflow}/steps', 'WorkflowsStepController@public')->name('public.workflow.step.show');
Route::get('/workflows', 'WorkflowsController@index')->name('workflows.index');
Route::get('/workflows/slug/{slug}', 'WorkflowsController@showBySlug')->name('workflows.show.slug');
Route::get('/workflows/{workflow}/steps', 'WorkflowsStepController@index')->name('workflow.step.index');
Route::post('/workflows/{workflow}/clone-steps', 'WorkflowSubjectStepsController@store')->name('workflows.clone-steps');
Route::post('/workflows/{workflow}/restart-steps', 'WorkflowSubjectStepsController@restart')->name('workflows.restart-steps');
Route::get('/workflows/{workflow}', 'WorkflowsController@show')->name('workflows.show');
Route::get('/steps/{step}', 'StepsController@show')->name('steps.show');
Route::get('/steps/{step}/values', 'StepsController@values')->name('steps.values');
Route::post('/inputs/{input}/value', 'InputsController@value')->name('inputs.value');
Route::get('/inputs/{input}', 'InputsController@show')->name('inputs.show');
Route::get('/subject-steps', 'SubjectStepController@index')->name('subject-step.index');
Route::get('/subject-steps/{subject_step}', 'SubjectStepController@show')->name('subject-step.show');
Route::get('/subject-steps/step/{step}', 'SubjectStepController@fromStep')->name('subject-step.from-step');
Route::patch('/subject-steps/step/{step}', 'SubjectStepController@fromStepComplete')->name('subject-step.from-step-update');
Route::post('/subject-steps/{subject_step}/complete', 'SubjectStepController@complete')->name('subject-step.complete');
Route::post('/subject-steps/{subject_step}/rollback', 'SubjectStepController@rollback')->name('subject-step.rollback');
Route::post('/subject-steps/deals/import', 'SubjectStepController@importFromDeals')->name('subject-step.deals.import');

